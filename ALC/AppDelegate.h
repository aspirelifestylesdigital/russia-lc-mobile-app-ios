//
//  AppDelegate.h
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ALRadialMenu.h"
#import "UserObject.h"
#import "SlideNavigationController.h"
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) NSString* Email;
//@property (strong, nonatomic) NSString* Password;

@property CLLocationCoordinate2D currentGeo;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (assign)BOOL isCheckedLocation;
@property (strong, nonatomic) ALRadialMenu *socialMenu;
@property (strong, nonatomic)   UIView* socialView;
@property (strong, nonatomic)   UIButton* socialButton;
@property (strong, nonatomic)   UIImageView* imvBackGround;
@property (nonatomic, strong) SlideNavigationController *rootVC;


@property (strong, nonatomic) NSString* AuthToken;
@property (strong, nonatomic) NSDate* AuthExpDate;
-(void) storeToken:(NSString*) token;
-(NSString*) getToken;
- (NSString*) getUserEmail;
- (NSString*) getUserPassword;
- (CLLocation*) requestUserCurrentLocation;
-(void) logoutUser;
-(BOOL) isLoggedIn;
- (UserObject*) getLoggedInUser;
- (void)setRootViewAfterLogin:(BOOL)withAnimation;

@property (nonatomic, strong) NSString* FORGET_PASS_FROM_USER;
@property BOOL HAS_FORGOT_PASSWORD;
//B2C data
@property (nonatomic, strong)NSString* ACCESS_TOKEN;
@property (nonatomic, strong)NSString* REFRESH_TOKEN;
@property (nonatomic, strong)NSString* ONLINE_MEMBER_ID;
@property (nonatomic, strong)NSDate* B2C_ExpiredAt;

-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime;
- (void)showLivePersonChatScreen;

@end

