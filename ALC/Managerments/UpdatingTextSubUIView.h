//
//  UpdatingTextSubUIView.h
//  ALC
//
//  Created by Chung Mai on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatingTextSubUIView : UIView

-(void) setTextForViews;
@end
