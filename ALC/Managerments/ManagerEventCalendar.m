//
//  ManagerEventCalendar.m
//  ALC
//
//  Created by Hai NguyenV on 10/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ManagerEventCalendar.h"
#import <EventKit/EventKit.h>
#import "EventObj.h"

#define kEventKey @"ListEventItems"

@implementation ManagerEventCalendar

static ManagerEventCalendar *calendarManager;

+ (ManagerEventCalendar*) instance
{
    if (!calendarManager) {
        calendarManager = [[ManagerEventCalendar alloc]init];
    }
    
    return calendarManager;
}
-(void)removeEventFromCalendar:(NSString*)identifier{
    EKEventStore* store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent* eventToRemove = [store eventWithIdentifier:identifier];
        if (eventToRemove) {
            NSError* error = nil;
            [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&error];
        }
    }];
}

-(void)addEventoCalendar:(NSString*)bookingID withTitle:(NSString*)title withDate:(NSDate*)startDate{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = title;
        event.startDate = startDate; //date to start
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        [self saveEventId:bookingID withID:event.eventIdentifier];
    }];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:NSLocalizedString(@"Your request service date is added to calendar",ni)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [alert popoverPresentationController];
                                                           }];
    
    [alert addAction:cancelAction];
    
    [[self currentNavigationController] presentViewController:alert animated:YES completion:nil];
}
-(void)saveEventId:(NSString*)bookingID withID:(NSString*)eventID{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrRecentlyViewedItems;
    
    NSData *data = [userDefault objectForKey:[NSString stringWithFormat:kEventKey]];
    if(data){
        NSArray *tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (tempArray) {
            arrRecentlyViewedItems = [[NSMutableArray alloc]initWithArray:tempArray];
        }
        
        EventObj *recentlyViewed = [[EventObj alloc]init];
        recentlyViewed.bookingID = bookingID;
        recentlyViewed.identify = eventID;
        
        if (arrRecentlyViewedItems == nil) {
            arrRecentlyViewedItems = [[NSMutableArray alloc]init];
            
            [arrRecentlyViewedItems addObject:recentlyViewed];
        }
        else
        {
            EventObj *aRecentlyViewed = nil;
            BOOL isExist = NO;
            for (aRecentlyViewed in arrRecentlyViewedItems) {
                if ([aRecentlyViewed.identify isEqualToString:eventID]) {
                    isExist = YES;
                    break;
                }
            }
            if (isExist && aRecentlyViewed) {
                [arrRecentlyViewedItems removeObject:aRecentlyViewed];
            }
            
            [arrRecentlyViewedItems insertObject:recentlyViewed atIndex:0];
            
        }
        data = [NSKeyedArchiver archivedDataWithRootObject:arrRecentlyViewedItems];
        [userDefault setObject:data forKey:[NSString stringWithFormat:kEventKey]];
        [userDefault synchronize];
    
    }
    
}
-(SlideNavigationController*) currentNavigationController{
    AppDelegate* appDelegate;
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    return appDelegate.rootVC;
}
-(NSArray*)getListEvent{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSData *data = [pref objectForKey:[NSString stringWithFormat:kEventKey]];
    NSArray *arrRecently = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return arrRecently;
}
-(NSString*)getIdentifyByBookingID:(NSString*)bookingID{
    NSString* eventID=nil;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSData *data = [pref objectForKey:[NSString stringWithFormat:kEventKey]];
    NSArray *arrRecently = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(arrRecently){
        EventObj *aRecentlyViewed = nil;
        for (aRecentlyViewed in arrRecently) {
            if ([aRecentlyViewed.bookingID isEqualToString:bookingID]) {
                eventID = aRecentlyViewed.identify;
                break;
            }
        }
    }
    
    return eventID;

}

@end
