//
//  MainViewController.h
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "ALRadialMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "SlideNavigationController.h"
#import <MessageUI/MessageUI.h>

@interface MainViewController : UIViewController<ALRadialMenuDelegate,SlideNavigationControllerDelegate,MFMailComposeViewControllerDelegate>
{
    AppDelegate* appdelegate;
}

//@property (strong, nonatomic) UIButton *socialButton;

- (UIViewController*) controllerByIdentifier:(NSString*) identifier;

-(void)setShowSplashScreen;
-(void)setRootViewAfterLogin;
-(void)customButtonBack;
-(void)addTitleToHeader:(NSString*)titleString withColor:(UIColor*)color;
-(void)addTitleToHeader:(NSString*)titleString;
-(void)showToast;
-(void)stopToast;
-(void)createConciergeButton;
-(BOOL)isShowConciergeButton;
-(void) buttonBack;
- (BOOL)slideNavigationControllerShouldDisplayRightMenu;
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu;
-(void) showConciergeButtonToolTip;
-(void) trackEcommerceProduct:(NSString*)name withCate:(NSString*)category;

// Set Localized String For View (Button, Label, TextField...)
-(void) setTextForViews;
-(NSRange) rangeAsteriskInString:(NSString *)text;
-(NSAttributedString *) setStyleForString:(NSString *)text;
@end


