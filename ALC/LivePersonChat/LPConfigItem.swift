//
//  LPConfigItem.swift
//  ALC
//
//  Created by mac-216 on 6/26/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import LPInfra

enum LPConfigItemType: String {
    case string = "String"
    case float = "Float"
    case double = "Double"
    case color = "UIColor"
    case bool = "Bool"
    case int = "Int"
    case image = "UIImage"
    case uint = "UInt"
    case urlPreviewStyle = "LPUrlPreviewStyle"
    case checkmarksState = "CheckmarksState"
    case uiFontTextStyle = "UIFontTextStyle"
    case lpLanguage = "LPLanguage"
    case uiStatusBarStyle = "UIStatusBarStyle"
}


struct LPConfigItem {
    
    let name: String
    let type: LPConfigItemType
    let value: Any
    
    init?(csv: String, separator: CharacterSet = CharacterSet(charactersIn: ",")) {
        let params = csv.components(separatedBy: separator)
        if params.count < 3 {
            print("LPConfigItem error: incorrect csv string")
            return nil
        }
        
        name = params[0]
        if name.isEmpty {
            print("LPConfigItem error: name is empty")
            return nil
        }
        
        if name == "conversationSeparatorFontName" {
            print("LPConfigItem error: name is empty")
        }
        
        if let newType = LPConfigItemType(rawValue: params[1]) {
            type = newType
        } else {
            print("LPConfigItem error: type of <" + name + "> is undefined" )
            return nil
        }
        
        let valueParam = params[2].lowercased()
        if valueParam == "nil" {
            return nil
        }
        var newValue: Any?
        switch type {
        case .bool:
            newValue = Bool(valueParam)
        case .string:
            newValue = valueParam
        case .float:
            newValue = Float(valueParam)
        case .double:
            newValue = Double(valueParam)
        case .color:
            newValue = UIColor(hex: valueParam)
        case .int:
            newValue = Int(valueParam)
        case .image:
            newValue = UIImage(named: valueParam)
        case .uint:
            newValue = UInt(valueParam)
        default:
            newValue = valueParam
        }
        if newValue != nil {
            value = newValue!
        } else {
            print("LPConfigItem error: value of <" + name + "> is incorrect" )
            return nil
        }
    }
    
}
