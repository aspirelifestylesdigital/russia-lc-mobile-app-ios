//
//  LPChatNotificationsProtocol.swift
//  ALC
//
//  Created by mac-216 on 6/25/18.
//  Copyright © 2018 Sunrise Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

@objc protocol LPChatNotificationsProtocol {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?)
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
}
