//
//  main.m
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NSBundle+LanguageManager.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        [NSBundle setLanguage:@"ru"];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
