//
//  SplashStep1.h
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerItem.h>
#import <AVFoundation/AVPlayerLayer.h>
#import "UpdatingTextSubUIView.h"

@interface SplashStep1 : UpdatingTextSubUIView
{
    AVPlayer* _player;
    CMTime currentTime;
}
@property (weak, nonatomic) IBOutlet UIView *pView;

@property (weak, nonatomic) IBOutlet UILabel *savourLbl;
@property (weak, nonatomic) IBOutlet UILabel *savourMessageLbl;

-(void)onResume;
-(void)onPause;
@end
