//
//  SplashStep4.m
//  ALC
//
//  Created by Hai NguyenV on 9/5/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SplashStep4.h"

@implementation SplashStep4

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"SplashStep4" owner:self options:nil]
            objectAtIndex:0];
    self.frame = frame;
    resetScaleViewBaseOnScreen(self);
    [self setTextForViews];
    return self;
}


-(void)setTextForViews
{
    self.indulgeLbl.text = @"Не отказывайте себе!";
    self.indulgeMessageLbl.text = @"Подробное опишите, чего вы хотите, все остальное сделаем мы.";
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
