//
//  CCNumberCell.m
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//
#import "CCNumberCell.h"

@implementation CCNumberCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end