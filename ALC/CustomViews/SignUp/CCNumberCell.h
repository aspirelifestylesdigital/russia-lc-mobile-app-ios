//
//  CCNumberCell.h
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface CCNumberCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UIButton *btCheck;
@end