//
//  SplashStep4.h
//  ALC
//
//  Created by Hai NguyenV on 9/5/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdatingTextSubUIView.h"

@interface SplashStep4 : UpdatingTextSubUIView
@property (weak, nonatomic) IBOutlet UILabel *indulgeLbl;
@property (weak, nonatomic) IBOutlet UILabel *indulgeMessageLbl;
@end
