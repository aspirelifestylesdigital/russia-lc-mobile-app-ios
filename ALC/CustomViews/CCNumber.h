//
//  CCNumber.h
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface CCNumber : NSObject
@property (strong, nonatomic) NSString* longDescription;
@property (strong, nonatomic) NSString* shortCCNumber;
-(id)init:(NSString*) shortCC withDescription:(NSString*) longDesc;
@end
