//
//  MyRequestCell.m
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyRequestCell.h"
#import "WSCancelRequest.h"
#import "BaseResponseObject.h"
#import "HotelRequestController.h"
#import "GourmetRequestController.h"
#import "MainViewController.h"
#import "OrthersBookingController.h"
#import "CarRentalTransferRequestController.h"
#import "ManagerEventCalendar.h"
#import "GolfRequestController.h"
#import "EventRequestController.h"

@interface MyRequestCell (){
    WSCancelRequest* wsCancel;
    MyRequestObject* requestItem;
}

@end

@implementation MyRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) showData:(MyRequestObject*) item{
    
    
    _lbTitleStatus.text = NSLocalizedString(@"Status: ", nil);
    [_btAmend setTitle:NSLocalizedString(@"Amend", nil) forState:UIControlStateNormal];
    [_btCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    requestItem = item;
    
    [_icRequestType setImage:[UIImage imageNamed:[self getImageByRequestType:item.parsedRequestType]]];
    
    NSDictionary *attributes = [(NSAttributedString *)_lbTitle.attributedText attributesAtIndex:0 effectiveRange:NULL];
    [_lbTitle setAttributedText:[[NSAttributedString alloc] initWithString:item.itemTitle attributes:attributes]];
    
    _lbStatus.text = item.requestStatus;
    
    attributes = [(NSAttributedString *)_lbDescription.attributedText attributesAtIndex:0 effectiveRange:NULL];
    
    
    [_lbDescription setAttributedText:[[NSAttributedString alloc] initWithString:item.fullDescription attributes:attributes]];
    
    if(item.notDisplayEditCancel){
        _amendcancelView.hidden = true;
        _heightConstraintAmend.constant = 0;
        _heightConstraintCancel.constant = 0;
    } else {
        _amendcancelView.hidden = false;
        _heightConstraintAmend.constant = 22*SCREEN_SCALE;
        _heightConstraintCancel.constant = 22*SCREEN_SCALE;
    }
    
    if(item.isHistory){
        _btCalendar.hidden = YES;
    }else{
        _btCalendar.hidden = NO;
    }
}

- (IBAction)actionAddCalendar:(id)sender {
    if(requestItem.happenDateTime){
        [self addEventToCalendar:requestItem.requestId withTitle:requestItem.itemTitle withDate:requestItem.happenDateTime];
    }
}

-(void)addEventToCalendar:(NSString*)bookingID withTitle:(NSString*)title withDate:(NSDate*)date{
    NSString* eventID = [[ManagerEventCalendar instance] getIdentifyByBookingID:bookingID];
    if(eventID!=nil)
        [[ManagerEventCalendar instance] removeEventFromCalendar:eventID];
    // add a event to calendar
    [[ManagerEventCalendar instance] addEventoCalendar:bookingID withTitle:title withDate:date];
    //////
}

- (IBAction)actionAmend:(id)sender {
    if(_delegate){
        [_delegate amendRequest:requestItem];
    }
    if(![requestItem notAllowEditCancel]){
        
        //load Request details to amend
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        MainViewController* controller;
        switch (requestItem.parsedRequestType) {
            case HOTEL_REQUEST:
            case HOTEL_RECOMMEND_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"HotelRequestController"];
                ((HotelRequestController*) controller).hideTopNavigator = YES;
                ((HotelRequestController*) controller).requestType = requestItem.parsedRequestType;
                ((HotelRequestController*) controller).bookingId = requestItem.requestId;
                break;
            case GOLF_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"GolfRequestController"];
                ((GolfRequestController*) controller).bookingId = requestItem.requestId;
                break;
            case ENTERTAINMENT_REQUEST:
            case ENTERTAINMENT_RECOMMEND_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"EventRequestController"];
                ((EventRequestController*) controller).hideTopNavigator = YES;
                ((EventRequestController*) controller).requestType = requestItem.parsedRequestType;
                ((EventRequestController*) controller).bookingId = requestItem.requestId;
                break;
            case CAR_RENTAL_REQUEST:
            case CAR_TRANSFER_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"CarRentalTransferRequestController"];
                ((CarRentalTransferRequestController*) controller).hideTopNavigator = YES;
                ((CarRentalTransferRequestController*) controller).requestType = requestItem.parsedRequestType;
                ((CarRentalTransferRequestController*) controller).bookingId = requestItem.requestId;
                break;
            case GOURMET_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"GourmetRequestController"];
                ((GourmetRequestController*) controller).bookingId = requestItem.requestId;
                ((GourmetRequestController*) controller).hideTopNavigator = YES;
                break;
            case GOURMET_RECOMMEND_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"GourmetRequestController"];
                ((GourmetRequestController*) controller).bookingId = requestItem.requestId;
                ((GourmetRequestController*) controller).hideTopNavigator = YES;
                ((GourmetRequestController*) controller).requestType = requestItem.parsedRequestType;
                break;
            case SPA_REQUEST:
            case OTHER_REQUEST:
                controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"OrthersBookingController"];
                ((OrthersBookingController*) controller).bookingId = requestItem.requestId;
                break;
            default:
                break;
                
        }
        
        if(controller){
            AppDelegate* appdele =(AppDelegate*) [[UIApplication sharedApplication] delegate];
            [appdele.rootVC pushViewController:controller animated:YES];
        }
    }
    
}

- (IBAction)actionCancel:(id)sender {
    if(_delegate){
        [_delegate cancelingRequest:requestItem];
    }
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_CANCEL_REQUEST){
        if(_delegate){
            [_delegate didCancelRequest:requestItem];
        }
    }
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result.task == WS_CANCEL_REQUEST){
        if(_delegate){
            [_delegate failCancelRequest:requestItem];
        }
    }
}


-(NSString*) getImageByRequestType:(enum REQUEST_TYPE) type{
    switch (type) {
        case HOTEL_REQUEST:
        case HOTEL_RECOMMEND_REQUEST:
            return @"Hotel_Circle.png";
        case GOLF_REQUEST:
            return @"Golf_Circle.png";
        case ENTERTAINMENT_REQUEST:
        case ENTERTAINMENT_RECOMMEND_REQUEST:
            return @"Event_Circle.png";
        case CAR_RENTAL_REQUEST:
        case CAR_TRANSFER_REQUEST:
            return @"Car_Circle.png";
        case GOURMET_REQUEST:
            return @"Dining_Circle.png";
        case OTHER_REQUEST:
            return @"Other_Request_Sel.png";
        case GOURMET_RECOMMEND_REQUEST:
            return @"Dining_Circle.png";
        case SPA_REQUEST:
            return @"Spa_Circle_Sel.png";
        default:
            return @"Other_Request_Sel.png";
    }
    
}

@end
