//
//  SplashStep1.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SplashStep1.h"


@implementation SplashStep1

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"SplashStep1" owner:self options:nil]
            objectAtIndex:0];
    self.frame = frame;
    [self setNeedsLayout];
    [self sizeToFit];
    
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"frying" ofType:@"mp4"]];
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVPlayerItem *item = [[AVPlayerItem alloc] initWithAsset:asset];
    
    _player = [[AVPlayer alloc] initWithPlayerItem:item];
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:_player];
    layer.frame = frame;
    _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_player currentItem]];
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    //view.backgroundColor = [UIColor blackColor];
    [self.pView.layer addSublayer:layer];
    [self.pView addSubview:view];
    
    [_player play];
    
    resetScaleViewBaseOnScreen(self);
    [self setTextForViews];
    return self;
}

-(void) setTextForViews
{
    self.savourLbl.text = NSLocalizedString(@"Savour", @"");
    self.savourMessageLbl.text = NSLocalizedString(@"Tantalise your taste buds with our unique selection of the world's best restaurants and bars", @"");
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}
-(void)onPause{
    [_player pause];
    currentTime = [_player currentTime];
}
-(void)onResume{
    if(CMTimeGetSeconds(currentTime) != CMTimeGetSeconds(kCMTimeZero))
    {
        [self playAt:currentTime];
    }
    
}
-(void)playAt: (CMTime)time {
    if(_player.status == AVPlayerStatusReadyToPlay && _player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        [_player seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
    [_player play];
        }];
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self playAt:time];
        });
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
