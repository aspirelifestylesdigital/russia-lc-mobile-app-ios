//
//  CellView_User.m
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CellView_User.h"
#import "StoreUserData.h"
#import "UserObject.h"
#import "UIImageView+AFNetworking.h"
#import "CommonUtils.h"

@implementation CellView_User

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) loadView
{
//    ÷÷
//    [self layoutIfNeeded];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn])
    {
        UserObject* user = getUser();
        [self.imvAvartar setImageWithURL:[NSURL URLWithString:[DOMAIN_URL stringByAppendingString:stringFromObject(user.avatarUrl)]] placeholderImage:[UIImage imageNamed:@"default_profle.png"]];
        [self.lblUserName setText:user.fullName];
        [self.lblUserName sizeToFit];
    }
    else
    {
        [self.imvAvartar setImage:[UIImage imageNamed:@"default_profle.png"]];
        [self.lblUserName sizeToFit];
    }
    
    [self.vwFrameAvatar.layer setCornerRadius:self.vwFrameAvatar.frame.size.height/2.0];
    [self.vwFrameContent.layer setCornerRadius:4.0];
    [self.lblContent setText:self.currentChat.valueContent];

    NSInteger fistWidth = self.lblContent.frame.size.width;
    
    NSLog(@"%ld,%ld",(long)self.lblContent.frame.size.width,(long)self.lblContent.frame.size.height);
    
    [self.lblContent sizeToFit];
    
    NSLog(@"%ld,%ld",(long)self.lblContent.frame.size.width,(long)self.lblContent.frame.size.height);
    
    NSInteger spaceBetweenWidth = self.lblContent.frame.size.width - fistWidth;
    if (self.lblContent.frame.size.width < self.lblUserName.frame.size.width)
    {
        spaceBetweenWidth = self.lblUserName.frame.size.width - fistWidth;
    }
    
    
    self.layoutHeightContent.constant = self.lblContent.frame.size.height;
    
    self.layoutLeadingWithView.constant = self.layoutLeadingWithView.constant - spaceBetweenWidth;
    
    NSInteger currentHeight = self.lblContent.frame.origin.y + self.layoutHeightContent.constant + 5;
    self.layoutFrameCOntent.constant = currentHeight;
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + spaceBetweenWidth, self.vwFrameContent.frame.origin.y*2 + currentHeight)];
}
@end
