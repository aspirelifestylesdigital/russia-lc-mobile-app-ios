//
//  CellView_Bot.h
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ChatBotObject.h"
#import "CustomButtonView.h"

@protocol CellView_BotDelegate <NSObject>

@optional
- (void) activeClickToButton:(ChatButtonObject*)currentButton;

@end

@interface CellView_Bot : UIView<CustomButtonViewDelegate>
{
    NSInteger currentBeginX;
    NSInteger currentBeginY;
}

@property (assign,nonatomic) id <CellView_BotDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *vwFrameAvatar;
@property (strong, nonatomic) IBOutlet UIView *vwFrameContent;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutFrameCOntent;
@property (strong, nonatomic) IBOutlet UIView *vwShowSpecial;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightShowSpecial;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutLeadingWithView;

@property (strong,nonatomic) ChatBotObject* currentChat;

- (void) loadView;
- (void) lockView;
@end
