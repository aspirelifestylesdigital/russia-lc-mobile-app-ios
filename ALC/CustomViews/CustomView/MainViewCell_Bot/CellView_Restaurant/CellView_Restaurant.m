//
//  CellView_Restaurant.m
//  demoChatBot
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CellView_Restaurant.h"
#import "UIImageView+AFNetworking.h"
#import "DCRItemDetailsController.h"
#import "BaseResponseObject.h"
#import "InAppBrowserViewController.h"
#import "GourmetRequestController.h"

@implementation CellView_Restaurant

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)clickImage:(id)sender
{
    /*GourmetDetailsController* controller = (GourmetDetailsController*) [self controllerByIdentifier:@"GourmetDetailsController"];
    controller.restaurantId = self.currentGourmet.restaurantID;
    [topViewController().navigationController pushViewController:controller animated:YES];*/
}

- (IBAction)clickTitle:(id)sender
{
    /*GourmetDetailsController* controller = (GourmetDetailsController*) [self controllerByIdentifier:@"GourmetDetailsController"];
    controller.restaurantId = self.currentGourmet.restaurantID;
    [topViewController().navigationController pushViewController:controller animated:YES];
     */
}


- (void) loadView
{
    [self.vwFrameContent.layer setCornerRadius:8.0];
    if (self.currentGourmet)
    {
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        self.btnFarvorite.hidden = ![appdele isLoggedIn];
        if (self.currentGourmet.isInterested)
        {
            [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart.png"] forState:UIControlStateNormal];
            [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart.png"] forState:UIControlStateHighlighted];
        }
        else
        {
            [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart_white.png"] forState:UIControlStateNormal];
            [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart_white.png"] forState:UIControlStateHighlighted];

        }
        
        [self.imvRestaurant setImageWithURL:[NSURL URLWithString:self.currentGourmet.backgroundImg]];
        [self.lblRestaurantName setText:self.currentGourmet.restaurantName];
        if (![self.currentGourmet.address isEqualToString:@""])
        {
            NSArray* arrayString = [self.currentGourmet.address componentsSeparatedByString:@","];
            if ([arrayString count] > 0)
            {
                for (int i = 0; i < [arrayString count]; i++)
                {
                    if (i == 0)
                    {
                        self.lblAddress_street.text = stringFromObject([arrayString objectAtIndex:i]);
                        self.lblAddress_street.text = [self.lblAddress_street.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    }
                    else if (i == 1)
                    {
                        self.lblAddres_number.text = stringFromObject([arrayString objectAtIndex:i]);
                        self.lblAddres_number.text = [self.lblAddres_number.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    }
                }
            }
        }
        
        self.lblAddress_city_phone.text = [NSString stringWithFormat:@"%@, %@",stringFromObject(self.currentGourmet.cityName),stringFromObject(self.currentGourmet.countryName)];

        self.lblAddress_city_phone.text = [self.lblAddress_city_phone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if ([self.currentGourmet.countryName isEqualToString:self.currentGourmet.cityName])
        {
            self.lblAddress_city_phone.text = stringFromObject(self.currentGourmet.countryName);
        }
        
        [self.lblNumberRating setText:self.currentGourmet.ratingNumber];
        
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@""];
        
        UIColor * colorShow = [UIColor colorWithRed:188.0/255 green:168.0/255 blue:86.0/255 alpha:1.0];
        
        NSMutableDictionary * attributes = [NSMutableDictionary dictionaryWithObject:colorShow forKey:NSForegroundColorAttributeName];
        [attributes setObject:self.lblRating.font forKey:NSFontAttributeName];
        NSAttributedString * subString = [[NSAttributedString alloc] initWithString:self.currentGourmet.priceRange attributes:attributes];
        [string appendAttributedString:subString];
        
        switch ([string length])
        {
            case 0:
            {
                colorShow = [UIColor colorWithRed:153.0/255 green:153.0/225 blue:153.0/255 alpha:1.0];
                NSMutableDictionary * attributesAdd = [NSMutableDictionary dictionaryWithObject:colorShow forKey:NSForegroundColorAttributeName];
                [attributesAdd setObject:self.lblRating.font forKey:NSFontAttributeName];
                NSAttributedString * subStringAdd = [[NSAttributedString alloc] initWithString:@"$$$$" attributes:attributesAdd];
                [string appendAttributedString:subStringAdd];
            }
             break;
            case 1:
            {
                colorShow = [UIColor colorWithRed:153.0/255 green:153.0/225 blue:153.0/255 alpha:1.0];
                NSMutableDictionary * attributesAdd = [NSMutableDictionary dictionaryWithObject:colorShow forKey:NSForegroundColorAttributeName];
                [attributesAdd setObject:self.lblRating.font forKey:NSFontAttributeName];
                NSAttributedString * subStringAdd = [[NSAttributedString alloc] initWithString:@"$$$" attributes:attributesAdd];
                [string appendAttributedString:subStringAdd];
            }
                break;
            case 2:
            {
                colorShow = [UIColor colorWithRed:153.0/255 green:153.0/225 blue:153.0/255 alpha:1.0];
                NSMutableDictionary * attributesAdd = [NSMutableDictionary dictionaryWithObject:colorShow forKey:NSForegroundColorAttributeName];
                [attributesAdd setObject:self.lblRating.font forKey:NSFontAttributeName];
                NSAttributedString * subStringAdd = [[NSAttributedString alloc] initWithString:@"$$" attributes:attributesAdd];
                [string appendAttributedString:subStringAdd];
            }
                break;
            case 3:
            {
                colorShow = [UIColor colorWithRed:153.0/255 green:153.0/225 blue:153.0/255 alpha:1.0];
                NSMutableDictionary * attributesAdd = [NSMutableDictionary dictionaryWithObject:colorShow forKey:NSForegroundColorAttributeName];
                [attributesAdd setObject:self.lblRating.font forKey:NSFontAttributeName];
                NSAttributedString * subStringAdd = [[NSAttributedString alloc] initWithString:@"$" attributes:attributesAdd];
                [string appendAttributedString:subStringAdd];
            }
                break;
                
            default:
                break;
        }
        
        [self.lblRating setAttributedText:string];
    }
    
}

- (UIViewController*) controllerByIdentifier:(NSString*) identifier {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    return [mainStoryboard instantiateViewControllerWithIdentifier:identifier];
}


- (IBAction)clickReadMore:(id)sender
{
    /*GourmetDetailsController* controller = (GourmetDetailsController*) [self controllerByIdentifier:@"GourmetDetailsController"];
    controller.restaurantId = self.currentGourmet.restaurantID;
    [topViewController().navigationController pushViewController:controller animated:YES];*/
}

- (IBAction)clickBookNew:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        /*MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:[NSString stringWithFormat:@"Hey Concierge, I want to book %@",self.currentGourmet.restaurantName]];
        [mail setToRecipients:@[@"askconcierge@aspirelifestyles.com"]];
        [topViewController() presentViewController:mail animated:YES completion:NULL];*/
        
    }
    else
    {
        [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
            UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                                   style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                       setRootViewLogout();
                                                                   }];
            showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction);            }];
        
    }
}

- (IBAction)clickFarvorite:(id)sender
{
    /*[(MainViewController*)topViewController() showToast];
    if(self.currentGourmet.isInterested)
    {
        //remove
        wsRemoveItemfromWishList = [[WSRemoveItemfromWishList alloc] init];
        wsRemoveItemfromWishList.delegate = self;
        [wsRemoveItemfromWishList removeItem:self.currentGourmet.restaurantID];
    } else {
        //add
        wsAddItemToWishList = [[WSAddItemToWishList alloc] init];
        wsAddItemToWishList.delegate = self;
        [wsAddItemToWishList addItem:self.currentGourmet.restaurantID];
    }*/

}

- (IBAction)clickShare:(id)sender
{
    NSURL* currentURL = [NSURL URLWithString:self.currentGourmet.ItemURL];
    if (currentURL)
    {
        NSArray *activityItems = @[currentURL];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypeMail,UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop,UIActivityTypeOpenInIBooks];
        [topViewController() presentViewController:activityVC animated:TRUE completion:nil];
    }
   
}


#pragma mark Delegate Click Favorite
/*
-(void)loadDataDoneFrom:(WSBase*)ws{
    [(MainViewController*)topViewController() stopToast];
    if (ws.task == WS_ADD_ITEM_TO_WISHLIST)
    {
        self.currentGourmet.isInterested = YES;
        
    } else if (ws.task == WS_REMOVE_ITEM_FROM_WISHLIST)
    {
        self.currentGourmet.isInterested = NO;
    }
    
    if (self.currentGourmet.isInterested)
    {
        [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart.png"] forState:UIControlStateNormal];
        [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart_white.png"] forState:UIControlStateNormal];
        [self.btnFarvorite setImage:[UIImage imageNamed:@"icon_heart_white.png"] forState:UIControlStateHighlighted];
        
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [(MainViewController*)topViewController() stopToast];
    if (result.task == WS_ADD_ITEM_TO_WISHLIST)
    {
        
    } else if (result.task == WS_REMOVE_ITEM_FROM_WISHLIST){
        
    }
}*/

#pragma mark Huy Truong Mail Delegat

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    [topViewController() dismissViewControllerAnimated:YES completion:nil];
}



@end
