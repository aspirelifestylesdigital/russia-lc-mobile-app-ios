//
//  CellView_Restaurant.h
//  demoChatBot
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GourmetObject.h"

@interface CellView_Image : UIView

@property (strong, nonatomic) NSString* linkImage;
@property (strong, nonatomic) IBOutlet UIView *vwFrameContent;
@property (strong, nonatomic) IBOutlet UIImageView *imvRestaurant;

- (void) loadView;
@end
