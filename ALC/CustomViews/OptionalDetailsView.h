//
//  OptionalDetailsView.h
//  ALC
//
//  Created by Anh Tran on 10/4/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "CCNumberController.h"
#import "SubFilterController.h"
#import "WSGetMyPreferences.h"
#import "PhotoResizedInfo.h"
#import "BaseBookingDetailsObject.h"
#import "CarRentalDetailsObject.h"
#import "HotelRequestDetailsObject.h"
#import "GouetmetRecommendDetailsObj.h"

#define OPTIONAL_DETAIL_TAG 3000
@protocol OptionalDetailsViewDelegate <NSObject>

@optional
-(void) actionOptionalDetailsOpen:(BOOL) isOpened;
-(void) textFieldBeginEdit:(UIView*) textField;
-(void) touchinDetailsView;
@end

@interface OptionalDetailsView : UIView <UIImagePickerControllerDelegate,UINavigationControllerDelegate,CCNumberControllerDelegate,UITextFieldDelegate, SubFilterControllerDelegate, DataLoadDelegate,UITextViewDelegate>

@property (unsafe_unretained) id<OptionalDetailsViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIStackView *viewDetails;
@property (strong, nonatomic) IBOutlet UITextField *txtPax;
@property (strong, nonatomic) IBOutlet CustomTextField *txtLoyaltyProgram;
@property (strong, nonatomic) IBOutlet CustomTextField *txtMembershipNo;
@property (strong, nonatomic) IBOutlet UISwitch *swSmoking;
@property (strong, nonatomic) IBOutlet UITextView *txtSpecialMessage;
@property (strong, nonatomic) IBOutlet UIButton *btArrowViewDetails;
@property (strong, nonatomic) IBOutlet UILabel *lbTotalPhotos;
@property (strong, nonatomic) IBOutlet UITextField *txtAlternateEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtContactPhone;
@property (strong, nonatomic) IBOutlet UIButton *btCountryCode;
@property (strong, nonatomic) IBOutlet UIView *viewImageAndButton;
@property (strong, nonatomic) IBOutlet UIImageView *imvPhoto;
@property (strong, nonatomic) IBOutlet UIButton *btRoomType;
@property (strong, nonatomic) IBOutlet UIStackView *viewCombineAdultAndKids;
@property (strong, nonatomic) IBOutlet UITextField *txtAdultsPax;
@property (strong, nonatomic) IBOutlet CustomTextField *txtKidsPax;
@property (strong, nonatomic) IBOutlet CustomTextField *txtRetalCompany;
@property (strong, nonatomic) IBOutlet UIStackView *viewNormalPax;
@property (strong, nonatomic) IBOutlet UIButton *btRadioEmail;
@property (strong, nonatomic) IBOutlet UIButton *btRadioPhone;
@property (strong, nonatomic) IBOutlet UIView *viewMobileNumber;
@property (strong, nonatomic) IBOutlet UIView *viewEmail;
@property (strong, nonatomic) IBOutlet UIView *viewLoyalty;
@property (strong, nonatomic) IBOutlet UIView *viewMembership;
@property (strong, nonatomic) IBOutlet UIView *viewSmoking;
@property (strong, nonatomic) IBOutlet UIView *viewRooml;
@property (strong, nonatomic) IBOutlet UILabel *lbKidsUnder12;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationName;

@property (strong, nonatomic) IBOutlet UIView *viewPreferredVerhicle;
@property (strong, nonatomic) IBOutlet UIView *viewPrefferedCarCompany;
@property (strong, nonatomic) IBOutlet UIStackView *viewBudgetRange;
@property (strong, nonatomic) IBOutlet UIView *viewSpecialRequirement;
@property (strong, nonatomic) IBOutlet UIView *viewOptionalHeader;
@property (strong, nonatomic) IBOutlet UIStackView *viewCheckInOutDate;
@property (strong, nonatomic) IBOutlet UIView *viewReservations;
@property (strong, nonatomic) IBOutlet UIView *viewUploadPhoto;
@property (strong, nonatomic) IBOutlet UIView *viewPersonalInfomation;
@property (strong, nonatomic) IBOutlet UILabel *lbHolderAny;

//OptionDetails Output values
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* email;
@property ( nonatomic) NSInteger adultPax;
@property ( nonatomic) NSInteger kidsPax;
@property (nonatomic) NSInteger normalPax;
@property (strong, nonatomic)  NSString* loyaltyMembershipProgram;
@property (strong, nonatomic)  NSString* loyaltyMembershipNo;
@property (strong, nonatomic) NSString* roomType;
@property ( nonatomic) BOOL isSmoking;
@property (strong, nonatomic) NSString* specialMessage;
@property (strong, nonatomic) PhotoResizedInfo* selectedPhoto;
@property (strong, nonatomic)  NSMutableArray *selectedRoomKey;
@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;
@property (strong, nonatomic)  NSMutableArray *selectedPreferredVerhicleKeys;
@property (strong, nonatomic)  NSMutableArray *selectedPreferredCompanyKeys;
@property (strong, nonatomic) NSString* maximumPrice;
@property (strong, nonatomic) NSString* rentalCompany;
@property (strong, nonatomic) NSString* ratingKey;
@property (strong, nonatomic) NSString* checkInDate;
@property (strong, nonatomic) NSString* checkOutDate;
@property (strong, nonatomic) NSString* reservationDate;
@property (strong, nonatomic) NSString* reservationsTime;
@property (strong, nonatomic) NSString* reservationName;
@property ( nonatomic) BOOL isWithBreakfast;
@property ( nonatomic) BOOL isWithWifi;

-(void) exportValues;
-(void) displayAdultKidsView:(BOOL)display;
-(void) displayGourmetView;
-(void) showDetailsView:(BaseBookingDetailsObject*) details;
-(void) showCarRentalDetails:(CarRentalDetailsObject*) details;
-(void) showGourmetRecommendDetails:(GouetmetRecommendDetailsObj*) details;
-(void) showHotelRecommendDetails:(HotelRequestDetailsObject*) details;
-(void) applyRequestType:(enum REQUEST_TYPE) type;
-(void) closeAllKeyboardAndPicker;

-(void)setRoomPreferen:(NSString*)room;
-(void)setLoyalty:(NSString*)program withNo:(NSString*)number;
-(void)setCarPreference:(NSString*)verhicle withNo:(NSString*)companies;
@end


