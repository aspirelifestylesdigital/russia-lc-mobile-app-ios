//
//  SplashStep3.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SplashStep3.h"

@implementation SplashStep3
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"SplashStep3" owner:self options:nil]
            objectAtIndex:0];
    self.frame = frame;
    resetScaleViewBaseOnScreen(self);
    [self setTextForViews];
    return self;
}
-(void) setTextForViews
{
    self.enjoyLbl.text = NSLocalizedString(@"Enjoy", @"");
    self.enjoyMessageLbl.text = NSLocalizedString(@"Be it prime tickets, exclusive retreats, or the hottest party guest lists, let your Concierge connect you to all things exceptional without exception", @"");
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
