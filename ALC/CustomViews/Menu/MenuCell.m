//
//  MenuCell.m
//  ALC
//
//  Created by Hai NguyenV on 8/25/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
