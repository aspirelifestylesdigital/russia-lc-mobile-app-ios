//
//  OptionalDetailsView.m
//  ALC
//
//  Created by Anh Tran on 10/4/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OptionalDetailsView.h"
#import "ImageResizeOperation.h"
#import "UIImage+Resize.h"
#import "UIImageView+AFNetworking.h"
#import "UserObject.h"
#import "HGCheckCountryCode.h"
#import "EDStarRating.h"
#import "CommonUtils.h"

#define DISPLAY_PHOTO_HEIGHT 80
#define DISPLAY_PHOTO_WIDTH  90

#define ROOM_HINT_TEXT NSLocalizedString(@"Please select room preference", nil)
#define CAR_VERHICLE_HINT_TEXT NSLocalizedString(@"Please select preferred vehicle", nil)
#define CAR_COMPANY_HINT_TEXT NSLocalizedString(@"Please enter preferred car rental company", nil)

@interface OptionalDetailsView (){
    UIImagePickerController *pickerLibrary;
    AppDelegate* appDelegate;
    NSOperationQueue *queue;
    NSMutableArray *selectedRoom;
    NSMutableArray *selectedPreferredVerhicle;
    NSMutableArray *selectedPreferredCompany;
    WSGetMyPreferences* wsGetMyPreference;
    enum REQUEST_TYPE requestType;
    UIDatePicker *datepickerCheckIn;
    UIDatePicker *datepickerCheckOut;
    UIDatePicker *datepickerReservation;
    UIDatePicker *timepickerReservation;
}
@property (strong, nonatomic) IBOutlet UIButton *btPreferredVerhicle;
@property (strong, nonatomic) IBOutlet UIButton *btPreferredCarCompany;
@property (strong, nonatomic) IBOutlet CustomTextField *txtMaximumPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckOutDate;
@property (strong, nonatomic) IBOutlet UITextField *txtCheckInDate;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationDate;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationTime;

@property (strong, nonatomic) IBOutlet ErrorToolTip *icPhoneError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icEmailError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icReservationError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icRoomError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icAdultError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icAnyRequirementError;


@property (strong, nonatomic) IBOutlet UIView *viewBreakfastWifi;
@property (strong, nonatomic) IBOutlet UIButton *btWithBreakfast;
@property (strong, nonatomic) IBOutlet UIButton *btWithWifi;

@property (weak, nonatomic) IBOutlet UILabel *lbCheckOut;
@property (weak, nonatomic) IBOutlet UILabel *lbCheckIn;
@property (weak, nonatomic) IBOutlet UILabel *lbPreferredVerhicle;
@property (weak, nonatomic) IBOutlet UILabel *lbPrefferedCarCompany;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationDate;
@property (weak, nonatomic) IBOutlet UILabel *lbReservationTime;
@property (weak, nonatomic) IBOutlet UILabel *lbAdult;
@property (weak, nonatomic) IBOutlet UILabel *lbChildren1;
@property (weak, nonatomic) IBOutlet UILabel *lbRoom;
@property (weak, nonatomic) IBOutlet UILabel *lbSmoking;
@property (weak, nonatomic) IBOutlet UILabel *lbWitBreakfast;
@property (weak, nonatomic) IBOutlet UILabel *lbWithWifi;
@property (weak, nonatomic) IBOutlet UILabel *lbMaxPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbSpecialRequiment;
@property (weak, nonatomic) IBOutlet UILabel *lbAttachPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbPersonalInfo;
@property (weak, nonatomic) IBOutlet UILabel *lbResevationName;
@property (weak, nonatomic) IBOutlet UILabel *lbSelectMailAndPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbPhone;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbLoyalty;
@property (weak, nonatomic) IBOutlet UILabel *lbMemeberShipNo;





@end

@implementation OptionalDetailsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"OptionalDetailsView" owner:self options:nil]
                 objectAtIndex:0];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
        [self setUpView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        self.view.translatesAutoresizingMaskIntoConstraints = false;
        [self.view.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:0.0].active = YES;
        [self.view.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:0.0].active = YES;
        [self.view.topAnchor constraintEqualToAnchor:self.topAnchor constant:0.0].active = YES;
        [self.view.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0.0].active = YES;

        [self setUpView];
        return self;
    }
    
    return self;
}

-(void) setUpView{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self displayUserInfo];
    [self displayPaxNumber:0];
    [self displayAdultPaxNumber:1];
    [self displayKidsPaxNumber:0];
    
    [_btPreferredCarCompany setTitle:CAR_COMPANY_HINT_TEXT forState:UIControlStateNormal];
    [_btPreferredVerhicle setTitle:CAR_VERHICLE_HINT_TEXT forState:UIControlStateNormal];
    [_btRoomType setTitle:ROOM_HINT_TEXT forState:UIControlStateNormal];
    [_btArrowViewDetails setSelected:NO];
    [_viewOptionalHeader setHidden:YES];
    //[_viewDetails setHidden:YES];
    
    _txtPax.delegate = self;
    _txtKidsPax.delegate = self;
    _txtAdultsPax.delegate = self;
    _txtSpecialMessage.delegate = self;
    _txtLoyaltyProgram.delegate = self;
    _txtAlternateEmail.delegate = self;
    _txtContactPhone.delegate = self;
    _txtMaximumPrice.delegate = self;
    _txtCheckInDate.delegate = self;
    _txtCheckOutDate.delegate = self;
    
    [self setTextForView];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [self closeAllKeyboardAndPicker];
    if(_delegate){
        [_delegate touchinDetailsView];
    }
}

-(void) applyRequestType:(enum REQUEST_TYPE) type{
    requestType = type;
    //[self getPreferences];
    [self setUpViewBaseOnRequestType:type];
    
}

-(void) setUpViewBaseOnRequestType:(enum REQUEST_TYPE) type{
    _viewCheckInOutDate.hidden = YES;
    _viewCombineAdultAndKids.hidden= YES;
    _viewNormalPax.hidden = YES;
    _viewLoyalty.hidden = YES;
    _viewRooml.hidden = YES;
    _viewSmoking.hidden = YES;
    _viewPreferredVerhicle.hidden = YES;
    _viewPrefferedCarCompany.hidden = YES;
    _viewBudgetRange.hidden = YES;
    _viewReservations.hidden = YES;
    _viewUploadPhoto.hidden = YES;
    _viewMembership.hidden = YES;
    _viewBreakfastWifi.hidden = YES;
    _viewMobileNumber.hidden = YES;
    
    switch (type) {
        case HOTEL_REQUEST:
            _viewCombineAdultAndKids.hidden= NO;
            _viewRooml.hidden = NO;
            _viewSmoking.hidden = NO;
            _viewLoyalty.hidden = NO;
            _viewMembership.hidden = NO;
            //_viewBreakfastWifi.hidden = NO;
            //_viewNormalPax.hidden = NO;
            //[_lbKidsUnder12 setText:NSLocalizedString(@"No. of Rooms", nil)];
            //_txtPax.text = @"1";
            break;
        case HOTEL_RECOMMEND_REQUEST:
            _viewCombineAdultAndKids.hidden = NO;
            _viewCheckInOutDate.hidden = NO;
            _viewRooml.hidden = NO;
            _viewSmoking.hidden = NO;
            _viewLoyalty.hidden = NO;
            _viewMembership.hidden = NO;
            //_viewBreakfastWifi.hidden = NO;
            [self setUpCheckInCheckOutDate];
            
            break;
        case GOLF_REQUEST:
           
            break;
        case CAR_RENTAL_REQUEST:
            _viewPrefferedCarCompany.hidden = NO;
            _viewPreferredVerhicle.hidden = NO;
            _viewBudgetRange.hidden = NO;
            _txtMaximumPrice.placeholder = NSLocalizedString(@"Please enter your maximum budget car_rental", nil);
            break;
        case CAR_TRANSFER_REQUEST:
            
            break;
        case GOURMET_REQUEST:
            _viewCombineAdultAndKids.hidden = NO;
            break;
        case ENTERTAINMENT_REQUEST:
            _viewNormalPax.hidden = NO;
            _lbKidsUnder12.attributedText = [self setRequireString:NSLocalizedString(@"No. of Tickets", nil)];
            _txtPax.text = @"1";
            _normalPax = 1;
            break;
        case OTHER_REQUEST:
            _viewSpecialRequirement.hidden = YES;
            _viewDetails.hidden = NO;
            _viewOptionalHeader.hidden = YES;
            break;
        case GOURMET_RECOMMEND_REQUEST:
            _viewCombineAdultAndKids.hidden = NO;
            //[self setUpReservationDateTime];
            break;
        default:
            break;
    }

}

-(void) getPreferences{
    wsGetMyPreference = [[WSGetMyPreferences alloc] init];
    wsGetMyPreference.delegate = self;
    wsGetMyPreference.category = [self programCategoryFromRequestType:requestType];
    [wsGetMyPreference getMyPreference];
}


-(void) displayUserInfo{
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    UserObject *user = [appDelegate getLoggedInUser];
    if(user){
        _txtReservationName.text = user.fullName;
        _txtAlternateEmail.text = user.email;
        [_btCountryCode setTitle:[NSString stringWithFormat:@"+%@",user.countryCode] forState:UIControlStateNormal];
        [_txtContactPhone setText:user.shortMobileNumber];
    }
}

-(void) displayPaxNumber:(NSInteger) pax{
    _normalPax = pax;
    _txtPax.text = [NSString stringWithFormat:@"%ld ", _normalPax];
}

-(void) displayKidsPaxNumber:(NSInteger) pax{
    _kidsPax = pax;
    _txtKidsPax.text = [NSString stringWithFormat:@"%ld ", _kidsPax];
}

-(void) displayAdultPaxNumber:(NSInteger) pax{
    _adultPax = pax;
    _txtAdultsPax.text = [NSString stringWithFormat:@"%ld ", _adultPax];
}

- (IBAction)actionSelectRoom:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
   SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = ROOM;
    controller.delegate = self;
    controller.maxCount = 1;
    controller.isCloseAfterClick = YES;
    controller.selectedValues = [[NSMutableArray alloc] initWithArray:selectedRoom];
    controller.selectedKey = [[NSMutableArray alloc] initWithArray:_selectedRoomKey];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}

- (IBAction)actionOpenCloseDetails:(id)sender {
    [_btArrowViewDetails setSelected:!_btArrowViewDetails.selected];
    [self rotateButton:_btArrowViewDetails];
    [self transitionView:_viewDetails to:_btArrowViewDetails.selected];
    [self closeAllKeyboardAndPicker];
    if(_delegate){
        [_delegate actionOptionalDetailsOpen:_btArrowViewDetails.selected];
    }
    
}

- (IBAction)actionCaptureImage:(id)sender {
    [self closeAllKeyboardAndPicker];
    [self goToTakePicture];
}

- (IBAction)actionDecreasePax:(id)sender {
    if(_normalPax>1){
        [self displayPaxNumber:_normalPax-1];
    }
}

- (IBAction)actionIncreasePax:(id)sender {
    [self displayPaxNumber:_normalPax+1];
}

- (IBAction)actionDecreaseAdults:(id)sender {
    if(_adultPax>1){
        [self displayAdultPaxNumber:_adultPax-1];
    }
}

- (IBAction)actionIncreaseAdult:(id)sender {
    _icAdultError.hidden = YES;
    [self displayAdultPaxNumber:_adultPax+1];
}

- (IBAction)actionDecreaseKids:(id)sender {
    if(_kidsPax>0){
        [self displayKidsPaxNumber:_kidsPax-1];
    }
}

- (IBAction)actionIncreaseKids:(id)sender {
    [self displayKidsPaxNumber:_kidsPax+1];
}
- (IBAction)actionEmailiClicked:(id)sender {
    _btRadioPhone.selected = NO;
    _btRadioEmail.selected = YES;
    _viewEmail.hidden = NO;
    _viewMobileNumber.hidden= YES;
    [self closeAllKeyboardAndPicker];
    if(_delegate){
        [_delegate touchinDetailsView];
    }
}

- (IBAction)actionPhoneClicked:(id)sender {
    
    _btRadioPhone.selected = YES;
    _btRadioEmail.selected = NO;
    _viewEmail.hidden = YES;
    _viewMobileNumber.hidden= NO;
    [self closeAllKeyboardAndPicker];
    if(_delegate){
        [_delegate touchinDetailsView];
    }
}
- (IBAction)actionBothClicked:(id)sender {
    _btRadioPhone.selected = NO;
    _btRadioEmail.selected = NO;
    _viewEmail.hidden = NO;
    _viewMobileNumber.hidden= NO;
    [self closeAllKeyboardAndPicker];
    if(_delegate){
        [_delegate touchinDetailsView];
    }
}

- (IBAction)actionPrefferedVerhicle:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = RETAL_VERTICLE;
    controller.delegate = self;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedPreferredVerhicle] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:_selectedPreferredVerhicleKeys];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}

- (IBAction)actionPrefferedCarCompany:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubFilterController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubFilter"];
    controller.dataType = COMPANY;
    controller.delegate = self;
    controller.selectedValues =[[NSMutableArray alloc] initWithArray:selectedPreferredCompany] ;
    controller.selectedKey =[[NSMutableArray alloc] initWithArray:_selectedPreferredCompanyKeys];
    [[self currentNavigationController] pushViewController:controller animated:YES];
}

#pragma mark selec photo
-(void)goToTakePicture
{
    pickerLibrary = [[UIImagePickerController alloc] init];
    pickerLibrary.delegate = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Take Photo"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [alert popoverPresentationController];
                                                              [self displayImagePickerType:UIImagePickerControllerSourceTypeCamera];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Choose Existing"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [alert popoverPresentationController];
                                                               [self displayImagePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                                                           }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [alert popoverPresentationController];
                                                           }];
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:cancelAction];
    
    [[self currentNavigationController] presentViewController:alert animated:YES completion:nil];
}

-(void) displayImagePickerType:(UIImagePickerControllerSourceType) type{
    pickerLibrary = [[UIImagePickerController alloc] init];
    pickerLibrary.delegate = self;
    pickerLibrary.sourceType = type;
    [[self currentNavigationController] presentViewController:pickerLibrary animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
    // check if image type is PNG or JPG
    BOOL isAllowImageExtension = YES;
    NSString *imageExtension = kJpegPhotoExt;
    NSURL *imageReferenceURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    if (imageReferenceURL) {
        NSString *imageReference = [imageReferenceURL description];
        if(imageReference)
        {
            if (imageReference != nil || ![imageReference isKindOfClass:[NSNull class]]) {
                NSRange extensionRange = [imageReference rangeOfString:@"&ext=" options:NSCaseInsensitiveSearch];
                if (extensionRange.location + extensionRange.length + 3 < imageReference.length) {
                    NSString *extension = [imageReference substringWithRange:NSMakeRange(extensionRange.location + extensionRange.length, 3)];
                    if (extension != nil) {
                        if (![[extension lowercaseString] isEqualToString:kPngPhotoExt] &&
                            ![[extension lowercaseString] isEqualToString:kJpegPhotoExt]) {
                            isAllowImageExtension = NO;
                        }
                        imageExtension = extension;
                    }
                }
            }
        }
    }
    
    if (!isAllowImageExtension) {
        NSLog(@"Photo extension is not allowed");
        showAlertOneButton([self currentNavigationController], @"Aspire Lifestyle", @"Photo extension is not allowed!", @"OK");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // get image
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self resizeImage:image extension:imageExtension];
    
    image = nil;
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)resizeImage:(UIImage*)image extension:(NSString*)imageExtension
{
    // resize image
    queue = [[NSOperationQueue alloc]init];
    ImageResizeOperation *imageResizer = [[ImageResizeOperation alloc] initWithImage:image completionHandler:^(PhotoResizedInfo* photoResizedInfo) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self resizeImageDone:photoResizedInfo];
            
        });
        
        queue = nil;   // we are finished with the queue and our ParseOperation
    } scaledToMaxSize:CGSizeMake(600, 400) maxBytesLength:kPhotoUploadMaxSize extension:imageExtension];
    
    imageResizer.errorHandler = ^(NSError *parseError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            [self resizeImageFailed:parseError];
            NSLog(@"Photo has been resized failed");
            showAlertOneButton([self currentNavigationController], @"Aspire Lifestyle", @"Photo has been resized failed! Please try again.", @"OK");
        });
    };
    
    [queue addOperation:imageResizer];
}

-(void)resizeImageDone:(PhotoResizedInfo*)photoResizedInfo
{
    if (photoResizedInfo.imageData == nil || photoResizedInfo.image == nil) {
        NSLog(@"Photo has been resized failed");
        showAlertOneButton([self currentNavigationController], @"Aspire Lifestyle", @"Photo has been resized failed! Please try again.", @"OK");
        return;
    }
    [self addImageToScrollView:photoResizedInfo];
}

-(void) addImageToScrollView:(PhotoResizedInfo*) photo{
   /* UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake((DISPLAY_PHOTO_WIDTH+10)*photos.count, 10, DISPLAY_PHOTO_WIDTH, DISPLAY_PHOTO_HEIGHT)];
    imv.contentMode = UIViewContentModeScaleAspectFill;
    [imv addConstraint:[NSLayoutConstraint constraintWithItem:imv
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute: NSLayoutAttributeNotAnAttribute
                                                   multiplier:1
                                                     constant:DISPLAY_PHOTO_WIDTH]];
    */
    [_imvPhoto setImage:photo.image];
    _viewImageAndButton.hidden = NO;
    _selectedPhoto = photo;
    [self displayPhotoCount];
}

-(void) rotateButton:(UIButton*) bt{
    [UIView beginAnimations:@"rotate" context:nil];
    [UIView setAnimationDuration:.5f];
    if( CGAffineTransformEqualToTransform( bt.imageView.transform, CGAffineTransformIdentity ) )
    {
        bt.imageView.transform = CGAffineTransformMakeRotation(M_PI);
    } else {
        bt.imageView.transform = CGAffineTransformIdentity;
    }
    [UIView commitAnimations];
}

-(void)transitionView:(UIView*)hview to:(Boolean) status{
   [UIView transitionWithView:hview
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        hview.hidden = !status;
                    }
                    completion:NULL];
    
}

-(void) closeAllKeyboardAndPicker{    
    [_txtContactPhone resignFirstResponder];
    [_txtAlternateEmail resignFirstResponder];
    [_txtPax resignFirstResponder];
    [_txtLoyaltyProgram resignFirstResponder];
    [_txtSpecialMessage resignFirstResponder];
    [_txtCheckOutDate endEditing:YES];
    [_txtCheckInDate endEditing:YES];
    [_txtMaximumPrice resignFirstResponder];
    [_txtReservationDate endEditing:YES];
    [_txtReservationTime endEditing:YES];
    [_txtReservationName resignFirstResponder];
    [_txtMembershipNo resignFirstResponder];
    [_txtRetalCompany resignFirstResponder];
    
    [self adjustChechOutDate];
}

-(SlideNavigationController*) currentNavigationController{
    if(!appDelegate){
        appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    }
    return appDelegate.rootVC;
}
- (IBAction)actionCCNumber:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    CCNumberController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SelectionList"];
    controller.data = getCCNumberList();
    controller.dataType = 2;
    controller.delegate = self;
    controller.selectedValue = _btCountryCode.currentTitle;
    [[self currentNavigationController] pushViewController:controller animated:YES];
    
}

- (IBAction)actionDeletePhoto:(id)sender {
    _selectedPhoto = nil;
    _viewImageAndButton.hidden = YES;
    [_imvPhoto setImage:nil];
    [self displayPhotoCount];
}

- (IBAction)actionWithBreakfastClicked:(id)sender {
    [((UIButton*)sender) setSelected:!((UIButton*)sender).selected];
}

- (IBAction)actionWithWifiClicked:(id)sender {
    [((UIButton*)sender) setSelected:!((UIButton*)sender).selected];
}


- (void) pickCCNumber:(NSString*) number forType:(NSInteger) type{
    if (type == 2){
        if(number!=nil && number.length>0){
            [_btCountryCode setSelected:NO];
            [_btCountryCode setTitle:number forState:UIControlStateNormal];
        }else{
            [_btCountryCode setSelected:YES];
            [_btCountryCode setTitle:@"CC" forState:UIControlStateNormal];
        }
    }
}

-(void)displayPhotoCount{
    [_lbTotalPhotos setText:[NSString stringWithFormat:@"(%d)", (_selectedPhoto ? 1 : 0)]];
}

#pragma textField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.tag == _txtAlternateEmail.tag){
        _icEmailError.hidden = checkEmailValid(_txtAlternateEmail.text);
        if(_txtAlternateEmail.text.length==0){
            _icEmailError.errorMessage = MANDATORY_BLANK;
        }
    }
    return textField.tag !=0;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag == _txtContactPhone.tag){
        _icPhoneError.hidden = YES;
    } else if(textField.tag == _txtAlternateEmail.tag){
        if(textField.text.length > 0 && !checkEmailValid(textField.text)){
            _icEmailError.hidden = NO;
        } else {
        _icEmailError.hidden = YES;
        }
    } else if(textField.tag == _txtReservationName.tag){
        _icReservationError.hidden = YES;
    }
    if(_delegate){
        [_delegate textFieldBeginEdit:textField];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _lbHolderAny.hidden = YES;
    _icAnyRequirementError.hidden = YES;
    if(_delegate){
        [_delegate textFieldBeginEdit:[textView superview]];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView.text.length==0){
        _lbHolderAny.hidden = NO;
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    _icAnyRequirementError.hidden = YES;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if([self.view viewWithTag:textField.tag+1]){
        if([[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextView class]])
            [(UITextView*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        else if( [[self.view viewWithTag:textField.tag+1] isKindOfClass:[UITextField class]]){
             [(UITextField*)[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
        }
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}


#pragma end
-(void)setRoomPreferen:(NSString*)room{
    if(room && room.length>0){
        selectedRoom = [[NSMutableArray alloc] initWithObjects:room, nil];
        _selectedRoomKey = [[NSMutableArray alloc] initWithObjects:room, nil];
    } else {
        selectedRoom = [NSMutableArray array];
        _selectedRoomKey = [NSMutableArray array];
    }
    
    [_btRoomType setSelected:NO];
    [self.btRoomType setTitle:convertArrayToString(selectedRoom) forState:UIControlStateNormal];
    if(self.btRoomType.currentTitle.length == 0){
        [_btRoomType setSelected:YES];
        [self.btRoomType setTitle:ROOM_HINT_TEXT forState:UIControlStateNormal];
    }
}
-(void)setLoyalty:(NSString*)program withNo:(NSString*)number{
    _txtLoyaltyProgram.text = program;
    _txtMembershipNo.text = number;
}

#pragma Sub filter delegate
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type{
    if(type == ROOM){
        _icRoomError.hidden = YES;
        selectedRoom = values;
        _selectedRoomKey = key;
        [_btRoomType setSelected:NO];
        [self.btRoomType setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(self.btRoomType.currentTitle.length == 0){
            [_btRoomType setSelected:YES];
            [self.btRoomType setTitle:ROOM_HINT_TEXT forState:UIControlStateNormal];
        }
    } else if (type == COMPANY){
        selectedPreferredCompany = values;
        _selectedPreferredCompanyKeys = key;
        [self.btPreferredCarCompany setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(self.btPreferredCarCompany.currentTitle.length == 0){
            [self.btPreferredCarCompany setTitle:CAR_COMPANY_HINT_TEXT forState:UIControlStateNormal];
        }
    } else if (type == RETAL_VERTICLE){
        selectedPreferredVerhicle = values;
        _selectedPreferredVerhicleKeys = key;
        [_btPreferredVerhicle setSelected:NO];
        [self.btPreferredVerhicle setTitle:convertArrayToString(values) forState:UIControlStateNormal];
        if(self.btPreferredVerhicle.currentTitle.length == 0){
            [_btPreferredVerhicle setSelected:YES];
            [self.btPreferredVerhicle setTitle:CAR_VERHICLE_HINT_TEXT forState:UIControlStateNormal];
        }
    }
}
#pragma end

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_MY_PREFERENCE){
        [self displayPreferenceByRequestType];
    } else if (ws.task == WS_GET_RATING){
        
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(errorCode == 600){
        return;
    }
}

-(void) displayPreferenceByRequestType{
    if(requestType == HOTEL_REQUEST || requestType == HOTEL_RECOMMEND_REQUEST){
        NSArray* arr= [wsGetMyPreference.pData objectForKey:@"RoomTypes"];
        if(arr.count>0){
            selectedRoom = [[NSMutableArray alloc] init];
            _selectedRoomKey = [[NSMutableArray alloc] init];
            // in this page just get the first item
            NSDictionary* dict = [arr objectAtIndex:0];
            [selectedRoom addObject:[dict stringForKey:@"Value"]];
            [_selectedRoomKey addObject:[dict stringForKey:@"Key"]];
            
            [self.btRoomType setTitle:convertArrayToString(selectedRoom) forState:UIControlStateNormal];
        }
        
        NSDictionary* smokingDict = [wsGetMyPreference.pData dictionaryForKey:@"SmookingRoom"];
        NSString* value = [smokingDict stringForKey:@"Value"];
        if(isEqualIgnoreCase(value ,@"yes")){
            self.swSmoking.on = YES;
        }else{
            self.swSmoking.on = NO;
        }
        
        NSArray* arrLoyalty= [wsGetMyPreference.pData objectForKey:@"LoyalProgrammes"];
        NSString* keyBaseOnType = [self programCategoryFromRequestType:requestType];
        if(arrLoyalty.count>0){
            for (int i = 0; i<arrLoyalty.count; i++) {
                NSDictionary* dict= [arrLoyalty objectAtIndex:i];
                NSString* category= [dict stringForKey:@"Category"];
                if([category isEqualToString:keyBaseOnType]){
                    self.txtLoyaltyProgram.text = [dict stringForKey:@"Name"];
                }
            }
        }
        
        if(requestType == HOTEL_RECOMMEND_REQUEST){
            NSDictionary* starDict = [wsGetMyPreference.pData dictionaryForKey:@"RatingForApp"];
            NSString* strStar = [starDict stringForKey:@"Value"];
            NSArray* arrTemp = [strStar componentsSeparatedByString:@" "];
            if(arrTemp.count>0){
              //  _starRatingControl.rating = [[arrTemp objectAtIndex:0] intValue];
            }
        }
    } else if(requestType == CAR_RENTAL_REQUEST){
        NSArray* arrVer= [wsGetMyPreference.pData objectForKey:@"RentalVehicleTypes"];
        if(arrVer.count>0){
            selectedPreferredVerhicle = [[NSMutableArray alloc] init];
            _selectedPreferredVerhicleKeys = [[NSMutableArray alloc] init];
            for (int i = 0; i<arrVer.count; i++) {
                NSDictionary* dict = [arrVer objectAtIndex:i];
                [selectedPreferredVerhicle addObject:[dict stringForKey:@"Value"]];
                [_selectedPreferredVerhicleKeys addObject:[dict stringForKey:@"Key"]];
            }
            [_btPreferredVerhicle setTitle:convertArrayToString(selectedPreferredVerhicle) forState:UIControlStateNormal];
        }
        NSArray* arrCom= [wsGetMyPreference.pData objectForKey:@"RentalCompanies"];
        if(arrCom.count>0){
            selectedPreferredCompany = [[NSMutableArray alloc] init];
            _selectedPreferredCompanyKeys = [[NSMutableArray alloc] init];
            for (int i = 0; i<arrCom.count; i++) {
                NSDictionary* dict = [arrCom objectAtIndex:i];
                [selectedPreferredCompany addObject:[dict stringForKey:@"Value"]];
                [_selectedPreferredCompanyKeys addObject:[dict stringForKey:@"Key"]];
            }
            [_btPreferredCarCompany setTitle:convertArrayToString(selectedPreferredCompany) forState:UIControlStateNormal];
        }
        
    }
    
}

-(NSString*) programCategoryFromRequestType:(enum REQUEST_TYPE) type{
    switch (type) {
        case HOTEL_REQUEST:
            return kHotelPreference;
        case GOLF_REQUEST:
            return kGolfPreference;
        case CAR_RENTAL_REQUEST:
        case CAR_TRANSFER_REQUEST:
            return kCarPreference;
        case GOURMET_REQUEST:
            return kGourmetPreference;
        case ENTERTAINMENT_REQUEST:
        case OTHER_REQUEST:
            return kOrtherPreference;
        default:
            return kOrtherPreference;
    }
}

-(void) exportValues{
    _phoneNumber = nil;
    _countryCode = [_btCountryCode.currentTitle stringByReplacingOccurrencesOfString:@"+" withString:@""];
    if([_countryCode isEqualToString:@"CC"]){
        _icPhoneError.hidden = false;
        _icPhoneError.errorMessage = NSLocalizedString(@"Please select the country code",nil);
        _countryCode = nil;
    } else {
        NSString* value = _txtContactPhone.text;
        if(checkPhoneValid(value)){
            _phoneNumber = [NSString stringWithFormat:@"%@%@", _countryCode, value];
        } else {
            _countryCode = nil;
            if(value.length==0){
                _icPhoneError.errorMessage = MANDATORY_BLANK;
            }else{
                _icPhoneError.errorMessage = NSLocalizedString(@"Please provide a valid number", nil);
            }
            _icPhoneError.hidden = false;
            
        }
    }
    
    NSString* value =  _txtAlternateEmail.text ;
    if((_btRadioEmail.selected) && (!value || value.length==0)){
        _icEmailError.hidden = NO;
        _email = nil;
        if(value.length==0){
            _icEmailError.errorMessage = MANDATORY_BLANK;
        }
    } else {
        if(checkEmailValid(value)){
            _email = value;
        }else {
            _email = nil;
            _icEmailError.hidden = NO;
            _icEmailError.errorMessage = NSLocalizedString(@"Please enter a valid email address", nil);
        }
        
    }
    
    NSString* reservation = _txtReservationName.text;
    if(isValidValue(reservation)){
        if (isNotIncludedSpecialCharacters(reservation)) {
             _reservationName = reservation;
        } else {
            [_icReservationError setHidden:NO];
            _icReservationError.errorMessage = INCLUDE_UNSUPPORT_CHAR_ERR;
        }
    }else{
        _icReservationError.hidden = NO;
        if(reservation.length==0){
            _icReservationError.errorMessage = MANDATORY_BLANK;
        }
    }
    
    _adultPax = [_txtAdultsPax.text integerValue];
    if(_adultPax<=0){
        _icAdultError.hidden = NO;
        _adultPax = 0;
        _icAdultError.errorMessage = MANDATORY_BLANK;
    }
    _kidsPax = [_txtKidsPax.text integerValue];
    _normalPax = [_txtPax.text integerValue];
    _loyaltyMembershipProgram = _txtLoyaltyProgram.text;
    _loyaltyMembershipNo = _txtMembershipNo.text;
    _rentalCompany = _txtRetalCompany.text;
    _isSmoking = _swSmoking.isOn;
    
   
    _isContactPhone = _btRadioPhone.selected;
    _isContactEmail = _btRadioEmail.selected;
    
    _isWithWifi = _btWithWifi.selected;
    _isWithBreakfast = _btWithBreakfast.selected;
    
    _specialMessage = _txtSpecialMessage.text;
    if(!isNotIncludedSpecialCharacters(_specialMessage)){
        _icAnyRequirementError.hidden = NO;
        _icAnyRequirementError.errorMessage = INCLUDE_UNSUPPORT_CHAR_ERR;
    } else {
        _icAnyRequirementError.hidden = YES;
    }
    
    
    if(_selectedRoomKey.count>0){
        _roomType = convertArrayToString(_selectedRoomKey);
    }else {
        _roomType = nil;
        _icRoomError.hidden = NO;
        _icRoomError.errorMessage = MANDATORY_BLANK;
    }
    
    
    if(datepickerCheckOut && datepickerCheckIn){
        _checkInDate = formatDate(datepickerCheckIn.date, DATETIME_SEND_REQUEST_FORMAT);
        _checkOutDate = formatDate(datepickerCheckOut.date, DATETIME_SEND_REQUEST_FORMAT);
    }
    
    if(datepickerReservation && timepickerReservation){
        _reservationDate = formatDate(datepickerReservation.date, DATE_SEND_REQUEST_FORMAT);
        _reservationsTime = formatDate(timepickerReservation.date, TIME_SEND_REQUEST_FORMAT);
    }
    _maximumPrice = _txtMaximumPrice.text;
    
}

-(void) displayAdultKidsView:(BOOL) display{
    _viewCombineAdultAndKids.hidden = !display;
    _viewNormalPax.hidden = display;
}

-(void) displayGourmetView{
    _viewNormalPax.hidden = YES;
    _viewRooml.hidden = YES;
    _viewLoyalty.hidden = YES;
    _viewSmoking.hidden = YES;
}

-(void)setCarPreference:(NSString*)verhicle withNo:(NSString*)companies{
    NSArray* arr= convertStringToArray(verhicle);
    if(arr.count>0){
        selectedPreferredVerhicle = [[NSMutableArray alloc] initWithArray:arr];
        _selectedPreferredVerhicleKeys = [[NSMutableArray alloc] initWithArray:arr];
        [self.btPreferredVerhicle setSelected:NO];
        [self.btPreferredVerhicle setTitle:((selectedPreferredVerhicle && selectedPreferredVerhicle.count>0) ? convertArrayToString(selectedPreferredVerhicle):CAR_VERHICLE_HINT_TEXT) forState:UIControlStateNormal];
    }
    self.txtRetalCompany.text =companies;
}
-(void) showDetailsView:(BaseBookingDetailsObject*) details{
    _txtReservationName.text = details.guestName;
    _btRadioEmail.selected = details.isContactEmail;
    _btRadioPhone.selected = details.isContactPhone;
    _viewMobileNumber.hidden = !(_btRadioPhone.selected);
    _viewEmail.hidden = !(_btRadioEmail.selected );
    _swSmoking.on = details.isSmoking;
    _btWithWifi.selected = details.isWithWifi;
    _btWithBreakfast.selected = details.isWithBreakfast;
    [self isValidMobileNumber:details.mobileNumber];
    _txtAlternateEmail.text = details.email;
    
    if(details.parsedRequestType == HOTEL_REQUEST){
        [self displayPaxNumber:details.noOfRooms];
    } else {
        [self displayPaxNumber:details.kidsPax];
    }
    
    [self displayKidsPaxNumber:details.kidsPax];
    [self displayAdultPaxNumber:details.adulsPax];
    _txtLoyaltyProgram.text = details.loyaltyMembership;
    _txtMembershipNo.text = details.loyaltyMemberNo;
    _txtSpecialMessage.text = details.specialRequirement;
    if(_txtSpecialMessage.text.length>0){
        _lbHolderAny.hidden = YES;
    }
    
    if(details.roomTypeValue.length>0 && details.roomTypeKey.length>0){
        [_btRoomType setSelected:NO];
        [_btRoomType setTitle:details.roomTypeValue forState:UIControlStateNormal];
        _selectedRoomKey = [NSMutableArray arrayWithObject:details.roomTypeKey];
        selectedRoom = _selectedRoomKey;
    }
    
    /*
    if(details.attachedPhotoUrl && details.attachedPhotoUrl.length>0){
        [_imvPhoto setImageWithURL:[NSURL URLWithString:details.attachedPhotoUrl] placeholderImage:nil];
        _viewImageAndButton.hidden = NO;
        _selectedPhoto = [[PhotoResizedInfo alloc] init];
        _selectedPhoto.imageData = UIImagePNGRepresentation(_imvPhoto.image);
        [self displayPhotoCount];
    }*/
}


-(BOOL)isValidMobileNumber:(NSString*)checkString
{
    checkString = [checkString stringByReplacingOccurrencesOfString:@"+" withString:@""];
    if ((checkString == nil) || (checkString.length < 8)) {
        return NO;
    }
    
    NSString* mobile= checkString;
    
    HGCheckCountryCode* checkCode= [[HGCheckCountryCode alloc] init];
    
    NSString* tempCode = @"";
    NSString* countrycode =@"";
    
    for (int i =1; i <5; i++) {
        NSString* temp = [mobile substringToIndex:i];
        if([temp isEqualToString:@"1"]){
            NSString* code = [mobile substringToIndex:4];
            if([checkCode checkCountryCode:code]){
                countrycode = [NSString stringWithFormat:@"%@",code];
            }else{
                countrycode = [NSString stringWithFormat:@"1"];
            }
            break;
        }else{
            tempCode = [NSString stringWithFormat:@"%@",temp];
            if([checkCode checkCountryCode:tempCode]){
                countrycode = [NSString stringWithFormat:@"%@",tempCode];
                break;
            }
        }
    }
    NSString* checkPhone = [mobile substringFromIndex:countrycode.length];
    _txtContactPhone.text = checkPhone;
    [self.btCountryCode setTitle:[@"+" stringByAppendingString:countrycode] forState:UIControlStateNormal];
    
    return NO;
}

-(void) showCarRentalDetails:(CarRentalDetailsObject*) details{
    [self showDetailsView:details];
    
    selectedPreferredCompany = [details prefferedRentalValues];
    _selectedPreferredCompanyKeys = [details prefferedRentalKeys];
    selectedPreferredVerhicle = [details prefferedVerhicleValues];
    _selectedPreferredVerhicleKeys = [details prefferedVerhicleKeys];
    [_btPreferredVerhicle setSelected:NO];
    [self.btPreferredVerhicle setTitle:((selectedPreferredVerhicle && selectedPreferredVerhicle.count>0) ? convertArrayToString(selectedPreferredVerhicle):CAR_VERHICLE_HINT_TEXT) forState:UIControlStateNormal];
    if(selectedPreferredVerhicle.count==0){
        [_btPreferredVerhicle setSelected:YES];
    }
    _txtRetalCompany.text = convertArrayToString(selectedPreferredCompany);
    _txtMaximumPrice.text = details.maximumPrice;
    
}
-(void) showGourmetRecommendDetails:(GouetmetRecommendDetailsObj*) details{
    
    [self showDetailsView:details];
    
    _txtMaximumPrice.text = details.MaximumPrice;
    
    if(datepickerReservation && timepickerReservation && details.reservationDate && details.reservationTime){
        datepickerReservation.date = details.reservationDate;
        timepickerReservation.date = details.reservationTime;
        _txtReservationDate.text = formatDateToString(datepickerReservation.date, DATE_FORMAT);
        _txtReservationTime.text = formatDateToString(timepickerReservation.date, TIME_FORMAT);
    }
}
-(void) showHotelRecommendDetails:(HotelRequestDetailsObject*) details{
    [self showDetailsView:details];
    
    if(datepickerCheckIn && datepickerCheckOut && details.checkInDate && details.checkOutDate){
        datepickerCheckIn.date = details.checkInDate;
        datepickerCheckOut.date = details.checkOutDate;
        datepickerCheckOut.minimumDate = datepickerCheckIn.date;
        _txtCheckInDate.text = formatDateToString(datepickerCheckIn.date, DATE_FORMAT);
        _txtCheckOutDate.text = formatDateToString(datepickerCheckOut.date, DATE_FORMAT);
    }
}



-(void) setUpCheckInCheckOutDate{
    
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtCheckInDate.inputAccessoryView = toolbar;
    _txtCheckOutDate.inputAccessoryView = toolbar;
    
    //Check in date picker
    datepickerCheckIn = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerCheckIn setDatePickerMode:UIDatePickerModeDate];
    datepickerCheckIn.datePickerMode = UIDatePickerModeDate;
    [datepickerCheckIn setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerCheckIn addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerCheckIn.tag = _txtCheckInDate.tag*10;
    _txtCheckInDate.inputView = datepickerCheckIn;
    _txtCheckInDate.delegate = self;
    datepickerCheckIn.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtCheckInDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckIn.date, DATE_FORMAT)];
    //end
    
    //Check out date picker
    datepickerCheckOut = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerCheckOut setDatePickerMode:UIDatePickerModeDate];
    datepickerCheckOut.datePickerMode = UIDatePickerModeDate;
    [datepickerCheckOut setMinimumDate:datepickerCheckIn.date];
    [datepickerCheckOut addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerCheckOut.tag = _txtCheckOutDate.tag*10;
    _txtCheckOutDate.inputView = datepickerCheckOut;
    _txtCheckOutDate.delegate = self;
    datepickerCheckOut.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*2];
    _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    //end
    
    
}

-(void) setUpReservationDateTime{
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    
    _txtReservationDate.inputAccessoryView = toolbar;
    _txtReservationTime.inputAccessoryView = toolbar;
    
    //Check in date picker
    datepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datepickerReservation setDatePickerMode:UIDatePickerModeDate];
    datepickerReservation.datePickerMode = UIDatePickerModeDate;
    [datepickerReservation setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [datepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    datepickerReservation.tag = _txtReservationDate.tag*10;
    _txtReservationDate.inputView = datepickerReservation;
    _txtReservationDate.delegate = self;
    datepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    
    timepickerReservation = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [timepickerReservation setDatePickerMode:UIDatePickerModeTime];
    timepickerReservation.datePickerMode = UIDatePickerModeTime;
    // [timepickerGolf setMinimumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]];
    [timepickerReservation addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    timepickerReservation.tag = _txtReservationTime.tag*10;
    _txtReservationTime.inputView = timepickerReservation;
    _txtReservationTime.delegate = self;
    timepickerReservation.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    _txtReservationTime.text = [self getTimebyDate:timepickerReservation.date];

}


-(NSString*)getTimebyDate:(NSDate*)pdate{
    NSString* time = @"";
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    [outputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *temp = [outputFormatter stringFromDate:pdate];
    
    NSDate *date = [outputFormatter dateFromString:temp];
    
    outputFormatter.dateFormat = @"hh:mm a";
    time = [outputFormatter stringFromDate:date];
    
    return  time;
}
-(void) dateTextField:(id)sender
{
    if(((UIView*)sender).tag == _txtCheckInDate.tag*10){
        _txtCheckInDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckIn.date, DATE_FORMAT)];
        
    } else if(((UIView*)sender).tag == _txtCheckOutDate.tag*10){
        _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
    } else if(((UIView*)sender).tag == _txtReservationDate.tag*10){
        _txtReservationDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerReservation.date, DATE_FORMAT)];
    }else if(((UIView*)sender).tag == _txtReservationTime.tag*10){
        _txtReservationTime.text = [NSString stringWithFormat:@"%@",formatDateToString(timepickerReservation.date, TIME_FORMAT)];
    }
    
}

-(void) adjustChechOutDate{
    if(datepickerCheckOut){
        if([datepickerCheckOut.date compare:datepickerCheckIn.date] != NSOrderedDescending
           || ([formatDateToString(datepickerCheckOut.date, DATE_FORMAT) isEqualToString:formatDateToString(datepickerCheckIn.date, DATE_FORMAT)])){
            datepickerCheckOut.date = [datepickerCheckIn.date dateByAddingTimeInterval:60*60*24*1];
            _txtCheckOutDate.text = [NSString stringWithFormat:@"%@",formatDateToString(datepickerCheckOut.date, DATE_FORMAT)];
        }
        [datepickerCheckOut setMinimumDate:[datepickerCheckIn.date dateByAddingTimeInterval:60*60*24*1]];
    }
}

- (void)dismiss
{
    [self closeAllKeyboardAndPicker];
}

-(void)setTextForView {
    
    _lbAdult.attributedText = [self setRequireString:NSLocalizedString(@"Adults (12 & above)", nil)];
    _lbResevationName.attributedText = [self setRequireString:NSLocalizedString(@"Reservation name", nil)];
    _lbSelectMailAndPhone.attributedText = [self setRequireString:NSLocalizedString(@"I would like to be contacted via", nil)];
    //_lbEmail.attributedText = [self setRequireString:NSLocalizedString(@"Email", nil)];
    _lbPhone.attributedText = [self setRequireString:NSLocalizedString(@"Mobile no", nil)];
    _lbRoom.attributedText = [self setRequireString:NSLocalizedString(@"Room preferences", nil)];
    _lbSmoking.attributedText = [self setRequireString:NSLocalizedString(@"I prefer smoking room", nil)];
    _lbMaxPrice.text = NSLocalizedString(@"Maximum budget", nil);
    _lbCheckIn.attributedText = [self setRequireString:NSLocalizedString(@"Check in date", nil)];
     _lbCheckOut.attributedText = [self setRequireString:NSLocalizedString(@"Check out date", nil)];
    
    
    
    
    _lbPersonalInfo.text = NSLocalizedString(@"Any special requirements", nil);
    _lbHolderAny.text = NSLocalizedString(@"Let us know how we can help you", nil);
    _lbChildren1.text = NSLocalizedString(@"Children book_dining", nil);
    _lbSpecialRequiment.text = NSLocalizedString(@"Any special requirements", nil);
    _lbKidsUnder12.text = NSLocalizedString(@"Children book_dining", nil);
    _lbPersonalInfo.text = NSLocalizedString(@"Personal Information", nil);
    _lbLoyalty.text = NSLocalizedString(@"Loyalty program", nil);
    _lbMemeberShipNo.text = NSLocalizedString(@"Membership no", nil);
    _lbPreferredVerhicle.text = NSLocalizedString(@"Preferred vehicle", nil);
    _lbPrefferedCarCompany.text = NSLocalizedString(@"Car rental company", nil);

    
    _txtMaximumPrice.placeholder = NSLocalizedString(@"Please enter your maximum budget", nil);
    
    _txtContactPhone.placeholder = NSLocalizedString(@"No. optional", nil);
    _txtLoyaltyProgram.placeholder = NSLocalizedString(@"Please enter name of loyalty program", nil);
    _txtMembershipNo.placeholder = NSLocalizedString(@"Please enter loyalty membership number", nil);
    _txtRetalCompany.placeholder = NSLocalizedString(@"Please enter preferred car rental company", nil);
    
    [_btPreferredVerhicle setTitle:NSLocalizedString(@"Please select preferred vehicle", nil) forState:UIControlStateNormal];
    [_btRoomType setTitle:NSLocalizedString(@"Please select room preference", nil) forState:UIControlStateNormal];
    //[_btRadioEmail setTitle:NSLocalizedString(@"Email", nil) forState:UIControlStateNormal];
    [_btRadioPhone setTitle:NSLocalizedString(@"Phone", nil) forState:UIControlStateNormal];
}


-(NSAttributedString*)setRequireString:(NSString *)string {
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSAttributedString *required = [[NSAttributedString alloc] initWithString:@"*" attributes:@{  NSForegroundColorAttributeName: [UIColor redColor]  }];
    [attString appendAttributedString:required];
    return attString;
    
    
}


@end
