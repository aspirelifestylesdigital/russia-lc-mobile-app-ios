//
//  CustomTextField.m
//  ALC
//
//  Created by Hai NguyenV on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CustomTextField.h"

IB_DESIGNABLE
@implementation CustomTextField

@synthesize paddingBottom, paddingRight, paddingLeft, paddingTop;

-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectMake(paddingLeft, paddingTop, bounds.size.width - paddingLeft - paddingRight, bounds.size.height - paddingTop - paddingBottom);
    //return CGRectInset(bounds, padding, padding);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    //return [self textRectForBounds:bounds];
    return CGRectMake(paddingLeft, paddingTop, bounds.size.width - paddingLeft - paddingRight, bounds.size.height - paddingTop - paddingBottom);
}

- (void)makeDefaultValues {
    paddingBottom = 0;
    paddingTop = 0;
    paddingRight = 0;
    paddingLeft = 0;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self makeDefaultValues];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self makeDefaultValues];
    }
    return self;
}
-(void)showErrorIconForMsg:(NSString *)msg{
    UIButton *btnError=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btnError addTarget:self action:@selector(tapOnError) forControlEvents:UIControlEventTouchUpInside];
    [btnError setBackgroundImage:[UIImage imageNamed:IconImageName] forState:UIControlStateNormal];
    self.rightView=btnError;
    self.rightViewMode=UITextFieldViewModeAlways;

}
-(void)tapOnError{
   // [self showErrorWithMsg:strMsg];
}
@end
