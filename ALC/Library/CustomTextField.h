//
//  CustomTextField.h
//  ALC
//
//  Created by Hai NguyenV on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorToolTip.h"

@interface CustomTextField : UITextField

@property (nonatomic) IBInspectable CGFloat paddingLeft;
@property (nonatomic) IBInspectable CGFloat paddingRight;
@property (nonatomic) IBInspectable CGFloat paddingTop;
@property (nonatomic) IBInspectable CGFloat paddingBottom;

#define IconImageName @"error.png"


@end
