//
//  NSDate+Category.m
//  ALC
//
//  Created by Tho Nguyen on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "NSDate+Category.h"

@implementation NSDate (Category)

- (NSString *)stringFromDateWithDateFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    
    return [formatter stringFromDate:self];
}

@end
