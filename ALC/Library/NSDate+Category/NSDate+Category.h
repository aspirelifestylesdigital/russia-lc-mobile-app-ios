//
//  NSDate+Category.h
//  ALC
//
//  Created by Tho Nguyen on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DATE_FORMAT_DD_MMMM_YYYY @"dd MMMM yyyy"
#define DATE_FORMAT_DD_MMMM_YYYY_H_MM_A @"dd MMMM yyyy-H:mm a"

@interface NSDate (Category)

- (NSString *)stringFromDateWithDateFormat:(NSString *)dateFormat;

@end
