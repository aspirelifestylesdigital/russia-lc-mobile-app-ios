//
//  NSBundle+LanguageManager.h
//  MultipleLanguage
//
//  Created by Chung Mai on 8/28/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (LanguageManager)

+(void)setLanguage:(NSString*)language;

@end
