//
//  PhotoResizedInfo.h
//  HungryGoWhere
//
//  Created by Linh Le on 11/26/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoResizedInfo : NSObject
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSString *extension;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSString *photoName;
@end
