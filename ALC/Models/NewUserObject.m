//
//  NewUserObject.m
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "NewUserObject.h"

@implementation NewUserObject
@synthesize email, phoneNumber, ccNumber, lastName, firstName, salutation, cardNumber, password, postalCode;
@end
