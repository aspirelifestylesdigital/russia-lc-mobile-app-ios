//
//  EventRequestObj.h
//  ALC
//
//  Created by Hai NguyenV on 11/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntertainmentRequestDetailsObject.h"
@interface EventRequestObj : NSObject

@property (strong, nonatomic) NSString* reservationDate;
@property (strong, nonatomic) NSString* reservationTime;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* EventName;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* BookingItemId;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* SpecialRequirements;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;

@property ( nonatomic) BOOL isEdit;
@property (strong, nonatomic) NSString* EditType;
@property ( nonatomic) NSInteger adultPax;

-(id) initFromDetails:(EntertainmentRequestDetailsObject*)detailsObj;

@end
