//
//  GourmetRequestObject.m
//  ALC
//
//  Created by Hai NguyenV on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetRequestObject.h"

@implementation GourmetRequestObject
-(id) initFromDetails:(GourmetRequestDetailsObj*)detailsObj{
    self = [super init];
    self.BookingId = detailsObj.bookingId;
    self.EmailAddress = detailsObj.email;
    self.MobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.SpecialRequirements = detailsObj.specialRequirement;
    
    self.City = detailsObj.city;
    self.Country = detailsObj.country;
    
    self.reservationDate = formatDate(detailsObj.reservationDate, DATE_SEND_REQUEST_FORMAT);
    self.reservationTime = formatDate(detailsObj.reservationsTime, TIME_SEND_REQUEST_FORMAT);
    self.reservationName = detailsObj.reservationName;
    self.restaurantName = detailsObj.restaurantName;
    self.adultPax = detailsObj.adulsPax;
    self.kidsPax = detailsObj.kidsPax;
    self.Occasion = [NSMutableArray arrayWithArray:detailsObj.occasion];
    [self setPrivilegeData:detailsObj.requestDetails];
    return self;
}

@end
