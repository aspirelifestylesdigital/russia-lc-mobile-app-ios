//
//  CityGuideObject.m
//  ALC
//
//  Created by Anh Tran on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CityGuideObject.h"

@implementation CityGuideObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.cityId = [dict stringForKey:@"IDStr"];
    self.cityName = [dict stringForKey:@"CityName"];
    self.title = [dict stringForKey:@"Title"];
    self.cityCountry = [dict stringForKey:@"CountryName"];
    self.cityCode = [dict stringForKey:@"CountryCode"];
    self.introduction = stringByStrippingHTML([dict stringForKey:@"Introduction"]);
    self.cityDescription = stringByStrippingHTML([dict stringForKey:@"CityDescription"]);
    self.shortDescription = stringByStrippingHTML([dict stringForKey:@"Description"]);
    self.backgroundImageUrl = [dict stringForKey:@"BackgroundImageUrl"];
    self.bannerUrl = [dict stringForKey:@"BannerURL"];
    self.isAddWishList = [dict stringForKey:@"AddWishListStatusToCSS"].length > 0;
    return self;
}
@end
