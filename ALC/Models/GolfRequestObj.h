//
//  GolfRequestObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoResizedInfo.h"
#import "GolfDetailsObj.h"

@interface GolfRequestObj : NSObject

@property (strong, nonatomic) NSString* GolfCourse;
@property (strong, nonatomic) NSString* GolfDate;
@property (strong, nonatomic) NSString* GolfTime;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* BookingItemId;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* Handicap;
@property (strong, nonatomic) NSString* SpecialRequirements;
@property (strong, nonatomic) NSString* reservationName;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;
@property (strong, nonatomic) NSString* EditType;

@property ( nonatomic) NSInteger adultPax;
@property ( nonatomic, strong) NSString* noOfBaler;
@property ( nonatomic, strong) NSString* hole;

@property (strong, nonatomic) PhotoResizedInfo* photo;

-(id) initFromDetails:(GolfDetailsObj*)detailsObj;

@end
