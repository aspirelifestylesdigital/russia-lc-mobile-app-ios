//
//  OrthersRequestObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OrtherRequestDetailsObj.h"
#import "BaseBookingRequestObject.h"
#import "PhotoResizedInfo.h"

@interface OrthersRequestObj : BaseBookingRequestObject

@property (strong, nonatomic) NSString* ortherString;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* EditType;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* spaName;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;

//These are used for Privilege spa
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;


@property (strong, nonatomic) PhotoResizedInfo* photo;

-(id) initFromDetails:(OrtherRequestDetailsObj*)detailsObj;


@end
