//
//  TempExperienceObject.m
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "TempExperienceObject.h"

@implementation TempExperienceObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.itemName = [dict stringForKey:@"Title"];
    self.itemDescription = stringByStrippingHTML([[dict stringForKey:@"Description"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"]);
   
    self.itemID = [dict stringForKey:@"IDStr"];
    self.backgroundImg = [DOMAIN_URL stringByAppendingString:[dict stringForKey:@"BannerUrl"]];
    self.siteURL = [dict stringForKey:@"Website"];
    return self;
}

@end
