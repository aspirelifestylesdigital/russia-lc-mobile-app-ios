//
//  SuccessObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/25/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuccessObj : NSObject

-(id) initFromDict:(NSDictionary*) dict;

@property (strong, nonatomic) NSString* bookingID;
@property (strong, nonatomic) NSString* bookingName;
@property (strong, nonatomic) NSString* bookingTitle;
@property (strong, nonatomic) NSDate*   bookingDate;
@end
