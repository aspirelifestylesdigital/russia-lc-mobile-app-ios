//
//  BaseBookingRequestObject.m
//  ALC
//
//  Created by Anh Tran on 3/30/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingRequestObject.h"

@implementation BaseBookingRequestObject

- (void) setPrivilegeData:(NSDictionary*) requestDetails{
    self.fullAddress = [requestDetails stringForKey:FullAddress];
    self.privilegeId = [requestDetails stringForKey:PrivilegeId];
}
@end
