//
//  EntertainmentRequestDetailsObject.m
//  ALC
//
//  Created by Anh Tran on 12/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "EntertainmentRequestDetailsObject.h"

@implementation EntertainmentRequestDetailsObject
-(id) initFromDict:(NSDictionary*) dict
{
    self = [super initFromDict:dict];
    _eventName = [self.requestDetails stringForKey:Event_Name];
    
    NSString* tempValue = [self.requestDetails stringForKey:PreferedDate];
    if(tempValue && tempValue.length>0){
        _preferDate = convertStringToDate(tempValue, DATETIME_SEND_REQUEST_FORMAT);
    }
    
    _noOfTickets = [self.requestDetails stringForKey:NoofTickets];
    if(_noOfTickets && _noOfTickets.length>0){
        self.adulsPax = [_noOfTickets integerValue];
        self.kidsPax = self.adulsPax;
    }
    
    tempValue = [self.requestDetails stringForKey:Event_Category];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _eventCategory = convertStringToArray(tempValue);
    } else {
        _eventCategory = [NSArray array];
    }
    return  self;
}
@end
