//
//  PhotoResizedInfo.m
//  HungryGoWhere
//
//  Created by Linh Le on 11/26/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "PhotoResizedInfo.h"

@implementation PhotoResizedInfo

@synthesize image;
@synthesize extension;
@synthesize caption;
@synthesize photoName;
@synthesize imageData;
@end
