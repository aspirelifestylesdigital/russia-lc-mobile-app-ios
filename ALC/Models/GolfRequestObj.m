//
//  GolfRequestObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GolfRequestObj.h"

@implementation GolfRequestObj
-(id) initFromDetails:(GolfDetailsObj*)detailsObj{
    self = [super init];
    self.BookingId = self.BookingItemId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    
    self.GolfCourse = detailsObj.GolfCourse;
    self.MobileNumber = detailsObj.mobileNumber;       
    self.EmailAddress = detailsObj.email;
    
    self.GolfDate = formatDate(detailsObj.golfCourseDateTime, DATE_SEND_REQUEST_FORMAT);
    self.GolfTime = formatDate(detailsObj.golfCourseDateTime, TIME_SEND_REQUEST_FORMAT);
    
    self.adultPax = detailsObj.adulsPax;
    self.noOfBaler = detailsObj.noOfBaler;
    self.hole = detailsObj.hole;
    self.City = detailsObj.city;
    self.Country = detailsObj.country;
    self.State = detailsObj.stateValue;
    self.SpecialRequirements = detailsObj.specialRequirement;
    
    return self;
}
@end
