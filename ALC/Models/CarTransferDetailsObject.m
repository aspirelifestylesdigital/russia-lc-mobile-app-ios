//
//  CarTransferDetailsObject.m
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarTransferDetailsObject.h"

@implementation CarTransferDetailsObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _pickUpLocation = [self.requestDetails stringForKey:PickLocation];
    _dropOffLocation =  [self.requestDetails stringForKey:DropLocation];
    
    _nbPassengers = [self.requestDetails integerForKey:NumberPassengers];
    _pickUpDate = self.pickUpDateTime;
    _pickUpTime = self.pickUpDateTime;
    _transportType = [self.requestDetails stringForKey:TransportType];
    return self;
}

-(NSString*) transportKey{
    
    return _transportType;
}

-(NSString*) transportValue{
    
    return _transportType;
}

@end
