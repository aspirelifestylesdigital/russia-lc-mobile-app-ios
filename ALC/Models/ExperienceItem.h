//
//  ExperienceItem.h
//  ALC
//
//  Created by Anh Tran on 9/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExperienceItem : NSObject
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* imageURL;
-(id) initWithType:(NSString*) type andImage:(NSString*) image;
@end
