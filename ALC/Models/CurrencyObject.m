//
//  CurrencyObject.m
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CurrencyObject.h"

@implementation CurrencyObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    _currencyKey = [dict stringForKey:@"Key"];
    _currencyText = [dict stringForKey:@"Value"];
    return self;
}

-(id) init:(NSString*) key withValue:(NSString*) value{
    self = [super init];
    self.currencyKey = key;
    self.currencyText = value;
    return self;
}

-(NSString*) fullKeyvalue{
    if(_currencyKey && _currencyText){
        return [NSString stringWithFormat:@"%@ (%@)", _currencyText, _currencyKey];
    }
    return @"";
}

@end