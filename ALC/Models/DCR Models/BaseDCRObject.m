//
//  BaseDCRObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "BaseDCRObject.h"
#import "DCRFacetObject.h"
@implementation BaseDCRObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.success = [dict boolForKey:@"success"];
    self.message = [dict stringForKey:@"message"];
    self.facets = [dict arrayForKey:@"facets" withObjectClass:[DCRFacetObject class]];
    self.errors = [dict arrayForKey:@"errors"];
    return self;
}
@end
