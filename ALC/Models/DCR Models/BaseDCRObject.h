//
//  BaseDCRObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseDCRObject : NSObject
@property (nonatomic) NSInteger task;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSArray* errors;
@property (nonatomic, strong) NSArray* facets;
@property (nonatomic, strong) NSArray* data;
@property (nonatomic) Boolean success;

-(id) initFromDict:(NSDictionary*) dict;
@end
