//
//  DCRItemDetailsResponse.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemDetailsResponse.h"
#import "DCRItemDetails.h"
@implementation DCRItemDetailsResponse
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    self.items = [dict arrayForKey:@"items" withObjectClass:[DCRItemDetails class]];
    
    return self;
}
@end
