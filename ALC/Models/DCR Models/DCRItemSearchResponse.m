//
//  DCRItemSearchResponse.m
//  ALC
//
//  Created by Anh Tran on 3/13/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemSearchResponse.h"
#import "DCRItemObject.h"
@implementation DCRItemSearchResponse

-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    self.data = [dict arrayForKey:@"data" withObjectClass:[DCRItemObject class]];
    self.totalRecord = [dict integerForKey:@"total"];
    return self;
    
}
@end
