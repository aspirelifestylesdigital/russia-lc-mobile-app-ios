//
//  DCRImageObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCRImageObject : NSObject
@property (nonatomic, strong) NSString* url;
@property (nonatomic) BOOL defaultFlag;

-(id) initFromDict:(NSDictionary*) dict;
@end
