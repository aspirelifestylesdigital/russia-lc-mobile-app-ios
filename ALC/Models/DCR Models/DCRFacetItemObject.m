//
//  DCRFacetItemObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRFacetItemObject.h"

@implementation DCRFacetItemObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.count = [dict integerForKey:@"count"];
    self.name = [dict stringForKey:@"name"];
    return self;
}
@end
