//
//  DCRItemSearchResponse.h
//  ALC
//
//  Created by Anh Tran on 3/13/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "BaseDCRObject.h"

@interface DCRItemSearchResponse : BaseDCRObject
@property (nonatomic) NSInteger totalRecord;
-(id) initFromDict:(NSDictionary*) dict;

@end
