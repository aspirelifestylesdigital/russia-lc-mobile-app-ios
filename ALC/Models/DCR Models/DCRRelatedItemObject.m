//
//  DCRRelatedItemObject.m
//  ALC
//
//  Created by Anh Tran on 3/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRRelatedItemObject.h"

@implementation DCRRelatedItemObject
-(id) initFromDict:(NSDictionary*) dict{
    if(dict){
        self = [super init];
        self.itemId = [dict stringForKey:@"id"];
        self.Name = [dict stringForKey:@"name"];
        self.itemDescription = [dict stringForKey:@"description"];
        self.type = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"type"]];
        
    }
    return self;
}

- (BOOL) isValid{
    return self.itemId != nil;
}
@end
