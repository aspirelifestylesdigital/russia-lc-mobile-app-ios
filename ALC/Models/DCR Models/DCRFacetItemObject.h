//
//  DCRFacetItemObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCRFacetItemObject : NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic) NSInteger count;
-(id) initFromDict:(NSDictionary*) dict;
@end
