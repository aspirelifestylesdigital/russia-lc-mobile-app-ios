//
//  DCRFacetObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRFacetObject.h"
#import "DCRFacetItemObject.h"
@implementation DCRFacetObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.name = [dict stringForKey:@"name"];
    self.detail = [dict arrayForKey:@"detail" withObjectClass:[DCRFacetItemObject class]];
    return self;
}


@end
