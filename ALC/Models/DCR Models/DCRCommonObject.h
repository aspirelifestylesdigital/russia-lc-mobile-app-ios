//
//  DCRCommonObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCRCommonObject : NSObject
@property (nonatomic, strong) NSString* Id;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) NSString* Code;

-(id) initFromDict:(NSDictionary*) dict;
- (BOOL) isValid;
@end
