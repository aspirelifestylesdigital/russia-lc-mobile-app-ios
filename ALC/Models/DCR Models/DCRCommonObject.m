//
//  DCRCommonObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRCommonObject.h"

@implementation DCRCommonObject
-(id) initFromDict:(NSDictionary*) dict{
    if(dict){
        self = [super init];
        self.Id = [dict stringForKey:@"id"];
        self.Name = [dict stringForKey:@"name"];
        self.Code = [dict stringForKey:@"code"];
    }
    return self;
}

- (BOOL) isValid{
    return self.Name != nil;
}
@end
