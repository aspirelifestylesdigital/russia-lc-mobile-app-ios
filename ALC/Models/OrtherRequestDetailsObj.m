//
//  OrtherRequestDetailsObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OrtherRequestDetailsObj.h"

@implementation OrtherRequestDetailsObj

-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _ortherString = [self.requestDetails stringForKey:SPECIAL_REQUIREMENT];
    _spaName = [self.requestDetails stringForKey:SpaName];
    return self;
}

-(NSString*) privilegeName{
    return _spaName;
}
@end
