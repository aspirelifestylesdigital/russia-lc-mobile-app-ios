//
//  GourmetMenuObject.h
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GourmetMenuObject : NSObject
@property (strong, nonatomic) NSString* menuId;
@property (strong, nonatomic) NSString* groupName;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* menuDescription;
@property (strong, nonatomic) NSString* price;
@property (strong, nonatomic) NSString* imageURL;

-(id) initFromDict:(NSDictionary*) dict;
@end
