//
//  HGWBaseRequest.h
//  HungryGoWhere
//
//  Created by Linh Le on 11/9/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGWBaseRequest : NSObject

@property(assign) NSInteger contentType;
@property(strong) NSString *requestUrl;
@property(strong) NSMutableDictionary *dictKeyValues;

// Method to return request string
// Sub class should override it
-(NSString*) requestString;
-(NSString*) getSignature;

@end
