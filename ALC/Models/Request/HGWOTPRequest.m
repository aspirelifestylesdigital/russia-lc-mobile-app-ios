//
//  HGWOTPRequest.m
//  HungryGoWhere
//
//  Created by Linh Le on 11/16/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "HGWOTPRequest.h"

@implementation HGWOTPRequest

-(NSString*) requestString
{
    if (self.dictKeyValues == nil) {
        return nil;
    }
    
    // create request body
    NSMutableString *requestString = [[NSMutableString alloc]init];
    
    NSArray *keys = [self.dictKeyValues allKeys];
    NSString *value;
    int index = 0;
    for (NSString *key in keys) {
        
        value = [self.dictKeyValues objectForKey:key];
        NSLog(@"%@",key);
        
        if (index == 0) {
            [requestString appendFormat:@"\"%@\":\"%@\"", key, value];
        }
        else
        {
            [requestString appendFormat:@",\"%@\":\"%@\"", key, value];
        }
        
        index ++;
    }
    NSLog(@"%@", requestString);
    requestString = [NSMutableString stringWithFormat:@"{%@}", requestString];
    NSLog(@"%@",requestString);
    
    return requestString;
}

@end
