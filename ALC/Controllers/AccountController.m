//
//  AccountController.m
//  ALC
//
//  Created by Hai NguyenV on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "AccountController.h"
#import "RegisterViewController.h"
#import "MyCurrencyController.h"
#import "ChangePasswordController.h"

@interface AccountController ()

@property (weak, nonatomic) IBOutlet UILabel *myProfileLbl;
@property (weak, nonatomic) IBOutlet UILabel *myPreferencesLbl;
@property (weak, nonatomic) IBOutlet UILabel *myCurrencyLbl;
@property (weak, nonatomic) IBOutlet UILabel *changePasswordLbl;

@end

@implementation AccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customButtonBack];
}

-(void) setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"ACCOUNT", nil) withColor:HEADER_COLOR];
    self.myProfileLbl.text = NSLocalizedString(@"My Profile", @"");
    self.myPreferencesLbl.text = NSLocalizedString(@"My Preferences", @"");
    self.myCurrencyLbl.text = NSLocalizedString(@"My Currency", @"");
    self.changePasswordLbl.text = NSLocalizedString(@"Change Password", @"");
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionProfile:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    RegisterViewController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    controller.isEdit = YES;
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)actionCurrency:(id)sender {
    MyCurrencyController* controller = (MyCurrencyController*) [self controllerByIdentifier:@"MyCurrencyController"];
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)actionChangePassword:(id)sender {
    ChangePasswordController* controller = (ChangePasswordController*) [self controllerByIdentifier:@"ChangePasswordController"];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
