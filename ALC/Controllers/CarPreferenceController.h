//
//  CarPreferenceController.h
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubFilterController.h"
#import "CustomTextField.h"
#import "WSCreateCarPre.h"
#import "WSGetMyPreferences.h"

@interface CarPreferenceController : MainViewController<SubFilterControllerDelegate,UITextFieldDelegate,DataLoadDelegate>
{
    NSMutableArray *selectedVerhicle;
    NSMutableArray *selectedCompany;
    NSMutableArray *selectedVerhicleKey;
    NSMutableArray *selectedCompanyKey;
    BOOL isVehicle;
    WSCreateCarPre* wsCreateCarPre;
    WSGetMyPreferences* wsGetMyPreference;
    CGSize originalSize;
}
@property (weak, nonatomic) IBOutlet CustomTextField *tfMembership;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLoyalty;
//@property (weak, nonatomic) IBOutlet CustomTextField *tfOtherCompany;
@property (weak, nonatomic) IBOutlet CustomTextField *tfRentalCompany;
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnVehicle;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (nonatomic, strong)NSString* preferenceID;
@property (weak, nonatomic) IBOutlet UILabel *transportationTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *companiesLbl;
@property (weak, nonatomic) IBOutlet UILabel *hintHelpLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLoyaltyProgramLbl;
@property (weak, nonatomic) IBOutlet UILabel *membershipNoLbl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

- (IBAction)selectVehicle:(id)sender;
- (IBAction)actionSave:(id)sender;
- (IBAction)actionCancel:(id)sender;
@end
