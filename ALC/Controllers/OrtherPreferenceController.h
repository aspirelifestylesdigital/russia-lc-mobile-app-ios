//
//  OrtherPreferenceController.h
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "WSCreateOrtherPre.h"
#import "WSGetMyPreferences.h"

@interface OrtherPreferenceController : MainViewController<DataLoadDelegate, UITextViewDelegate>
{
    WSCreateOrtherPre* wsCreateOthers;
    WSGetMyPreferences* wsGetMyPreference;
}
@property (weak, nonatomic) IBOutlet UITextView *tfOrtherPreference;
@property (weak, nonatomic) IBOutlet UILabel *otherPreferTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *otherHelpLbl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;


- (IBAction)actionSave:(id)sender;
- (IBAction)actionCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbHolder;
@property (nonatomic, strong)NSString* preferenceID;
@end
