//
//  LocationSelectionViewController.m
//  ALC
//
//  Created by Tho Nguyen on 10/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "LocationSelectionViewController.h"
#import "CCNumberCell.h"

static NSString *kNumberCell = @"CCCell";
static NSString *const kCCNumberCell = @"CCNumberCell";

@interface LocationSelectionViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnSelection;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LocationSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUp {
    [self.btnSelection setTitle:NSLocalizedString(@"Select", nil) forState:UIControlStateNormal];
    [self.tableView registerNib:[UINib nibWithNibName:kCCNumberCell bundle:nil] forCellReuseIdentifier:kNumberCell];
}

- (IBAction)tappedAtSelectionButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectItem:andLocationSelectionType:)]) {
        
        [self.delegate didSelectItem:self.indexSelection andLocationSelectionType:self.type];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CCNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:kNumberCell];
    NSString *title = [self.datas objectAtIndex:indexPath.row];
    cell.labelName.text = title;
    if (self.indexSelection > 0) {
        if (indexPath.row == self.indexSelection) {
            cell.btCheck.hidden = NO;
        } else {
            cell.btCheck.hidden = YES;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.indexSelection = indexPath.row;
    [self.tableView reloadData];
}

@end
