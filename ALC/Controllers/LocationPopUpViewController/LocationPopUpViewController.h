//
//  LocationPopUpViewController.h
//  ALC
//
//  Created by Tho Nguyen on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationPopUpViewController : UIViewController

- (void)willAddLocationPopupViewControllerInViewController:(UIViewController *)vc;

@end
