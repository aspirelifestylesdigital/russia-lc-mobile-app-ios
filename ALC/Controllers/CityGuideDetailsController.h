//
//  CityGuideDetailsController.h
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "CityGuideCategoryView.h"
#import "WSGetCityGuideDetails.h"
@interface CityGuideDetailsController : MainViewController<DataLoadDelegate, CategoryDelegate>
@property (nonatomic, strong) NSString* cityGuideId;
@end
