//
//  GourmetRequestController.h
//  ALC
//
//  Created by Hai NguyenV on 10/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "OptionalDetailsView.h"
#import "SubFilterController.h"
#import "WSGourmetBooking.h"
#import "WSGetGourmetRequestDetails.h"
#import "GourmetRecommendView.h"
#import "WSGourmetRecommendRequest.h"
#import "WSGetGourmetRecommend.h"
#import "ErrorToolTip.h"
#import "WSGetMyPreferences.h"
#import "DCRItemDetails.h"
#import "RequestFormBaseController.h"

@interface GourmetRequestController : RequestFormBaseController<SubFilterControllerDelegate,DataLoadDelegate,GourmetRecommendViewDelegate,ErrorToolTipDelegate>
{
}

@property (nonatomic) enum REQUEST_TYPE requestType;

@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* restaurantName;
@property (strong, nonatomic) NSString* restaurantID;
@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* foodAllergies;


@end
