//
//  FAQController.m
//  ALC
//
//  Created by Anh Tran on 9/22/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "FAQController.h"

@interface FAQController ()
@property (strong, nonatomic) IBOutlet UIButton *btTopHeader;
@property (strong, nonatomic) IBOutlet UIStackView *stFAQContents;

@end

@implementation FAQController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
