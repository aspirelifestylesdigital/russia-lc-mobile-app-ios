//
//  MapController.m
//  ALC
//
//  Created by Anh Tran on 9/8/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MapController.h"
#import <MapKit/MapKit.h>

@interface MapController ()
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addTitleToHeader:NSLocalizedString(@"MAP", nil) withColor:HEADER_COLOR];
    [self customButtonBack];

    _mapView.showsUserLocation = YES;
    _mapView.mapType = MKMapTypeStandard;
    [self showLocation:_addressObject];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) showLocation:(DCRItemDetails*) location{
    if(location!=nil){
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake([location.latitude doubleValue], [location.longitude doubleValue]);
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:centerCoordinate];
        [annotation setTitle:location.itemName];
        [annotation setSubtitle:[location fullLocationAddress]];
        [self.mapView addAnnotation:annotation];
        
        [self updateMapZoom];
        
        [_mapView showAnnotations:[NSArray arrayWithObjects:annotation,nil] animated:YES];
        [self updateMapRegion:centerCoordinate withDelta:0.01];
        
        [self.mapView setSelectedAnnotations:[NSArray arrayWithObjects:annotation,nil]];
    }
}


-(void) updateMapZoom{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    CLLocation *location =  [appDelegate requestUserCurrentLocation];
    double miles = 1.0;
    double scalingFactor = ABS( (cos(2 * M_PI * location.coordinate.latitude / 360.0) ));
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = location.coordinate;
    
    [_mapView setRegion:region animated:YES];
}
- (void) updateMapRegion:(CLLocationCoordinate2D)region withDelta:(float) delta{
    [_mapView setCenterCoordinate:region animated:YES];
    
    MKCoordinateRegion viewRegion;
    viewRegion.center=region;
    MKCoordinateSpan span;
    span.latitudeDelta=delta;
    span.longitudeDelta=delta;
    viewRegion.span=span;
    [_mapView setRegion:viewRegion animated:YES];
    
}


@end
