//
//  CCNumberController.h
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
@protocol CCNumberControllerDelegate;

@interface CCNumberController : MainViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, UISearchBarDelegate>
@property (unsafe_unretained) id<CCNumberControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSString *selectedValue;
@property (nonatomic) NSInteger dataType;
@end

@protocol CCNumberControllerDelegate <NSObject>

- (void) pickCCNumber:(NSString*) number forType:(NSInteger) type;

@end