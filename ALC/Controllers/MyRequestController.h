//
//  MyRequestController.h
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSGetRequestList.h"
#import "MyRequestCell.h"
@interface MyRequestController : MainViewController<DataLoadDelegate, UITableViewDelegate, UITableViewDataSource,MyRequestCellDelegate>

@end
