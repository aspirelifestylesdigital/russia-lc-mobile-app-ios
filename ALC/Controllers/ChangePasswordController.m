//
//  ChangePasswordController.m
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//
#import "StoreUserData.h"
#import "ChangePasswordController.h"
#import "CustomTextField.h"
#import "ErrorToolTip.h"
#import "BaseResponseObject.h"
@interface ChangePasswordController (){
    WSChangePassword* wsChangePassword;
    CGSize originalSize;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet CustomTextField *txtOldPassword;
@property (strong, nonatomic) IBOutlet ErrorToolTip *errorOldPassword;

@property (strong, nonatomic) IBOutlet CustomTextField *txtNewPassword;
@property (strong, nonatomic) IBOutlet ErrorToolTip *errorNewPassword;
@property (strong, nonatomic) IBOutlet CustomTextField *txtRetypePassword;
@property (strong, nonatomic) IBOutlet ErrorToolTip *errorRetypePassword;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet UILabel *lbPasswordHint;
@property (weak, nonatomic) IBOutlet UILabel *oldPasswordLbl;
@property (weak, nonatomic) IBOutlet UILabel *mandatoryLbl;
@property (weak, nonatomic) IBOutlet UILabel *passwordLbl;
@property (weak, nonatomic) IBOutlet UILabel *retypeNewPasswordLbl;


@end

@implementation ChangePasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if(!self.isForceChange){
        [self customButtonBack];
    } else {
        UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 9, 15)];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [tapGesture addTarget:self action:@selector(tapInView:)];
    [self.view addGestureRecognizer:tapGesture];
    
    [_scrollView setNeedsLayout];
    [_scrollView layoutIfNeeded];
    originalSize = _scrollView.contentSize;
    
    _txtOldPassword.delegate = self;
    _txtNewPassword.delegate = self;
    _txtRetypePassword.delegate = self;
}

-(void)setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"CHANGE PASSWORD", nil) withColor:HEADER_COLOR];
    self.oldPasswordLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Old password*", @"")];
    self.mandatoryLbl.attributedText = [self setStyleForString:NSLocalizedString(@"*Mandatory_ChangePW", @"")];
    self.txtOldPassword.placeholder = NSLocalizedString(@"Old password", @"");
    self.passwordLbl.attributedText = [self setStyleForString:NSLocalizedString(@"New password*", @"")];
    self.txtNewPassword.placeholder = NSLocalizedString(@"New password", @"");
    self.lbPasswordHint.text = NSLocalizedString(@"Enter maximum of 5 numbers or letters", @"");
    self.retypeNewPasswordLbl.attributedText = [self setStyleForString:NSLocalizedString(@"Retype new password*", @"")];
    self.txtRetypePassword.placeholder = NSLocalizedString(@"Retype new password", @"");
    [self.btSubmit setTitle:NSLocalizedString(@"SUBMIT", @"") forState:UIControlStateNormal];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
   
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.isForceChange){
        [self lockScreen];
    }
}

-(BOOL)isShowConciergeButton{
    return !self.isForceChange;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

#pragma mark Show Left-Right Menu
- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return !self.isForceChange;
}


-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGSize temp = originalSize;
    temp.height = originalSize.height+100;
    [_scrollView setContentSize:temp];
}

-(void)keyboardWillHide {
    [_scrollView setContentSize:originalSize];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)tapInView:(UITapGestureRecognizer *)tapGuesture
{
    [_txtRetypePassword resignFirstResponder];
    [_txtNewPassword resignFirstResponder];
    [_txtOldPassword resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag == 3){
        //Retype password tag
        [textField resignFirstResponder];
        [self validateAndSubmit];
    } else {
        UITextField *txt = [self.view viewWithTag:textField.tag + 1];
        if(txt!=nil){
            [txt becomeFirstResponder];
        }
        
    }
    return YES;
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)sender {
    if(sender.tag != 0){
        float offset = (sender.frame.origin.y) - (SCREEN_HEIGHT - 64 - 250) + sender.frame.size.height;
        if(offset > 0){
            [self.scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
        }
        
        UIView *errorIcon = [self.view viewWithTag:100+sender.tag];
        if(errorIcon!=nil){
            errorIcon.hidden = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) changePassword:(NSString*) old toNew:(NSString*) newPass{
    wsChangePassword = [[WSChangePassword alloc] init];
    wsChangePassword.delegate = self;
    [wsChangePassword changePassword:old toPassword:newPass];
}

- (IBAction)actionSubmit:(id)sender {
    [self validateAndSubmit];
}

-(void)validateAndSubmit{
    [_txtNewPassword resignFirstResponder];
    [_txtOldPassword resignFirstResponder];
    [_txtRetypePassword resignFirstResponder];
    
    [_errorNewPassword setErrorHidden:YES];
    [_errorOldPassword setErrorHidden:YES];
    [_errorRetypePassword setErrorHidden:YES];
    
    if(!checkPasswordValid(_txtOldPassword.text)){
        _errorOldPassword.errorMessage = NSLocalizedString(@"Password should be in correct format", nil);
        [_errorOldPassword setErrorHidden:false];
    }
    
    if(!checkPasswordValid(_txtNewPassword.text)){
        _errorNewPassword.errorMessage = NSLocalizedString(@"Password should be in correct format", nil);
        [_errorNewPassword setErrorHidden:false];
    }
    
    if(!checkPasswordValid(_txtRetypePassword.text)){
        _errorRetypePassword.errorMessage = NSLocalizedString(@"Password should be in correct format", nil);
        [_errorRetypePassword setErrorHidden:false];
    }
    
    if(_errorNewPassword.hidden && _errorRetypePassword.hidden &&
       ![_txtRetypePassword.text isEqualToString:_txtNewPassword.text]){
        _errorRetypePassword.errorMessage = NSLocalizedString(@"Passwords do not match. Please try again", nil);
        _errorNewPassword.errorMessage = NSLocalizedString(@"Passwords do not match. Please try again", nil);
        [_errorRetypePassword setErrorHidden:false];
        [_errorNewPassword setErrorHidden:false];
    }
    
    if(_errorRetypePassword.hidden && _errorNewPassword.hidden && _errorOldPassword.hidden){
        [self showToast];
        [self changePassword:_txtOldPassword.text toNew:_txtNewPassword.text];
    }

}


-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_CHANGE_PASSWORD){
        [self unLockScreen];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Change password", nil)
                                                                       message:NSLocalizedString(@"You have successfully changed your password.", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  [alert popoverPresentationController];
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                              }];
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result!=nil && result.message!=nil){
        if([result.b2cErrorCode isEqualToString:@"ENR028-2"]){
            showAlertOneButton(self, NSLocalizedString(@"Change password",nil), NSLocalizedString(@"Password should be in correct format", nil), @"OK");
        } else {
            showAlertOneButton(self, NSLocalizedString(@"Change password",nil), result.message, @"OK");
        }
    }
}

-(void) lockScreen{
    [self.navigationController.navigationBar setUserInteractionEnabled:NO];
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.rootVC.enableSwipeGesture = NO;
}

-(void) unLockScreen{
    if(_isForceChange){
        _isForceChange = NO;
        removeUserForgot();
        [self.navigationController.navigationBar setUserInteractionEnabled:YES];
        appdelegate.rootVC.enableSwipeGesture = YES;
    }
}


@end
