//
//  SubFilterController.m
//  ALC
//
//  Created by Anh Tran on 8/24/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SubFilterController.h"
#import "CCNumberCell.h"
#import "SlideNavigationController.h"
#import "BaseResponseObject.h"
#import "CountryObject.h"
#import "CityObject.h"

@interface SubFilterController (){
    NSArray* filteredData;
    NSArray* filteredDataFull;
    NSArray *data;
    NSArray* dataFull;
    IBOutlet UITableView *table;
   
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintSearchBar;

@end

@implementation SubFilterController
@synthesize  dataType, selectedValues,selectedKey,countryType;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self customButtonBack];
    
    if(data == nil){
        data = [NSArray array];
    }
    filteredData = data;
    filteredDataFull = data;
    
    if(selectedValues == nil){
        selectedValues = [NSMutableArray array];
    }
    if(selectedKey == nil){
        selectedKey = [NSMutableArray array];
    }
    
    if(dataType != COUNTRY && dataType != STATE){
        _heightConstraintSearchBar.constant = 0;
    } else {
        _searchBar.delegate = self;
        _heightConstraintSearchBar.constant = _heightConstraintSearchBar.constant * SCREEN_SCALE;
        
        UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
        // To change background color
        searchField.backgroundColor = [UIColor colorWithRed:(242.0f/255.0f) green:(241.0f/255.0f) blue:(237.0f/255.0f) alpha:1.0f];
        // To change text color
        if(dataType == STATE){
            searchField.placeholder = NSLocalizedString(@"Enter state name",nil);
        } else if(dataType == COUNTRY){
            searchField.placeholder = NSLocalizedString(@"Enter country code",nil);        
        }
        searchField.textColor = [UIColor blackColor];
    }
    
    [self setHeaderTitle];
    
    if(_maxCount!=1){
        UIButton* rightListButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 45)];
        [rightListButton setTitle:NSLocalizedString(@"Select", nil) forState:UIControlStateNormal];
        [rightListButton setContentMode:UIViewContentModeCenter];
        [rightListButton addTarget:self action:@selector(selectValues:) forControlEvents:UIControlEventTouchUpInside];
    
        [((SlideNavigationController*)self.navigationController) setRightBarButtonItem:[[UIBarButtonItem  alloc] initWithCustomView:rightListButton]];
    }else{
        UIButton* rightListButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 45)];
        [rightListButton setTitle:@"" forState:UIControlStateNormal];
        [rightListButton setContentMode:UIViewContentModeCenter];
       // [rightListButton addTarget:self action:@selector(selectValues:) forControlEvents:UIControlEventTouchUpInside];
        
        [((SlideNavigationController*)self.navigationController) setRightBarButtonItem:[[UIBarButtonItem  alloc] initWithCustomView:rightListButton]];
    }
    
    [self getData];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        id _obj = [note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect _keyboardFrame = CGRectNull;
        if ([_obj respondsToSelector:@selector(getValue:)]) [_obj getValue:&_keyboardFrame];
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [table setContentInset:UIEdgeInsetsMake(0.f, 0.f, _keyboardFrame.size.height, 0.f)];
        } completion:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [UIView animateWithDuration:0.25f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [table setContentInset:UIEdgeInsetsZero];
        } completion:nil];
    }];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [((SlideNavigationController*)self.navigationController) setRightBarButtonItem:nil];
}

-(void) setHeaderTitle{
    NSString *title = NSLocalizedString(@"PREFERENCE", nil);
    if(dataType == CUISINES){
        title = NSLocalizedString(@"CUISINE PREFERENCES",nil);
    } else if(dataType == OCCASION) {
        title = NSLocalizedString(@"OCCASIONS",nil);
    } else if(dataType == RETAL_VERTICLE){
        title = NSLocalizedString(@"PREFERRED VERHICLE", nil);
    } else  if (dataType == COMPANY){
        title = NSLocalizedString(@"PREFERENCE", nil);
    } else if (dataType == TEE){
        title = NSLocalizedString(@"TEE TIME", nil);
    } else if (dataType == ROOM){
        title = NSLocalizedString(@"ROOM PREFERENCE", nil);
    } else if (dataType == BED){
        title = NSLocalizedString(@"BED PREFERENCE", nil);
    } else if(dataType == CITY){
        title = NSLocalizedString(@"CITY", nil);
    } else if(dataType == COUNTRY){
        title = NSLocalizedString(@"COUNTRY", nil);
    } else if(dataType == TRANSPORT){
        title = NSLocalizedString(@"TRANSPORT TYPE", nil);
    }  else  if(dataType == EVENT_CATEGORY){
        title = NSLocalizedString(@"EVENT CATEGORY", nil);
    }  else if(dataType == STATE){
        title = NSLocalizedString(@"STATE", nil);
    } else {
        title = NSLocalizedString(@"FILTER", nil);
    }
    
    [self addTitleToHeader:title withColor:HEADER_COLOR];
}

-(void)selectValues:(id)sender
{
    [self returnSelectedValue];
}

-(void) returnSelectedValue{
    if (dataType == TEE || dataType == BED || dataType == CUISINES || dataType == ROOM || dataType == COMPANY || dataType == RETAL_VERTICLE || dataType == COUNTRY|| dataType == CITY|| dataType == TRANSPORT|| dataType == OCCASION || dataType == EVENT_CATEGORY || dataType == STATE ){
        if(self.delegate != nil){
            [self.delegate pickOptionKey:selectedKey withValues:selectedValues forType:dataType];
        }
    }else{
        if(self.delegate != nil){
            [self.delegate pickOption:selectedValues forType:dataType];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) buttonBack{
    [super buttonBack];
}

- (void) getData{
    [self showToast];
    NSArray* array;
    if(dataType == CUISINES){
        /*
        wsCuisine = [[WSGetCuisines alloc] init];
        wsCuisine.delegate = self;
        [wsCuisine getList];
        */
//        array= [[NSArray alloc] initWithObjects:@"Африканская",
//                @"Амальфитанская",
//                @"Американская",
//                @"Арабская",
//                @"Аргентинская",
//                @"Азиатская",
//                @"Австралийская",
//                @"Австрийская",
//                @"Барбекю",
//                @"Бразильская",
//                @"Британская",
//                @"Бирманская",
//                @"Кахунская/ Креольская",
//                @"Калифорнийская",
//                @"Канадская",
//                @"Кантонская",
//                @"Карибская",
//                @"Курица",
//                @"Китайская",
//                @"Классическая итальянская/ Традиционная",
//                @"Кубинская",
//                @"Средиземноморская",
//                @"Голландская",
//                @"Европейская",
//                @"Французская",
//                @"Немецкая",
//                @"Греческая",
//                @"Гавайская",
//                @"Индийская",
//                @"Индонезийская",
//                @"Международная",
//                @"Итальянская",
//                @"Ямайская",
//                @"Японская",
//                @"Корейская",
//                @"Латиноамериканская",
//                @"Ливанская",
//                @"Малазийская",
//                @"Китайская",
//                @"Средиземноморская",
//                @"Мексиканская",
//                @"Ближневосточная",
//                @"Миланская",
//                @"Современная/ американская",
//                @"Современная/ французская",
//                @"Современная/ немецкая",
//                @"Современная/ итальянская",
//                @"Современная/ мексиканская",
//                @"Мароккансая",
//                @"Тихоокеанская",
//                @"Иранская",
//                @"Перуанская",
//                @"Пицца",
//                @"Польская",
//                @"Румынская",
//                @"Португальская",
//                @"Русская",
//                @"Скандинавская",
//                @"Морепродукты",
//                @"Сингапурская",
//                @"Южноамериканская",
//                @"Испанская",
//                @"Ланкийская",
//                @"Стейк/ Стейк-хаус",
//                @"Суши",
//                @"Шведская",
//                @"Швейцарская",
//                @"Сычуаньская",
//                @"Тайваньская",
//                @"Тайская",
//                @"Турецкая",
//                @"Тосканская",
//                @"Умбрийская ",
//                @"Венецианская ",
//                @"Вьетнамская", nil];
        array= [[NSArray alloc] initWithObjects:NSLocalizedString(@"African", nil),
                NSLocalizedString(@"Amalfitano", nil),
                NSLocalizedString(@"American", nil),
                NSLocalizedString(@"Arabic", nil),
                NSLocalizedString(@"Argentinian", nil),
                NSLocalizedString(@"Asian", nil),
                NSLocalizedString(@"Australian", nil),
                NSLocalizedString(@"Austrian", nil),
                NSLocalizedString(@"Barbeque", nil),
                NSLocalizedString(@"Brazilian", nil),
                NSLocalizedString(@"British", nil),
                NSLocalizedString(@"Burmese", nil),
                NSLocalizedString(@"Cajun/Creole", nil),
                NSLocalizedString(@"Californian", nil),
                NSLocalizedString(@"Canadian", nil),
                NSLocalizedString(@"Cantonese", nil),
                NSLocalizedString(@"Caribbean", nil),
                NSLocalizedString(@"Chicken", nil),
                NSLocalizedString(@"Chinese", nil),
                NSLocalizedString(@"Classic Italian/Traditional", nil),
                NSLocalizedString(@"Cuban", nil),
                NSLocalizedString(@"Cucina Povera", nil),
                NSLocalizedString(@"Dutch", nil),
                NSLocalizedString(@"European", nil),
                NSLocalizedString(@"French", nil),
                NSLocalizedString(@"German", nil),
                NSLocalizedString(@"Greek", nil),
                NSLocalizedString(@"Hawaii Regional", nil),
                NSLocalizedString(@"Indian", nil),
                NSLocalizedString(@"Indonesian", nil),
                NSLocalizedString(@"International/Global", nil),
                NSLocalizedString(@"Italian", nil),
                NSLocalizedString(@"Jamaican", nil),
                NSLocalizedString(@"Japanese", nil),
                NSLocalizedString(@"Korean", nil),
                NSLocalizedString(@"Latin", nil),
                NSLocalizedString(@"Lebanese", nil),
                NSLocalizedString(@"Malaysian", nil),
                NSLocalizedString(@"Mandarin", nil),
                NSLocalizedString(@"Mediterranean", nil),
                NSLocalizedString(@"Mexican", nil),
                NSLocalizedString(@"Middle Eastern", nil),
                NSLocalizedString(@"Milanese", nil),
                NSLocalizedString(@"Modern/Contempo American", nil),
                NSLocalizedString(@"Modern/Contempo French", nil),
                NSLocalizedString(@"Modern/Contempo German", nil),
                NSLocalizedString(@"Modern/Contempo Italian", nil),
                NSLocalizedString(@"Modern/Contempo Mexican", nil),
                NSLocalizedString(@"Moroccan", nil),
                NSLocalizedString(@"Pacific Rim", nil),
                NSLocalizedString(@"Persian", nil),
                NSLocalizedString(@"Peruvian", nil),
                NSLocalizedString(@"Pizza", nil),
                NSLocalizedString(@"Polish", nil),
                NSLocalizedString(@"Roman", nil),
                NSLocalizedString(@"Rortuguese", nil),
                NSLocalizedString(@"Russian", nil),
                NSLocalizedString(@"Scandinavian", nil),
                NSLocalizedString(@"Seafood", nil),
                NSLocalizedString(@"Singaporean", nil),
                NSLocalizedString(@"South American", nil),
                NSLocalizedString(@"Southern American", nil),
                NSLocalizedString(@"Spanish", nil),
                NSLocalizedString(@"Sri Lankan", nil),
                NSLocalizedString(@"Steak/Steakhouse", nil),
                NSLocalizedString(@"Sushi", nil),
                NSLocalizedString(@"Swedish", nil),
                NSLocalizedString(@"Swiss", nil),
                NSLocalizedString(@"Szechuan", nil),
                NSLocalizedString(@"Taiwanese", nil),
                NSLocalizedString(@"Thai", nil),
                NSLocalizedString(@"Turkish", nil),
                NSLocalizedString(@"Tuscan", nil),
                NSLocalizedString(@"Umbrian", nil),
                NSLocalizedString(@"Venetian", nil),
                NSLocalizedString(@"Vietnamese", nil), nil];
        
    } else if (dataType == OCCASION){
        /*
        wsOccasion = [[WSGetOccasion alloc] init];
        wsOccasion.delegate = self;
        [wsOccasion getList];
        */
//        array= [[NSArray alloc] initWithObjects:@"Встреча выпускников",
//                @"Годовщина",
//                @"Девичник",
//                @"Деловой ужин",
//                @"День рождения",
//                @"День Святого Валентина",
//                @"Дискотека",
//                @"Другой",
//                @"Корпоратив",
//                @"Мужская вечеринка",
//                @"Помолвка",
//                @"Свадебная вечеринка",
//                @"Свидание", nil];
        array= [[NSArray alloc] initWithObjects:NSLocalizedString(@"Anniversary", nil),
                NSLocalizedString(@"Birthdays", nil),
                NSLocalizedString(@"Bridal/Baby Shower", nil),
                NSLocalizedString(@"Business Dining", nil),
                NSLocalizedString(@"Night Out", nil),
                NSLocalizedString(@"Private Function", nil),
                NSLocalizedString(@"Romance/First Dates", nil),
                NSLocalizedString(@"Others_dining", nil), nil];
        
    }else if (dataType == RETAL_VERTICLE){
        /*
        wsGetRetal = [[WSGetRetalVehicle alloc] init];
        wsGetRetal.delegate = self;
        [wsGetRetal getList];
        */
        array= [[NSArray alloc] initWithObjects:NSLocalizedString(@"Luxury Sedan", nil),
                NSLocalizedString(@"MPV", nil),
               NSLocalizedString(@"Standard Sedan", nil), nil];
        
    }else if (dataType == COMPANY){
        /*
        wsGetCompanyPre = [[WSGetCompanyPreferences alloc] init];
        wsGetCompanyPre.delegate = self;
        [wsGetCompanyPre getList];
        */
         array= [[NSArray alloc] initWithObjects:@"Rental Company 1",@"Rental Company 2",@"Rental Company 3", nil];
        
    }else if (dataType == TEE){
        /*
        wsGetTee = [[WSGetTeeTimes alloc] init];
        wsGetTee.delegate = self;
        [wsGetTee getList];
        */
        array= [[NSArray alloc] initWithObjects:@"6:00 AM",@"7:00 AM",@"8:00 AM",@"9:00 AM",@"10:00 AM",@"11:00 AM",@"12:00 PM",@"1:00 PM",@"2:00 PM",@"3:00 PM",@"4:00 PM",@"5:00 PM", nil];
        
    }else if (dataType == ROOM){
        /*
        wsGetRoom = [[WSGetRoom alloc] init];
        wsGetRoom.delegate = self;
        [wsGetRoom getList];
        */
//        array= [[NSArray alloc] initWithObjects:@"Вилла",
//                @"Коттедж",
//                @"Люкс",
//                @"Полулюкс",
//                @"С видом на бассейн",
//                @"С видом на город",
//                @"С видом на горы",
//                @"С видом на океан",
//                @"С видом на сад",
//                @"С внутренним двориком",
//                @"Семейный номер",
//                @"Стандарт", nil];
        array= [[NSArray alloc] initWithObjects:NSLocalizedString(@"Standard", nil),
                NSLocalizedString(@"Deluxe/Superior", nil),
                NSLocalizedString(@"Suite", nil),
                NSLocalizedString(@"Family Suite", nil),
                NSLocalizedString(@"Pool view", nil),
                NSLocalizedString(@"Water/Ocean view", nil),
                NSLocalizedString(@"Garden view", nil),
                NSLocalizedString(@"Mountain View", nil),
                NSLocalizedString(@"City View", nil),
                NSLocalizedString(@"Villa", nil),
                NSLocalizedString(@"Bungalow", nil),
                NSLocalizedString(@"Courtyard", nil), nil];
        
        
    }else if (dataType == BED){
        /*
        wsGetBed = [[WSGetBed alloc] init];
        wsGetBed.delegate = self;
        [wsGetBed getList];
        */
        array= [[NSArray alloc] initWithObjects:@"Двуспальная кровать",@"Размер Queen",@"Размер King",@"Отдельные кровати", nil];
        
    } else if(dataType == COUNTRY){
        /*
        wsGetCountries = [[WSGetCountriesList alloc] init];
        wsGetCountries.delegate = self;
        [wsGetCountries getList];
        */
//        array= [[NSArray alloc] initWithObjects:@"Macao",
//                @"Palau",
//                @"Австралия",
//                @"Австрия",
//                @"Азербайджан",
//                @"Албания",
//                @"Алжир",
//                @"Американское Самоа",
//                @"Ангилья",
//                @"Ангола",
//                @"Андорра",
//                @"Антарктида",
//                @"Антигуа и Барбуда",
//                @"Аргентина",
//                @"Армения",
//                @"Аруба",
//                @"Афганистан",
//                @"Багамские о-ва",
//                @"Бангладеш",
//                @"Барбадос",
//                @"Бахрейн",
//                @"Беларусь",
//                @"Белиз",
//                @"Бельгия",
//                @"Бенин",
//                @"Берег Слоновой Кости",
//                @"Бермудские острова",
//                @"Болгария",
//                @"Боливия",
//                @"Босния и Герцеговина",
//                @"Ботсвана",
//                @"Бразилия",
//                @"Британская территория Индийского океана",
//                @"Бруней-Даруссалам",
//                @"Буркина Фасо",
//                @"Бурунди",
//                @"Бутан",
//                @"Вануату",
//                @"Великобритания",
//                @"Венгрия",
//                @"Венесуэла",
//                @"Виргинские острова, Британские",
//                @"Виргинские острова, США",
//                @"Внешние малые острова США",
//                @"Вьетнам",
//                @"Габон",
//                @"Гаити",
//                @"Гайана",
//                @"Гамбия",
//                @"Гана",
//                @"Гваделупа",
//                @"Гватемала",
//                @"Гвинея",
//                @"Гвинея-Бисау",
//                @"Германия",
//                @"Гибралтар",
//                @"Гондурас",
//                @"Гонконг",
//                @"Гренада",
//                @"Гренландия",
//                @"Греция",
//                @"Грузия",
//                @"Гуам",
//                @"Дания",
//                @"Джибути",
//                @"Доминика",
//                @"Доминиканская Республика",
//                @"Египет",
//                @"Замбия",
//                @"Западная Сахара",
//                @"Зимбабве",
//                @"Израиль",
//                @"Индия",
//                @"Индонезия",
//                @"Иордания",
//                @"Ирак",
//                @"Иран, Исламская Республика",
//                @"Ирландия",
//                @"Исландия",
//                @"Испания",
//                @"Италия",
//                @"Йемен",
//                @"Кабо-Верде",
//                @"Казахстан",
//                @"Каймановы острова",
//                @"Камбоджа",
//                @"Камерун",
//                @"Канада",
//                @"Катар",
//                @"Кения",
//                @"Кипр",
//                @"Киргизия",
//                @"Кирибати",
//                @"Китай",
//                @"Кокосовые (Килинг) острова",
//                @"Колумбия",
//                @"Коморские острова",
//                @"Конго",
//                @"Конго, Демократическая Республика",
//                @"Корея, Народно-Демократическая Республика",
//                @"Корея, Республика",
//                @"Коста-Рика",
//                @"Куба",
//                @"Кувейт",
//                @"Лаосская Народно-Демократическая Республика",
//                @"Латвия",
//                @"Лесото",
//                @"Либерия",
//                @"Ливан",
//                @"Ливийская арабская джамахирия",
//                @"Литва",
//                @"Лихтенштейн",
//                @"Люксембург",
//                @"Маврикий",
//                @"Мавритания",
//                @"Мадагаскар",
//                @"Майотта",
//                @"Македония, бывшая Югославская Республика ",
//                @"Малави",
//                @"Малайзия",
//                @"Мали",
//                @"Мальдивы",
//                @"Мальта",
//                @"Марокко",
//                @"Мартиника",
//                @"Маршалловы острова",
//                @"Мексика",
//                @"Микронезия, Федеративные Штаты",
//                @"Мозамбик",
//                @"Молдова",
//                @"Монако",
//                @"Монголия",
//                @"Монсеррат",
//                @"Мьянма",
//                @"Намибия",
//                @"Науру",
//                @"Непал",
//                @"Нигер",
//                @"Нигерия",
//                @"Нидерландские Антильские острова",
//                @"Нидерланды",
//                @"Никарагуа",
//                @"Ниуэ",
//                @"Новая Зеландия",
//                @"Новая Каледония",
//                @"Норвегия",
//                @"Объединенные Арабские Эмираты",
//                @"Оман",
//                @"Остров Буве",
//                @"Остров Норфолк",
//                @"Остров Рождества",
//                @"Остров Херд и острова Макдональд",
//                @"Острова Кука",
//                @"Острова Теркс и Кайкос",
//                @"Пакистан",
//                @"Палестинская территория, оккупированная",
//                @"Панама",
//                @"Папуа - Новая Гвинея",
//                @"Парагвай",
//                @"Перу",
//                @"Питкэрн",
//                @"Польша",
//                @"Португалия",
//                @"Пуэрто-Рико",
//                @"Реюньон",
//                @"Российская Федерация",
//                @"Руанда",
//                @"Румыния",
//                @"Сальвадор",
//                @"Самоа",
//                @"Сан - Марино",
//                @"Сан-Томе и Принсипи",
//                @"Саудовская Аравия",
//                @"Свазиленд",
//                @"Святейший Престол (Ватикан)",
//                @"Святой Елены остров",
//                @"Северные Марианские острова",
//                @"Сейшельские острова",
//                @"Сен-Пьер и Микелон",
//                @"Сенегал",
//                @"Сент-Винсент и Гренадины",
//                @"Сент-Китс и Невис",
//                @"Сент-Люсия",
//                @"Сербия и Черногория",
//                @"Сингапур",
//                @"Сирийская Арабская Республика",
//                @"Словакия",
//                @"Словения",
//                @"Соединенные Штаты Америки",
//                @"Соломоновы острова",
//                @"Сомали",
//                @"Судан",
//                @"Суринам",
//                @"Сьерра-Леоне",
//                @"Таджикистан",
//                @"Таиланд",
//                @"Тайвань, провинция Китая",
//                @"Танзания, Объединенная Республика",
//                @"Тимор-Лешти",
//                @"Того",
//                @"Токелау",
//                @"Тонга",
//                @"Тринидад и Тобаго",
//                @"Тувалу",
//                @"Тунис",
//                @"Туркменистан",
//                @"Турция",
//                @"Уганда",
//                @"Узбекистан",
//                @"Украина",
//                @"Уоллис и Футуна",
//                @"Уругвай",
//                @"Фарерские острова",
//                @"Фиджи",
//                @"Филиппины",
//                @"Финляндия",
//                @"Фолклендские (Мальвинские) острова",
//                @"Франция",
//                @"Французская Гвиана",
//                @"Французская Полинезия",
//                @"Хорватия",
//                @"Центрально-Африканская Республика",
//                @"Чад",
//                @"Чешская Республика",
//                @"Чили",
//                @"Швейцария",
//                @"Швеция",
//                @"Шпицберген и Ян-Майен",
//                @"Шри Ланка",
//                @"Эквадор",
//                @"Экваториальная Гвинея",
//                @"Эритрея",
//                @"Эстония",
//                @"Эфиопия",
//                @"Южная Африка",
//                @"Южная Джорджия и Южные Сандвичевы острова",
//                @"Южные Французские Территории",
//                @"Ямайка",
//                @"Япония", nil];
        NSArray *termArray= [[NSArray alloc] initWithObjects:NSLocalizedString(@"Afghanistan", nil),
                             NSLocalizedString(@"Albania", nil),
                             NSLocalizedString(@"Algeria", nil),
                             NSLocalizedString(@"American Samoa", nil),
                             NSLocalizedString(@"Andorra", nil),
                             NSLocalizedString(@"Angola", nil),
                             NSLocalizedString(@"Anguilla", nil),
                             NSLocalizedString(@"Antarctica", nil),
                             NSLocalizedString(@"Antigua and Barbuda", nil),
                             NSLocalizedString(@"Argentina", nil),
                             NSLocalizedString(@"Armenia", nil),
                             NSLocalizedString(@"Aruba", nil),
                             NSLocalizedString(@"Australia", nil),
                             NSLocalizedString(@"Austria", nil),
                             NSLocalizedString(@"Azerbaijan", nil),
                             NSLocalizedString(@"Bahamas", nil),
                             NSLocalizedString(@"Bahrain", nil),
                             NSLocalizedString(@"Bangladesh", nil),
                             NSLocalizedString(@"Barbados", nil),
                             NSLocalizedString(@"Belarus", nil),
                             NSLocalizedString(@"Belgium", nil),
                             NSLocalizedString(@"Belize", nil),
                             NSLocalizedString(@"Benin", nil),
                             NSLocalizedString(@"Bermuda", nil),
                             NSLocalizedString(@"Bhutan", nil),
                             NSLocalizedString(@"Bolivia", nil),
                             NSLocalizedString(@"Bosnia and Herzegovina", nil),
                             NSLocalizedString(@"Botswana", nil),
                             NSLocalizedString(@"Bouvet Island", nil),
                             NSLocalizedString(@"Brazil", nil),
                             NSLocalizedString(@"British Indian Ocean Territory", nil),
                             NSLocalizedString(@"Brunei Darussalam", nil),
                             NSLocalizedString(@"Bulgaria", nil),
                             NSLocalizedString(@"Burkina Faso", nil),
                             NSLocalizedString(@"Burundi", nil),
                             NSLocalizedString(@"Cambodia", nil),
                             NSLocalizedString(@"Cameroon", nil),
                             NSLocalizedString(@"Canada", nil),
                             NSLocalizedString(@"Cape Verde", nil),
                             NSLocalizedString(@"Cayman Islands", nil),
                             NSLocalizedString(@"Central African Republic", nil),
                             NSLocalizedString(@"Chad", nil),
                             NSLocalizedString(@"Chile", nil),
                             NSLocalizedString(@"China", nil),
                             NSLocalizedString(@"Christmas Island", nil),
                             NSLocalizedString(@"Cocos (Keeling) Islands", nil),
                             NSLocalizedString(@"Colombia", nil),
                             NSLocalizedString(@"Comoros", nil),
                             NSLocalizedString(@"Congo", nil),
                             NSLocalizedString(@"Congo, the Democratic Republic of the", nil),
                             NSLocalizedString(@"Cook Islands", nil),
                             NSLocalizedString(@"Costa Rica", nil),
                             NSLocalizedString(@"Cote D'Ivoire", nil),
                             NSLocalizedString(@"Croatia", nil),
                             NSLocalizedString(@"Cuba", nil),
                             NSLocalizedString(@"Cyprus", nil),
                             NSLocalizedString(@"Czech Republic", nil),
                             NSLocalizedString(@"Denmark", nil),
                             NSLocalizedString(@"Djibouti", nil),
                             NSLocalizedString(@"Dominica", nil),
                             NSLocalizedString(@"Dominican Republic", nil),
                             NSLocalizedString(@"Ecuador", nil),
                             NSLocalizedString(@"Egypt", nil),
                             NSLocalizedString(@"El Salvador", nil),
                             NSLocalizedString(@"Equatorial Guinea", nil),
                             NSLocalizedString(@"Eritrea", nil),
                             NSLocalizedString(@"Estonia", nil),
                             NSLocalizedString(@"Ethiopia", nil),
                             NSLocalizedString(@"Falkland Islands (Malvinas)", nil),
                             NSLocalizedString(@"Faroe Islands", nil),
                             NSLocalizedString(@"Fiji", nil),
                             NSLocalizedString(@"Finland", nil),
                             NSLocalizedString(@"France", nil),
                             NSLocalizedString(@"French Guiana", nil),
                             NSLocalizedString(@"French Polynesia", nil),
                             NSLocalizedString(@"French Southern Territories", nil),
                             NSLocalizedString(@"Gabon", nil),
                             NSLocalizedString(@"Gambia", nil),
                             NSLocalizedString(@"Georgia", nil),
                             NSLocalizedString(@"Germany", nil),
                             NSLocalizedString(@"Ghana", nil),
                             NSLocalizedString(@"Gibraltar", nil),
                             NSLocalizedString(@"Greece", nil),
                             NSLocalizedString(@"Greenland", nil),
                             NSLocalizedString(@"Grenada", nil),
                             NSLocalizedString(@"Guadeloupe", nil),
                             NSLocalizedString(@"Guam", nil),
                             NSLocalizedString(@"Guatemala", nil),
                             NSLocalizedString(@"Guinea", nil),
                             NSLocalizedString(@"Guinea-Bissau", nil),
                             NSLocalizedString(@"Guyana", nil),
                             NSLocalizedString(@"Haiti", nil),
                             NSLocalizedString(@"Heard Island and Mcdonald Islands", nil),
                             NSLocalizedString(@"Holy See (Vatican City State)", nil),
                             NSLocalizedString(@"Honduras", nil),
                             NSLocalizedString(@"Hong Kong", nil),
                             NSLocalizedString(@"Hungary", nil),
                             NSLocalizedString(@"Iceland", nil),
                             NSLocalizedString(@"India", nil),
                             NSLocalizedString(@"Indonesia", nil),
                             NSLocalizedString(@"Iran, Islamic Republic of", nil),
                             NSLocalizedString(@"Iraq", nil),
                             NSLocalizedString(@"Ireland", nil),
                             NSLocalizedString(@"Israel", nil),
                             NSLocalizedString(@"Italy", nil),
                             NSLocalizedString(@"Jamaica", nil),
                             NSLocalizedString(@"Japan", nil),
                             NSLocalizedString(@"Jordan", nil),
                             NSLocalizedString(@"Kazakhstan", nil),
                             NSLocalizedString(@"Kenya", nil),
                             NSLocalizedString(@"Kiribati", nil),
                             NSLocalizedString(@"Korea, Democratic People's Republic of", nil),
                             NSLocalizedString(@"Korea, Republic of", nil),
                             NSLocalizedString(@"Kuwait", nil),
                             NSLocalizedString(@"Kyrgyzstan", nil),
                             NSLocalizedString(@"Lao People's Democratic Republic", nil),
                             NSLocalizedString(@"Latvia", nil),
                             NSLocalizedString(@"Lebanon", nil),
                             NSLocalizedString(@"Lesotho", nil),
                             NSLocalizedString(@"Liberia", nil),
                             NSLocalizedString(@"Libyan Arab Jamahiriya", nil),
                             NSLocalizedString(@"Liechtenstein", nil),
                             NSLocalizedString(@"Lithuania", nil),
                             NSLocalizedString(@"Luxembourg", nil),
                             NSLocalizedString(@"Macao", nil),
                             NSLocalizedString(@"Macedonia, the Former Yugoslav Republic of", nil),
                             NSLocalizedString(@"Madagascar", nil),
                             NSLocalizedString(@"Malawi", nil),
                             NSLocalizedString(@"Malaysia", nil),
                             NSLocalizedString(@"Maldives", nil),
                             NSLocalizedString(@"Mali", nil),
                             NSLocalizedString(@"Malta", nil),
                             NSLocalizedString(@"Marshall Islands", nil),
                             NSLocalizedString(@"Martinique", nil),
                             NSLocalizedString(@"Mauritania", nil),
                             NSLocalizedString(@"Mauritius", nil),
                             NSLocalizedString(@"Mayotte", nil),
                             NSLocalizedString(@"Mexico", nil),
                             NSLocalizedString(@"Micronesia, Federated States of", nil),
                             NSLocalizedString(@"Moldova, Republic of", nil),
                             NSLocalizedString(@"Monaco", nil),
                             NSLocalizedString(@"Mongolia", nil),
                             NSLocalizedString(@"Montserrat", nil),
                             NSLocalizedString(@"Morocco", nil),
                             NSLocalizedString(@"Mozambique", nil),
                             NSLocalizedString(@"Myanmar", nil),
                             NSLocalizedString(@"Namibia", nil),
                             NSLocalizedString(@"Nauru", nil),
                             NSLocalizedString(@"Nepal", nil),
                             NSLocalizedString(@"Netherlands", nil),
                             NSLocalizedString(@"Netherlands Antilles", nil),
                             NSLocalizedString(@"New Caledonia", nil),
                             NSLocalizedString(@"New Zealand", nil),
                             NSLocalizedString(@"Nicaragua", nil),
                             NSLocalizedString(@"Niger", nil),
                             NSLocalizedString(@"Nigeria", nil),
                             NSLocalizedString(@"Niue", nil),
                             NSLocalizedString(@"Norfolk Island", nil),
                             NSLocalizedString(@"Northern Mariana Islands", nil),
                             NSLocalizedString(@"Norway", nil),
                             NSLocalizedString(@"Oman", nil),
                             NSLocalizedString(@"Pakistan", nil),
                             NSLocalizedString(@"Palau", nil),
                             NSLocalizedString(@"Palestinian Territory, Occupied", nil),
                             NSLocalizedString(@"Panama", nil),
                             NSLocalizedString(@"Papua New Guinea", nil),
                             NSLocalizedString(@"Paraguay", nil),
                             NSLocalizedString(@"Peru", nil),
                             NSLocalizedString(@"Philippines", nil),
                             NSLocalizedString(@"Pitcairn", nil),
                             NSLocalizedString(@"Poland", nil),
                             NSLocalizedString(@"Portugal", nil),
                             NSLocalizedString(@"Puerto Rico", nil),
                             NSLocalizedString(@"Qatar", nil),
                             NSLocalizedString(@"Reunion", nil),
                             NSLocalizedString(@"Romania", nil),
                             NSLocalizedString(@"Russian Federation", nil),
                             NSLocalizedString(@"Rwanda", nil),
                             NSLocalizedString(@"Saint Helena", nil),
                             NSLocalizedString(@"Saint Kitts and Nevis", nil),
                             NSLocalizedString(@"Saint Lucia", nil),
                             NSLocalizedString(@"Saint Pierre and Miquelon", nil),
                             NSLocalizedString(@"Saint Vincent and the Grenadines", nil),
                             NSLocalizedString(@"Samoa", nil),
                             NSLocalizedString(@"San Marino", nil),
                             NSLocalizedString(@"Sao Tome and Principe", nil),
                             NSLocalizedString(@"Saudi Arabia", nil),
                             NSLocalizedString(@"Senegal", nil),
                             NSLocalizedString(@"Serbia and Montenegro", nil),
                             NSLocalizedString(@"Seychelles", nil),
                             NSLocalizedString(@"Sierra Leone", nil),
                             NSLocalizedString(@"Singapore", nil),
                             NSLocalizedString(@"Slovakia", nil),
                             NSLocalizedString(@"Slovenia", nil),
                             NSLocalizedString(@"Solomon Islands", nil),
                             NSLocalizedString(@"Somalia", nil),
                             NSLocalizedString(@"South Africa", nil),
                             NSLocalizedString(@"South Georgia and the South Sandwich Islands", nil),
                             NSLocalizedString(@"Spain", nil),
                             NSLocalizedString(@"Sri Lanka", nil),
                             NSLocalizedString(@"Sudan", nil),
                             NSLocalizedString(@"Suriname", nil),
                             NSLocalizedString(@"Svalbard and Jan Mayen", nil),
                             NSLocalizedString(@"Swaziland", nil),
                             NSLocalizedString(@"Sweden", nil),
                             NSLocalizedString(@"Switzerland", nil),
                             NSLocalizedString(@"Syrian Arab Republic", nil),
                             NSLocalizedString(@"Taiwan, Province of China", nil),
                             NSLocalizedString(@"Tajikistan", nil),
                             NSLocalizedString(@"Tanzania, United Republic of", nil),
                             NSLocalizedString(@"Thailand", nil),
                             NSLocalizedString(@"Timor-Leste", nil),
                             NSLocalizedString(@"Togo", nil),
                             NSLocalizedString(@"Tokelau", nil),
                             NSLocalizedString(@"Tonga", nil),
                             NSLocalizedString(@"Trinidad and Tobago", nil),
                             NSLocalizedString(@"Tunisia", nil),
                             NSLocalizedString(@"Turkey", nil),
                             NSLocalizedString(@"Turkmenistan", nil),
                             NSLocalizedString(@"Turks and Caicos Islands", nil),
                             NSLocalizedString(@"Tuvalu", nil),
                             NSLocalizedString(@"Uganda", nil),
                             NSLocalizedString(@"Ukraine", nil),
                             NSLocalizedString(@"United Arab Emirates", nil),
                             NSLocalizedString(@"United Kingdom", nil),
                             NSLocalizedString(@"United States", nil),
                             NSLocalizedString(@"United States Minor Outlying Islands", nil),
                             NSLocalizedString(@"Uruguay", nil),
                             NSLocalizedString(@"Uzbekistan", nil),
                             NSLocalizedString(@"Vanuatu", nil),
                             NSLocalizedString(@"Venezuela", nil),
                             NSLocalizedString(@"Viet Nam", nil),
                             NSLocalizedString(@"Virgin Islands, British", nil),
                             NSLocalizedString(@"Virgin Islands, U.s.", nil),
                             NSLocalizedString(@"Wallis and Futuna", nil),
                             NSLocalizedString(@"Western Sahara", nil),
                             NSLocalizedString(@"Yemen", nil),
                             NSLocalizedString(@"Zambia", nil),
                             NSLocalizedString(@"Zimbabwe", nil),nil];
        
        array = [termArray sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2){ return [[obj1 lowercaseString] compare:[obj2 lowercaseString] options:NSLiteralSearch];}];
        
    } else if(dataType == CITY){
        /*wsGetCities = [[WSGetCitiesList alloc] init];
        wsGetCities.filterCountry = _filterCountryForCitiesList;
        wsGetCities.delegate = self;
        [wsGetCities getList];*/
    }else if(dataType == TRANSPORT){
        /*
        wsGetTransportType = [[WSGetTransportType alloc] init];
        wsGetTransportType.delegate = self;
        [wsGetTransportType getList];
        */
        array= [[NSArray alloc] initWithObjects:NSLocalizedString(@"Bus", nil),
                NSLocalizedString(@"Private car/Limo", nil),
                NSLocalizedString(@"Taxi", nil),
                NSLocalizedString(@"Train", nil), nil];
        
    }else if(dataType == EVENT_CATEGORY){

        array = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Concerts", nil),
                NSLocalizedString(@"Sports", nil),
                 NSLocalizedString(@"Theatre", nil),
                 NSLocalizedString(@"Other Event", nil), nil];
       
    } else if(dataType == STATE){
        
        
        if(countryType == USA){
            array = [[NSArray alloc] initWithObjects:@"Айдахо",
                     @"Айова",
                     @"Алабама",
                     @"Аляска",
                     @"Аризона",
                     @"Арканзас",
                     @"Вайоминг",
                     @"Вашингтон",
                     @"Вермонт",
                     @"Вирджиния",
                     @"Висконсин",
                     @"Гавайи",
                     @"Делавэр",
                     @"Джорджия",
                     @"Западная Виргиния",
                     @"Иллинойс",
                     @"Индиана",
                     @"Калифорния",
                     @"Канзас",
                     @"Кентуки",
                     @"Колорадо",
                     @"Коннектикут",
                     @"Луизиана",
                     @"Массачусетс",
                     @"Миннесота",
                     @"Миссисипи",
                     @"Миссури",
                     @"Мичиган",
                     @"Монтана",
                     @"Мэн",
                     @"Мэриленд",
                     @"Небраска",
                     @"Невада",
                     @"Нью-Гэмпшир",
                     @"Нью-Джерси",
                     @"Нью-Йорк",
                     @"Нью-Мехико",
                     @"Огайо",
                     @"Оклахома",
                     @"Округ Колумбия",
                     @"Орегон",
                     @"Пенсильвания",
                     @"Род-Айленд",
                     @"Северная Дакота",
                     @"Северная Каролина",
                     @"Теннесси",
                     @"Техас",
                     @"Флорида",
                     @"Южная Дакота",
                     @"Южная Каролина",
                     @"Юта", nil];
        }else if(countryType == CANADA){ // Canada
            array = [[NSArray alloc] initWithObjects:@"Альберта",
                     @"Британская Колумбия",
                     @"Манитоба",
                     @"Новая Шотландия",
                     @"Нунавут",
                     @"Нью-Брансуик",
                     @"Онтарио",
                     @"Остров Принца Эдуарда",
                     @"Саскачеван",
                     @"Юкон", nil];
        } else {
            array = [NSArray array];
        }
    }
    
    if(array){
        dataFull = [self getHarcodeData:array];
        data= array;
        filteredData = data;
        filteredDataFull = dataFull;
        [table reloadData];
        [self stopToast];
        
        // Remove selected values that doesn't include in data array
        if(selectedValues && selectedValues.count>0){
            NSArray *tempValues = [NSArray arrayWithArray:selectedValues];
            for (NSString* value in tempValues) {
                if([data containsObject:value]){
                    continue;
                } else {
                    [selectedValues removeObject:value];
                    [selectedKey removeObject:value];
                }
            }
        }
    }
    
}

-(NSArray*)getHarcodeData:(NSArray*)arrData{
    NSMutableArray* resultData= [[NSMutableArray alloc] init];
    
    for (int i = 0; i<arrData.count; i++) {
        NSString* value = [arrData objectAtIndex:i];
        NSMutableDictionary* dict= [[NSMutableDictionary alloc]  init];
        [dict setObject:value forKey:@"Value"];
        [dict setObject:value forKey:@"Key"];
        [resultData addObject:dict];
    }
    
    return resultData;
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_CUISINE){
        data = ((WSGetCuisines*)ws).cuisineList;
    } else if(ws.task == WS_GET_OCCASION){
        data = ((WSGetOccasion*)ws).occasionList;
    } else if(ws.task == WS_GET_RETAL_VEHICLE){
        data = ((WSGetRetalVehicle*)ws).retalVehicleList;
    }else if(ws.task == WS_GET_COMPANY){
        data = ((WSGetCompanyPreferences*)ws).companyList;
    }else if(ws.task == WS_GET_TEE_TIMES){
        data = ((WSGetTeeTimes*)ws).teeList;
    }else if(ws.task == WS_GET_ROOM){
        data = ((WSGetRoom*)ws).roomList;
    }else if(ws.task == WS_GET_BED){
        data = ((WSGetBed*)ws).bedList;
    }else if(ws.task == WS_GET_CUISINE_PREFERENCE){
        data = ((WSGetCuisinePre*)ws).cuisineList;
    }else if(ws.task == WS_GET_COUNTRY){
        data = ((WSGetCountriesList*)ws).countriesList;
    }else if(ws.task == WS_GET_CITY){
        data = ((WSGetCitiesList*)ws).citiesList;
    }else if(ws.task == WS_GET_TRANSPORT_TYPE){
        data = ((WSGetTransportType*)ws).transportList;
    }
    
    [table reloadData];
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result!=nil && result.message!=nil){
       
    }
}*/


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0*SCREEN_SCALE;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CCCell";
    
    CCNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"CCNumberCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UIFont* font = cell.labelName.font;
        [cell.labelName setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];

    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    NSString* value = [filteredData objectAtIndex:indexPath.row];
    cell.labelName.text = value;
    value = removeSpecialCharacters(value);
    cell.btCheck.hidden = ![selectedValues containsObject:value];
    
    /*
    if(dataType == COUNTRY && selectedKey && selectedKey.count>0){
        NSString* key = [selectedKey objectAtIndex:0];
        cell.btCheck.hidden = ![value containsString:[NSString stringWithFormat:@"(%@)", key]];
    }
    */
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (cell && filteredData.count > indexPath.row) {
        if(((CCNumberCell*)cell).btCheck.hidden){
            if(self.maxCount ==1){
                [selectedValues removeAllObjects];
                [selectedKey removeAllObjects];
                [tableView reloadData];
            }
            if([self isMaxCount] || _isCloseAfterClick){
                [selectedValues addObject:[filteredData objectAtIndex:indexPath.row]];
                ((CCNumberCell*)cell).btCheck.hidden = false;
                if (dataType == ROOM){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == BED){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == RETAL_VERTICLE){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == COMPANY){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == TEE){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == CUISINES){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == COUNTRY){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == CITY){
                   // CityObject* city = [wsGetCities.citiesFullList objectAtIndex:indexPath.row];
                   // [selectedKey addObject:city.cityName];
                }else if (dataType == TRANSPORT){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == OCCASION){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == EVENT_CATEGORY){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }else if (dataType == STATE){
                    NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                    NSString* key = [dict objectForKey:@"Key"];
                    [selectedKey addObject:key];
                }
            }
            
            if(_maxCount==1){
                [self returnSelectedValue];
            }
        } else {
            ((CCNumberCell*)cell).btCheck.hidden = true;
            [selectedValues removeObject:[filteredData objectAtIndex:indexPath.row]];
            
            if (dataType == ROOM){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == BED){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == RETAL_VERTICLE){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == COMPANY){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == TEE){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == CUISINES){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == COUNTRY){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == CITY){
                /*CityObject* city = [wsGetCities.citiesFullList objectAtIndex:indexPath.row];
                [selectedKey removeObject:city.cityName];*/
            }else if (dataType == TRANSPORT){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == OCCASION){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == EVENT_CATEGORY){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }else if (dataType == STATE){
                NSDictionary* dict = [filteredDataFull objectAtIndex:indexPath.row];
                NSString* key = [dict objectForKey:@"Key"];
                [selectedKey removeObject:key];
            }
            if(_maxCount==1){
                [self returnSelectedValue];
            }
        }
    }
}

-(BOOL)isMaxCount{
    if(self.maxCount >0 && selectedValues.count>=self.maxCount){
        return NO;
    }
    return YES;
}

#pragma SearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText && searchText.length > 0){
        filteredData = [self filterItems:searchText inList:data];
        filteredDataFull = [self getHarcodeData:filteredData];
    } else {
        filteredData = data;
        filteredDataFull = dataFull;
    }
    [table reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


-(NSArray*) filterItems:(NSString*) term inList:(NSArray*) fullList{
    NSMutableArray *result = [NSMutableArray array];
    if(fullList!=nil){
        for (NSString* item in fullList) {
            if([[item lowercaseString] hasPrefix:[term lowercaseString]]){
                [result addObject:item];
            }
        }
    }
    return  result;
}
@end




