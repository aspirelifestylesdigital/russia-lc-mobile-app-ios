//
//  DCRSubItemWithIconView.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DCRSubItemWithIconView : UIView
@property (strong, nonatomic) IBOutlet UIButton *btViewMore;
@property (strong, nonatomic) IBOutlet UILabel *lbTite;
@property (strong, nonatomic) IBOutlet UILabel *lbValue;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UIView *view;

-(void) showView:(NSString*)iconName withTitle:(NSString*) title value:(NSString*)value action:(void(^)())clicked;
@end

