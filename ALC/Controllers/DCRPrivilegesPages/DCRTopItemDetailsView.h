//
//  DCRTopItemDetailsView.h
//  ALC
//
//  Created by Anh Tran on 3/16/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCRItemDetails.h"
#import "BaseBookingDetailsObject.h"
@interface DCRTopItemDetailsView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *imgTopImage;
@property (strong, nonatomic) IBOutlet UILabel* lbItemName;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *viewEditRequestInfo;
@property (strong, nonatomic) IBOutlet UILabel *lbEditPrivilegeName;
@property (strong, nonatomic) IBOutlet UILabel *lbEditPrivilegeFullAddress;
@property (strong, nonatomic) IBOutlet UIView *viewCreatePrivilegeName;
@property (strong, nonatomic) IBOutlet UILabel *lbImageCopyright;
@property (strong, nonatomic) IBOutlet UIView *viewImageCopyright;

- (void) showItemDetails:(DCRItemDetails*) item;
- (void) showEditItemDetails:(BaseBookingDetailsObject*) item;
@end
