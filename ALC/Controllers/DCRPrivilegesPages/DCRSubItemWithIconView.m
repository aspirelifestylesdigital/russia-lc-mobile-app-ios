//
//  DCRSubItemWithIconView.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRSubItemWithIconView.h"

@interface DCRSubItemWithIconView(){
    
}
@property (nonatomic, strong) void(^completionClicked)();
@end

@implementation DCRSubItemWithIconView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"DCRSubItemWithIconView" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    resetScaleViewBaseOnScreen(self);
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        resetScaleViewBaseOnScreen(self);
        return self;
    }
    
    return self;
}
- (IBAction)actionClickedOnView:(id)sender {
    if(_completionClicked){
        _completionClicked();
    }
}

-(void) showView:(NSString*)iconName withTitle:(NSString*) title value:(NSString*)value action:(void(^)())clicked{
    
    if(clicked != nil){
        _btViewMore.hidden = NO;
        _completionClicked = clicked;
    }
   
    if(iconName){
        [_icon setImage:[UIImage imageNamed:iconName]];
       
    }
    if(title){
        [_lbTite setText:title];
    }
    if(value){
        [_lbValue setAttributedText:[value stringByAddHtml]];

    }
    
}


@end
