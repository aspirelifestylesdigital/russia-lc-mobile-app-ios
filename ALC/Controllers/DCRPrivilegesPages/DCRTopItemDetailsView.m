//
//  DCRTopItemDetailsView.m
//  ALC
//
//  Created by Anh Tran on 3/16/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRTopItemDetailsView.h"
#import "UIImageView+AFNetworking.h"
@interface DCRTopItemDetailsView(){
    
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintImageHeight;

@end

@implementation DCRTopItemDetailsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"DCRTopItemDetailsView" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    resetScaleViewBaseOnScreen(self);
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        return self;
    }
    
    return self;
}

// Show new request submission
- (void) showItemDetails:(DCRItemDetails*) item{
    _constraintImageHeight.constant = SCREEN_HEIGHT / 3;
    
    [_lbItemName setText:item.itemName];
    
    if([item.getFirstImageUrl hasPrefix:@"http"]){
        [_imgTopImage setImageWithURL:[NSURL URLWithString:item.getFirstImageUrl] placeholderImage:nil];
    } else {
        //get image from resouce
        [_imgTopImage setImage:[UIImage imageNamed:item.getFirstImageUrl]];
    }
    
    [_lbImageCopyright setText:[NSString stringWithFormat:@"\n%@ %@ %@\n\n",
                                NSLocalizedString(@"*image is property of", nil), item.itemName,
                                NSLocalizedString(@"and shall not be reproduced without permission", nil)]];

    _viewEditRequestInfo.hidden = YES;
}

- (void) showEditItemDetails:(BaseBookingDetailsObject*) item{
    _imgTopImage.hidden = YES;
    _viewCreatePrivilegeName.hidden = YES;
    _viewImageCopyright.hidden = YES;
    
    [_lbEditPrivilegeName setText:[item privilegeName]];
    [_lbEditPrivilegeFullAddress setText:[item.requestDetails stringForKey:FullAddress]];
}

@end
