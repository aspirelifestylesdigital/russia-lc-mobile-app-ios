//
//  DCRDescriptionView.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCRItemDetails.h"
@interface DCRDescriptionView : UIView
@property (strong, nonatomic) IBOutlet UILabel *lbDescription;
@property (strong, nonatomic) IBOutlet UIButton *btReadmore;
@property (strong, nonatomic) IBOutlet UIButton *btTermsOfUse;
@property (strong, nonatomic) IBOutlet UILabel * lbTermsOfUse;
@property (strong, nonatomic) IBOutlet UIView *viewTermsOfUse;
@property (strong, nonatomic) IBOutlet UIView *view;
-(void) showData:(DCRItemDetails*) item;
@end
