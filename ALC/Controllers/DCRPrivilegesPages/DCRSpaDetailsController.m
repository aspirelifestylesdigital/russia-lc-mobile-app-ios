//
//  DCRSpaDetailsController.m
//  ALC
//
//  Created by Anh Tran on 3/22/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRSpaDetailsController.h"
#import "OrthersBookingController.h"

@interface DCRSpaDetailsController ()

@end
@implementation DCRSpaDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:@"DCRItemDetailsController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



- (void) displayItem:(DCRItemDetails*) item{
    [super displayItem:item];
    
    //Add more subview
    [self hideSummaryView];
    
    //Add privilege
    [self addPrivilegeView];
    
    //Add Location view
    [self addLocationView];
    
    //Add Service  view
    //[self addSubView:@"info" withTitle:NSLocalizedString(@"SERVICES",nil) value:item.spaService actionOnClicked:nil];
    
    //Add Opening Hours view
    [self addOpenHoursView];

    //Add Treatsment  view
    /* [self addSubView:@"info" withTitle:NSLocalizedString(@"TREATMENTS",nil) value:item.spaTreatment actionOnClicked:nil];*/
    
    //Add Web view
    [self addWebSiteView:NSLocalizedString(@"Visit the spa's website",nil)];
    
    [self addFooterText];
    
    //[self.view layoutIfNeeded];
}


-(void) goToRequestForm:(DCRItemDetails*) item{
    OrthersBookingController* controller = (OrthersBookingController*) [self controllerByIdentifier:@"OrthersBookingController"];
    controller.privilegeDetails = item;
    [self.navigationController pushViewController:controller animated:YES];
}


@end
