//
//  DCRDiningDetailsController.m
//  ALC
//
//  Created by Anh Tran on 3/15/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRDiningDetailsController.h"
#import "GourmetRequestController.h"

@interface DCRDiningDetailsController ()

@end

@implementation DCRDiningDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:@"DCRItemDetailsController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void) displayItem:(DCRItemDetails*) item{
    [super displayItem:item];
    
    //Add more subview
    
    //TODO Add privilege
    [self addPrivilegeView];
   
    //Add Location view
    [self addLocationView];
    
    //Add Cuisine view
    if(item.cuisines && item.cuisines.count > 0){
        NSString* fullCuisine = [item fullValueFromDCRCommonArray:item.cuisines];
        [self addSubView:@"eat" withTitle:NSLocalizedString(@"CUISINE",nil) value:fullCuisine actionOnClicked:nil];
    }
    
    //Add open hours
    [self addOpenHoursView];
    
    
    //Add Price view
    if([item.priceRange isValid]){
        [self addSubView:@"icon-price" withTitle:NSLocalizedString(@"PRICE",nil) value:[NSString stringWithFormat:@"%@%@", ([item.priceCurrency isValid]? item.priceCurrency.Name : @""), [self convertPriceRange:item.priceRange.Name]] actionOnClicked:nil];
    }
    
    //Add Web view
    [self addWebSiteView:NSLocalizedString(@"Visit the restaurant's website",nil)];
    
    [self addFooterText];
    
    //[self.view layoutIfNeeded];
}


-(void) goToRequestForm:(DCRItemDetails*) item{
    GourmetRequestController* controller = (GourmetRequestController*) [self controllerByIdentifier:@"GourmetRequestController"];
    controller.privilegeDetails = item;
    [self.navigationController pushViewController:controller animated:YES];

}

-(NSString*) convertPriceRange:(NSString*) value{
    if ([[[NSNumberFormatter alloc] init] numberFromString:value] != nil) {
        int number = [value intValue];
        NSString* result = @"";
        for (int i =0; i < number; i++) {
            result = [result stringByAppendingString:@"₽"];
        }
        return result;
    }
    return  value;
}
@end
