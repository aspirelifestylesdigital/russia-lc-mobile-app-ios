//
//  RegisterViewController.h
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "CCNumberController.h"
#import "WSCreateUser.h"
#import "WSUpdateProfile.h"
#import "HGCheckCountryCode.h"
#import "ErrorToolTip.h"

@interface RegisterViewController : MainViewController<CCNumberControllerDelegate, UITextFieldDelegate, DataLoadDelegate, ErrorToolTipDelegate, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pHeightPass;
@property (weak, nonatomic) IBOutlet UIView *pvPass;
@property (weak, nonatomic) IBOutlet UIView *pvMainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pHeightMain;
@property(nonatomic, assign)BOOL isEdit;
@property (strong, nonatomic) IBOutlet UILabel *lbHeaderTitle;
@end
