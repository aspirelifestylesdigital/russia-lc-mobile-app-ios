//
//  RequestDetailsTableViewCell.m
//  ALC
//
//  Created by Tho Nguyen on 10/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "RequestDetailsTableViewCell.h"

@interface RequestDetailsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end

@implementation RequestDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadDataForCell:(NSString *)title andValue:(NSString *)value {
    self.lblTitle.text = title;
    self.lblValue.text = value;
    
    [self.lblTitle setFont:[UIFont fontWithName:@"AvenirNextLTPro-Medium" size:FONT_SIZE_16*SCREEN_SCALE]];
    [self.lblValue setFont:[UIFont fontWithName:@"AvenirNextLTPro-Medium" size:FONT_SIZE_16*SCREEN_SCALE]];
}


@end
