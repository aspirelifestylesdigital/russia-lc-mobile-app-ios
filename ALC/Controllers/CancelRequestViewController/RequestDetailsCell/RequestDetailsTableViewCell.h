//
//  RequestDetailsTableViewCell.h
//  ALC
//
//  Created by Tho Nguyen on 10/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestDetailsTableViewCell : UITableViewCell

- (void)loadDataForCell:(NSString *)title andValue:(NSString *)value;

@end
