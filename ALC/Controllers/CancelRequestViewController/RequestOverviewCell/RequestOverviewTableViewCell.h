//
//  RequestOverviewTableViewCell.h
//  ALC
//
//  Created by Tho Nguyen on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyRequestObject;

@interface RequestOverviewTableViewCell : UITableViewCell

- (void)loadDataForCell:(id)requestDetail andRequestType:(NSInteger)requestType andMyRequest:(MyRequestObject *)myRequest;

@end
