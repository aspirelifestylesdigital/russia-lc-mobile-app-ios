//
//  EventRequestController.h
//  ALC
//
//  Created by Hai NguyenV on 11/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionalDetailsView.h"
#import "SubFilterController.h"
#import "EventRequestObj.h"
#import "WSCreateEventRequest.h"
#import "WSCreateRecommentRequest.h"
#import "EventRecommendRequestObj.h"

@interface EventRequestController : MainViewController<UITextFieldDelegate,OptionalDetailsViewDelegate,SubFilterControllerDelegate,ErrorToolTipDelegate,DataLoadDelegate>
{
    NSString* selectedCountry;
    NSString* selectedCountryRecom;
    NSString* selectedCity;
    NSString* selectedCityRe;
    NSString* selectedState;
    NSString* selectedStateRe;
    NSMutableArray *selectedEventCategoryKey;
    NSMutableArray *selectedEventCategory;
    NSMutableDictionary* dictIconErrorManager;
    UIDatePicker *datepickerReservation;
    UIDatePicker *timepickerReservation;
    
    WSCreateEventRequest* wsCreateRequest;
    WSCreateRecommentRequest* wsCreateRecommend;
}
@property (strong, nonatomic) NSString* bookingId;
@property (nonatomic) enum REQUEST_TYPE requestType;
@property (nonatomic) BOOL hideTopNavigator;

@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet CustomTextField *txtEventName;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationDate;
@property (strong, nonatomic) IBOutlet UITextField *txtReservationTime;
@property (weak, nonatomic) IBOutlet UIView *viewEventTime;
@property (weak, nonatomic) IBOutlet UILabel *lbEventDate;
@property (weak, nonatomic) IBOutlet UIView *viewEventName;
@property (weak, nonatomic) IBOutlet UIView *viewEventCategory;
@property (strong, nonatomic) IBOutlet UIButton *btEventCategory;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;


@property (strong, nonatomic) IBOutlet UIView *topOrangeUcomingBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowUpcoming;
@property (strong, nonatomic) IBOutlet UIView *topOrangeHistoryBar;
@property (strong, nonatomic) IBOutlet UIImageView *bottomArrowHistory;
@property (strong, nonatomic) IBOutlet UIButton *btBook;
@property (strong, nonatomic) IBOutlet UIButton *btRecommend;

@property (strong, nonatomic) IBOutlet ErrorToolTip *icCityError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icCountryError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icTimeError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icStateError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icEventNameError;
@property (strong, nonatomic) IBOutlet ErrorToolTip *icEventCategoryError;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pContraintEventDate;

- (IBAction)actionSubmitRequest:(id)sender;
@end
