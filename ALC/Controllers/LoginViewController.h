//
//  LoginViewController.h
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "ErrorToolTip.h"
#import "WSB2CLogin.h"
#import "WSB2CGetUserDetails.h"

@interface LoginViewController : MainViewController<DataLoadDelegate, UITextFieldDelegate, ErrorToolTipDelegate>
{
    WSB2CLogin* wsLogin;
}
- (IBAction)backSplashScreen:(id)sender;
@end
