//
//  ServiceLandingController.m
//  ALC
//
//  Created by Anh Tran on 9/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ServiceLandingController.h"
#import "ExperienceViewCell.h"
#import "ExperienceItem.h"
#import "GourmetRequestController.h"
#import "HotelRequestController.h"
#import "CarRentalTransferRequestController.h"
#import "GolfRequestController.h"
#import "OrthersBookingController.h"
#import "EventRequestController.h"

@interface ServiceLandingController (){
    NSArray *data;
    int cellHeight;
}
@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation ServiceLandingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"CONCIERGE REQUEST", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    data = [self experienceLandingList];
    cellHeight = self.view.frame.size.width / (320.0/185.0);
    // Do any additional setup after loading the view.
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ExperienceViewCell";
    
    ExperienceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib;
    nib = [[NSBundle mainBundle] loadNibNamed:@"ExperienceViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    ExperienceItem *item = ((ExperienceItem*)[data objectAtIndex:indexPath.row]);
    [cell.lbExperienceName setFont:[UIFont fontWithName:@"AvenirNextLTPro-Demi" size:FONT_SIZE_16*SCREEN_SCALE]];
    cell.lbExperienceName.text = NSLocalizedString(item.title, nil);
    cell.imvImage.image = [UIImage imageNamed:item.imageURL];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //TODO: open type landing page
    if(indexPath.row == 0){
        GourmetRequestController* controller = (GourmetRequestController*) [self controllerByIdentifier:@"GourmetRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if(indexPath.row == 1){
        HotelRequestController* controller = (HotelRequestController*) [self controllerByIdentifier:@"HotelRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    } else if(indexPath.row == 2){
        EventRequestController* controller = (EventRequestController*) [self controllerByIdentifier:@"EventRequestController"];
        [self.navigationController pushViewController:controller animated:YES];        
    } else if(indexPath.row == 3){
        CarRentalTransferRequestController* controller = (CarRentalTransferRequestController*) [self controllerByIdentifier:@"CarRentalTransferRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    }else if(indexPath.row == 4){
        GolfRequestController* controller = (GolfRequestController*) [self controllerByIdentifier:@"GolfRequestController"];
        [self.navigationController pushViewController:controller animated:YES];
    }else if(indexPath.row == 5){
        OrthersBookingController* controller = (OrthersBookingController*) [self controllerByIdentifier:@"OrthersBookingController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(NSArray*) experienceLandingList{
    return [[NSArray alloc] initWithObjects:
            [[ExperienceItem alloc] initWithType:@"DINING" andImage:@"rsz_concierge_service_-_restaurant_and_bars.jpg"],
            [[ExperienceItem alloc] initWithType:@"HOTEL" andImage:@"rsz_concierge_services_-_hotels.jpg"],
            [[ExperienceItem alloc] initWithType:@"ENTERTAINMENT" andImage:@"rsz_concierge_services_-_entertainment.jpg"],
            [[ExperienceItem alloc] initWithType:@"TRANSPORTATION" andImage:@"rsz_concierge_service_-_car_rental.jpg"],
            [[ExperienceItem alloc] initWithType:@"GOLF" andImage:@"rsz_concierge_service_-_golf.jpg"],            
            [[ExperienceItem alloc] initWithType:@"OTHERS" andImage:@"Image_6.png"],
            nil];
}

@end

