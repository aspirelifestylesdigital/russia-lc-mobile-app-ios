//
//  CityGuideLandingController.m
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CityGuideLandingController.h"
#import "CityGuideDetailsController.h"
#import "BaseResponseObject.h"
#import "ExperienceViewCell.h"
#import "CityGuideObject.h"
#import "UIImageView+AFNetworking.h"

@interface CityGuideLandingController ()
{
    int cellHeight;
    NSMutableArray *arrayData;
    NSMutableArray *filteredData;
    BOOL isLoading;
    WSGetCityGuideList *wsList;
    //CLLocation *currentLocation;
    AppDelegate* appdele;
    NSMutableDictionary *selectedFilterOptions;
    CGRect originalSize;
}
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation CityGuideLandingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTitleToHeader:NSLocalizedString(@"CITY GUIDES", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    cellHeight = self.view.frame.size.width / (320.0/185.0);
    
    arrayData = [NSMutableArray array];
    filteredData = [NSMutableArray array];
    
    appdele =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    originalSize = _table.frame;
    _txtSearch.delegate = self;
    [self getDataList];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_txtSearch resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGRect frame = _table.frame;
    frame.size.height = originalSize.size.height  - 216;
    _table.frame = frame;
}

-(void)keyboardWillHide {
    _table.frame = originalSize;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDataList{
    [self getDataListWithOptions:nil];
}

-(void) getDataListWithOptions:(NSMutableDictionary *)options{
    [self showToast];
    isLoading = true;
    
    arrayData = [NSMutableArray array];
    filteredData = [NSMutableArray array];
    [_table reloadData];
    
    //currentLocation = [appdele requestUserCurrentLocation];
    //NSLog(@"CurrentLocation: %f, %f", currentLocation.coordinate.longitude, currentLocation.coordinate.latitude);
    wsList = [[WSGetCityGuideList alloc] init];
    wsList.delegate = self;
    [wsList getCityGuideList];
}

-(void)loadDataDoneFrom:(WSBase*)ws{
    //TODO: display data to View
    if(filteredData!=nil && filteredData.count == arrayData.count){
        filteredData = nil;
    }
    
    [arrayData addObjectsFromArray:((WSGetCityGuideList*)ws).itemDatas];
   
    if(filteredData == nil){
        filteredData = arrayData;
    }
    
    [_table reloadData];
    
    isLoading = false;
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    if(result!=nil && result.message!=nil){
        // [self showErrorMessage:result.message];
    }
    
    isLoading = false;
    [self stopToast];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
/*- (IBAction)filterClicked:(id)sender {
 ExperiencesFilterController  * controller = (ExperiencesFilterController*) [self controllerByIdentifier:@"ExperiencesFilter"];
 controller.delegate = self;
 controller.filterType = FILTER_GOURMET;
 controller.selectedOptions = selectedFilterOptions;
 [self.navigationController pushViewController:controller animated:YES];
 }*/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{    
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ExperienceViewCell";
    
    ExperienceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(!cell){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"ExperienceViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    [cell setBackgroundColor:[UIColor clearColor]];
    CityGuideObject *item = ((CityGuideObject*)[filteredData objectAtIndex:indexPath.row]);
    [cell.lbExperienceName setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:FONT_SIZE_16*SCREEN_SCALE]];
    cell.lbExperienceName .text = item.cityName;
    [cell.imvImage setImageWithURL:[NSURL URLWithString:item.backgroundImageUrl] placeholderImage:nil];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CityGuideObject *item = ((CityGuideObject*)[arrayData objectAtIndex:indexPath.row]);
    CityGuideDetailsController* controller = (CityGuideDetailsController*) [self controllerByIdentifier:@"CityGuideDetailsController"];
    controller.cityGuideId = item.cityId;
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)searchValueChanged:(id)sender {
    if(_txtSearch.text!=nil && _txtSearch.text.length>0)
    {
        filteredData = [self filterCityByName:_txtSearch.text];
    } else {
        filteredData = arrayData;
    }
    [_table reloadData];
}

-(NSMutableArray*) filterCityByName:(NSString*) term{
    NSMutableArray *result = [NSMutableArray array];
    if(arrayData!=nil){
        for (CityGuideObject* city in arrayData) {
            if([[city.cityName lowercaseString] containsString:[term lowercaseString]]){
                [result addObject:city];
            }
        }
    }
    return  result;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [_txtSearch resignFirstResponder];
    return YES;
}

@end
