//
//  CCNumberController.m
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CCNumberController.h"
#import "CCNumberCell.h"
#import "CCNumber.h"
@interface CCNumberController (){
    int selectedIndex;
    CGRect originalSize;
    NSArray *filteredData;
}
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintSearchView;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@end

@implementation CCNumberController
@synthesize data, dataType, selectedValue;
- (void)viewDidLoad {
    [super viewDidLoad];
    if(data == nil){
        data = [NSArray array];
    }
    filteredData = data;
    
    selectedIndex = [self findSelectedIndex];
    
    if(dataType == 1){
        _heightConstraintSearchView.constant = 0;
        [self addTitleToHeader:NSLocalizedString(@"SALUTATION",nil) withColor:HEADER_COLOR];
    } else if(dataType == 2) {
        _heightConstraintSearchView.constant = _heightConstraintSearchView.constant  * SCREEN_SCALE;
        _searchBar.delegate = self;
        [self addTitleToHeader:NSLocalizedString(@"COUNTRY CODE",nil) withColor:HEADER_COLOR];
    }
    
    originalSize = _table.frame;
    [self customButtonBack];
}

//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return NO;
}
//--------------------------

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [_searchBar resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int) findSelectedIndex{
    if(selectedValue!=nil){
        for (int i = 0; i < filteredData.count; i++) {
            if ([selectedValue isEqualToString:((CCNumber*)[filteredData objectAtIndex:i]).shortCCNumber]) {
                return i;
            }
        }
    }
    return -1;
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    CGRect frame = _table.frame;
    frame.size.height = originalSize.size.height  - 216;
    _table.frame = frame;
}

-(void)keyboardWillHide {
    _table.frame = originalSize;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0*SCREEN_SCALE;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CCCell";
    
    CCNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib;
    nib = [[NSBundle mainBundle] loadNibNamed:@"CCNumberCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    UIFont* font = cell.labelName.font;
    [cell.labelName setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    cell.labelName.text = ((CCNumber*)[filteredData objectAtIndex:indexPath.row]).longDescription;
    cell.btCheck.hidden = indexPath.row != selectedIndex;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* newnumber =((CCNumber*)[filteredData objectAtIndex:indexPath.row]).shortCCNumber;
    if(indexPath.row == selectedIndex){
        newnumber = nil;
    }
    if(indexPath.row < filteredData.count && self.delegate!=nil){
        [self.delegate pickCCNumber:newnumber forType:dataType];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSMutableArray*) filterCountryByName:(NSString*) term{
    NSMutableArray *result = [NSMutableArray array];
    if(data!=nil){
        for (CCNumber* cc in data) {
            if([[cc.longDescription lowercaseString] containsString:[term lowercaseString]]){
                [result addObject:cc];
            }
        }
    }
    return  result;
}

#pragma SearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText && searchText.length>0)
    {
        filteredData = [self filterCountryByName:searchText];
    } else {
        filteredData = data;
    }
    selectedIndex = [self findSelectedIndex];
    [_table reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

@end




