//
//  InAppBrowserViewController.h
//  ALC
//
//  Created by Nhat Huy Truong  on 9/15/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

@interface InAppBrowserViewController : MainViewController<UIWebViewDelegate>

@property (assign, nonatomic) BOOL isLiveChat;
@property (strong, nonatomic) NSString* urlBooking;
@property (strong, nonatomic) NSString* headerTitle;
@property (strong, nonatomic) IBOutlet UIWebView *wbShowContent;
@end
