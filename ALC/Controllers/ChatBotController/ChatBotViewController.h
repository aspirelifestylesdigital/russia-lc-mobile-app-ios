//
//  ChatBotViewController.h
//  ALC
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "CellView_Bot.h"
#import "CellView_User.h"
#import "CellScrollViewBot.h"
#import "CommonUtils.h"
#import "StoreUserData.h"
#import "UserObject.h"
#import <MessageUI/MessageUI.h>

#define API_CALL_CHAT @"https://chatbot.s3corp.com.vn:3005/Program-O/chatbot/conversation_start.php"

@interface ChatBotViewController : MainViewController<UITextFieldDelegate,CLLocationManagerDelegate,CellView_BotDelegate,MFMailComposeViewControllerDelegate>
{
    NSInteger currentHeight;
    NSString* convo_id;
    CLLocationManager* currentLocation;
    BOOL isFirst;
    
    BOOL canGetLocation;
    ChatButtonObject* currentObjectClick;
    
    NSTimer* timeCount;
    NSInteger currentSecond;
    BOOL isFreeTime;
    
    BOOL isWarning;
    
    BOOL isNeedProvideLocation;
    
    NSTimer* timeShake;
}

@property (strong,nonatomic) NSMutableArray* arrayChatBot;
@property (strong,nonatomic) NSString* firstText;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutSpaceWithBottom;
@property (strong, nonatomic) IBOutlet UIView *vwBottom;
@property (strong, nonatomic) IBOutlet UITextField *txtChat;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *vwMainView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightMainView;
@property (strong, nonatomic) IBOutlet UIImageView *imvSpoon;


- (IBAction)clickSendMessage:(id)sender;
- (IBAction)clickShowOrCloseKeyBoard:(id)sender;
- (IBAction)clickListRestaurant:(id)sender;
@end
