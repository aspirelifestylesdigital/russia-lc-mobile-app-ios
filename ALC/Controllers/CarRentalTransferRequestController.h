//
//  CarRentalTransferRequestController.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSCreateCarTransferRequest.h"
#import "CarRentalTransferView.h"
#import "WSGetMyPreferences.h"

@interface CarRentalTransferRequestController : MainViewController<DataLoadDelegate, CarRentalTransferViewDelegate>
{
    WSGetMyPreferences* wsGetMyPreference;
}
@property (nonatomic) enum REQUEST_TYPE requestType;
@property (strong, nonatomic) NSString* bookingId;
@property (nonatomic) BOOL hideTopNavigator;
@end
