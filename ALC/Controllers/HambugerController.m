//
//  HambugerController.m
//  ALC
//
//  Created by Hai NguyenV on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "HambugerController.h"
#import "MenuCell.h"
#import "SlideNavigationController.h"
#import "ExperienceLandingController.h"
#import "LoginViewController.h"
#import "MyRequestController.h"
#import "CityGuideLandingController.h"
#import "AccountController.h"
#import "StoreUserData.h"
#import "AboutController.h"
#import "ServiceLandingController.h"




@interface HambugerController () {
    NSArray *dataSource;
}
@end

@implementation HambugerController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
//    INDEX_HOME = 0;
//    INDEX_ACCOUNT = 1;
//    INDEX_MY_REQUESTS = 2;
   // INDEX_NOTIFICATIONS = 3;
//    INDEX_SEND_CONCIERGE = 3;
   // INDEX_CITY_GUIDE = 4;
//    INDEX_EXPERIENCES = 4;
//    INDEX_ABOUT = 5;
//    INDEX_SIGNOUT = 6;
    
    
    
    INDEX_HOME = [NSNumber numberWithInteger:0];
    INDEX_ACCOUNT = [NSNumber numberWithInteger:1];
    INDEX_MY_REQUESTS = [NSNumber numberWithInteger:2];
    INDEX_SEND_CONCIERGE = [NSNumber numberWithInteger:3];
    INDEX_EXPERIENCES = [NSNumber numberWithInteger:4];
    INDEX_ABOUT = [NSNumber numberWithInteger:5];
    INDEX_SIGNOUT = [NSNumber numberWithInteger:6];
    
   
    dataSource = [[NSArray alloc] initWithObjects: INDEX_HOME, INDEX_ACCOUNT, INDEX_MY_REQUESTS, INDEX_SEND_CONCIERGE, INDEX_EXPERIENCES, INDEX_ABOUT, INDEX_SIGNOUT,  nil];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) redisplayHome{
    [_pTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[INDEX_HOME integerValue]  inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (dataSource == NULL) {
        return 0;
    }
    return dataSource.count;
//    if([appdele isLoggedIn] == false){
//        return 6;
//    }
//    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *predictCellIndentifier = @"MenuCell";
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:predictCellIndentifier];
    if(!cell){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"MenuCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        resetScaleViewBaseOnScreen(cell);
    }
    
    NSNumber *menuValue = [dataSource objectAtIndex:indexPath.row];
    
    
    
    if(menuValue == INDEX_HOME){
        cell.imvIcon.image = [UIImage imageNamed:@"Home"];
        cell.lbName.text = NSLocalizedString(@"Home", nil);
    }else if(menuValue == INDEX_ACCOUNT){
        cell.imvIcon.image = [UIImage imageNamed:@"Account"];
        cell.lbName.text = NSLocalizedString(@"Account", nil);
    }else if(menuValue == INDEX_MY_REQUESTS){
        cell.imvIcon.image = [UIImage imageNamed:@"MyRequests"];
        cell.lbName.text = NSLocalizedString(@"My Requests", nil);
    }else if(menuValue == INDEX_NOTIFICATIONS){
        cell.imvIcon.image = [UIImage imageNamed:@"Notifications"];
        cell.lbName.text = NSLocalizedString(@"Notifications", nil);
    }else if(menuValue == INDEX_SEND_CONCIERGE){
        cell.imvIcon.image = [UIImage imageNamed:@"SendConciergeRequests"];
        cell.lbName.text = NSLocalizedString(@"Send Concierge Requests", nil);
    }else if(menuValue == INDEX_CITY_GUIDE){
        cell.imvIcon.image = [UIImage imageNamed:@"CityGuides"];
        cell.lbName.text = NSLocalizedString(@"City Guides", nil);
    }else if(menuValue == INDEX_EXPERIENCES){
        cell.imvIcon.image = [UIImage imageNamed:@"Experiences"];
        cell.lbName.text = NSLocalizedString(@"Experiences", nil);
    }else if(menuValue == INDEX_ABOUT){
        cell.imvIcon.image = [UIImage imageNamed:@"About"];
        cell.lbName.text = NSLocalizedString(@"About", nil);
    }else if (menuValue == INDEX_SIGNOUT){
        if(isUserLoggedIn()){
            cell.imvIcon.image = [UIImage imageNamed:@"SignOut"];
            cell.lbName.text = NSLocalizedString(@"Sign Out", nil);
        }else{
            cell.imvIcon.image = [UIImage imageNamed:@"SignIn"];
            cell.lbName.text = SIGN_IN;
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45*SCREEN_SCALE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    NSNumber *menuValue = [dataSource objectAtIndex:indexPath.row];
    
    if(menuValue == INDEX_HOME){
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:NOTIFY_CLICK_HOME
         object:self];
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"Home",nil);
        
        [self.pTableView deselectRowAtIndexPath:[self.pTableView indexPathForSelectedRow] animated:YES];
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
        
    }else if(menuValue == INDEX_ACCOUNT){
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"Account",nil) ;
        
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if([appdele isLoggedIn]){
            AccountController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccountController"];
            
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                     withSlideOutAnimation:YES
                                                                             andCompletion:nil];
        } else {
            [self closeMenuAndShowSignInAlert];
        }
        
    }else if(menuValue == INDEX_MY_REQUESTS){
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"My Requests",nil) ;
        
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if([appdele isLoggedIn]){
            MyRequestController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"MyRequest"];
            
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                     withSlideOutAnimation:YES
                                                                             andCompletion:nil];
        } else {
            [self closeMenuAndShowSignInAlert];
        }
    }else if(menuValue == INDEX_NOTIFICATIONS){

    }else if(menuValue == INDEX_SEND_CONCIERGE){
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"Send concierge request",nil);
         
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if([appdele isLoggedIn]){
            ServiceLandingController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceLandingController"];
            
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                     withSlideOutAnimation:YES
                                                                             andCompletion:nil];
        } else {
            [self closeMenuAndShowSignInAlert];
        }
        
    }else if(menuValue == INDEX_CITY_GUIDE){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        CityGuideLandingController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"CityGuideLandingController"];
        
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                 withSlideOutAnimation:YES
                                                                         andCompletion:nil];

    }else if(menuValue == INDEX_EXPERIENCES){
        ExperienceLandingController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"ExperiencesLanding"];
        
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                      withSlideOutAnimation:YES
                                                                              andCompletion:nil];
    }else if(menuValue == INDEX_ABOUT){
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"About",nil);
         
        AboutController* controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"AboutController"];
        
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                                 withSlideOutAnimation:YES
                                                                         andCompletion:nil];
    }else if(menuValue == INDEX_SIGNOUT){
        
        trackGAICategory(@"Concierge",@"Open concierge menu",@"Sign Out",nil);
         
        AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        [appdele logoutUser];
        [self setRootViewLogout];
    }else{
        
    }
//    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
//                                                             withSlideOutAnimation:YES
//                                                                     andCompletion:nil];
}

- (void) closeMenuAndShowSignInAlert{
    [self redisplayHome];
    [[SlideNavigationController sharedInstance]  closeMenuWithCompletion:^{
        UIAlertAction *signInAction = [UIAlertAction actionWithTitle:SIGN_IN
                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                   [self setRootViewLogout];
                                                               }];
        showAlertTopOpenLogin([SlideNavigationController sharedInstance] , signInAction);            }];
}

-(void)setRootViewLogout{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];

    UINavigationController *navigation = (UINavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"NavigationbarLogin"];

    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdele.socialMenu = nil;
    [UIView transitionWithView:appdele.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        UIApplication.sharedApplication.delegate.window.rootViewController = navigation;
                        
                    }
                    completion:nil];
}

@end
