//
//  MMWebService.m
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//

#import "HGWWebService.h"
#import "Constant.h"

@implementation HGWWebService
@synthesize delegate, task, subTask, contentType;

-(BOOL) isRunning
{
	return isActive;
}

-(void) cancelRequest
{
	if(isActive)
	{
		[conn cancel],conn=nil;
		resultData=nil;
		
        isActive = NO;
	}
}

-(void) invokeMethod:(NSString*)httpVerb andObject:(HGWBaseRequest*)obj forTask:(NSInteger)aTask forSubTask:(NSInteger)aSubTask
{
	[self cancelRequest];
	
	isActive = YES;
    self.task = aTask;
    self.subTask = aSubTask;
    self.contentType = obj.contentType;
    
    NSString* requestString = [obj requestString];
    NSMutableURLRequest *req;
    
    if ([httpVerb isEqual: kHTTPGET]) {
        NSString* urlString;
        if (requestString != nil && requestString.length>0) {
            urlString = [NSString stringWithFormat:@"%@?%@",obj.requestUrl, requestString];
        }
        else
        {
            urlString = obj.requestUrl;
        }
        
        NSLog(@"Request: %@", urlString);
        
        req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    }
    else if([httpVerb isEqual: kHTTPPOST])
    {
        if (contentType == RCT_JSON)
        {
            req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",obj.requestUrl]]];
            
            [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            NSString *msgLen = [NSString stringWithFormat:@"%d", [requestString length]];
            [req addValue:msgLen forHTTPHeaderField:@"Content-Length"];
            
            [req setHTTPBody: [requestString dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else
        {
             req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:obj.requestUrl]];
            
            [req addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            NSString *msgLen = [NSString stringWithFormat:@"%d", [requestString length]];
            [req addValue:msgLen forHTTPHeaderField:@"Content-Length"];
            
            [req setHTTPBody: [requestString dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSLog(@"Request: %@", obj.requestUrl);
        NSLog(@"Body: %@", requestString);
    }
    else if([httpVerb isEqual: kHTTPPUT])
    {
        req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:obj.requestUrl]];
        if (contentType == RCT_JSON) {
            [req addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            NSString *msgLen = [NSString stringWithFormat:@"%d", [requestString length]];
            [req addValue:msgLen forHTTPHeaderField:@"Content-Length"];
            
            [req setHTTPBody: [requestString dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else
        {
            [req addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            
            NSString *msgLen = [NSString stringWithFormat:@"%d", [requestString length]];
            [req addValue:msgLen forHTTPHeaderField:@"Content-Length"];
            
            [req setHTTPBody: [requestString dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    else if ([httpVerb isEqual: kHTTPDEL]) {
        NSString* urlString;
        if (requestString != nil) {
            urlString = [NSString stringWithFormat:@"%@?%@",obj.requestUrl, requestString];
        }
        else
        {
            urlString = obj.requestUrl;
        }
        req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    }
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(appdele.AuthToken!=nil){
        [req addValue:appdele.AuthToken forHTTPHeaderField:@"Authorization"];
        NSLog(@"Authorization: %@", appdele.AuthToken);
    }
    [req setHTTPMethod:httpVerb];

	
	if(conn != nil)
	{
		[conn cancel];
	}
	
	conn = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    
    if (conn)
    {
        resultData = [[NSMutableData alloc] init];
    }
}

-(void) invokeMethodImageUpload:(NSString*)httpVerb andObject:(HGWBaseRequest*)obj forTask:(NSInteger)aTask forSubTask:(NSInteger)aSubTask withData:(NSData*)dataImage{
    [self cancelRequest];
	
	isActive = YES;
    self.task = aTask;
    self.subTask = aSubTask;
    self.contentType = obj.contentType;
    
    NSMutableURLRequest *request ;
    
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:obj.requestUrl]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentTypes = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentTypes forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in obj.dictKeyValues) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"%@_%@",param,[obj.dictKeyValues objectForKey:param]);
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [obj.dictKeyValues objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    if (dataImage) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", @"UploadPhoto"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:dataImage];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(appdele.AuthToken!=nil){
        [request addValue:appdele.AuthToken forHTTPHeaderField:@"Authorization"];
        NSLog(@"Authorization: %@", appdele.AuthToken);
    }
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
	
	if(conn != nil)
	{
		[conn cancel];
	}
	
	conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (conn)
    {
        resultData = [[NSMutableData alloc] init];
    }
}
#pragma mark -NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
//    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}
-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *) response
{
	[resultData setLength: 0];
}

-(void) connection:(NSURLConnection *) connection didReceiveData:(NSData *) data
{
	[resultData appendData:data];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
	if(connection != conn){return;}
	
	isActive = NO;
	
	if(self.delegate)
        [self.delegate processResults:resultData forTask:self.task forSubTask:self.subTask returnFormat:self.contentType];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	
	isActive = NO;
    
    if ([self.delegate conformsToProtocol:@protocol(WebServiceDelegate)]) {
        [self.delegate processResultWithError:error];
    }
}

- (void)dealloc
{
	[self cancelRequest];
}



#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengeUseCredential,[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
    [[challenge sender] useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
       [sessionDataTask resume];
}


#pragma mark SessionDataDelegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    //NSLog(@"didReceiveResponse");
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
  //  NSLog(@"didBecomeDownloadTask");
}

/* Sent when data is available for the delegate to consume.  It is
 * assumed that the delegate will retain and not copy the data.  As
 * the data may be discontiguous, you should use
 * [NSData enumerateByteRangesUsingBlock:] to access it.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
   // NSLog(@"didReceiveData");
}

/* Invoke the completion routine with a valid NSCachedURLResponse to
 * allow the resulting data to be cached, or pass nil to prevent
 * caching. Note that there is no guarantee that caching will be
 * attempted for a given resource, and you should not rely on this
 * message to receive the resource data.
 */
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    //NSLog(@"willCacheResponse");
}

@end

