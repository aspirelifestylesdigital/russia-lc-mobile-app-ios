//
//  WSGetCitiesList.m
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetCitiesList.h"
#import "HGWGetBaseRequest.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "CityObject.h"

@implementation WSGetCitiesList

-(void)getList{
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_CITY;
    subTask = WS_ST_NONE;
    
    HGWGetBaseRequest *request = [[HGWGetBaseRequest alloc]init];
    request.requestUrl = [API_URL stringByAppendingString:GetCityList];
    request.dictKeyValues = [self buildRequestParams];
    
    [service invokeMethod:kHTTPGET andObject:request forTask:task forSubTask:subTask];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            NSArray* data = [jsonResult arrayForKey:@"Data"];
            self.citiesList = [NSMutableArray array];
            self.citiesFullList = [NSMutableArray array];
            CityObject *city;
            for (int i = 0; i < data.count; i++) {
                city = [[CityObject alloc] initFromDict:[data objectAtIndex:i]];
                [self.citiesFullList addObject:city];
                [self.citiesList addObject:city.cityName];
            }
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
-(void)processDataResultWithError:(NSError *)error
{
    [delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    if(_filterCity){
        [dictKeyValues setObject:_filterCity forKey:@"City"];
    }
    
    if(_filterCountry){
        [dictKeyValues setObject:_filterCountry forKey:@"Country"];
    }
    return dictKeyValues;
}
@end
