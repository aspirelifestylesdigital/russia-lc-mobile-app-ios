//
//  WSB2CGetRequestToken.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSB2CGetRequestToken : WSBase
@property (strong, nonatomic) NSString* requestToken;
-(void) getRequestToken;
@end
