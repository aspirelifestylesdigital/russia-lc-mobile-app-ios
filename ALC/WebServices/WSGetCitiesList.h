//
//  WSGetCitiesList.h
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSGetCitiesList : WSBase

@property (strong, nonatomic) NSString *filterCity;
@property (strong, nonatomic) NSString *filterCountry;

//result
@property (strong, nonatomic) NSMutableArray* citiesList;
@property (strong, nonatomic) NSMutableArray* citiesFullList;
-(void)getList;
@end
