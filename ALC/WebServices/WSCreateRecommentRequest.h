//
//  WSCreateRecommentRequest.h
//  ALC
//
//  Created by Hai NguyenV on 12/2/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "EventRecommendRequestObj.h"

@interface WSCreateRecommentRequest : WSB2CBase
{
    EventRecommendRequestObj* requestObject;
}
-(void)bookingEvent:(EventRecommendRequestObj*)eventRequest;
@end
