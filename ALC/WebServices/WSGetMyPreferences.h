//
//  WSGetMyPreferences.h
//  ALC
//
//  Created by Hai NguyenV on 9/15/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSGetMyPreferences : WSB2CBase

@property(nonatomic, strong)NSMutableDictionary* pData;
@property(nonatomic, strong)NSString* category;

-(void)getMyPreference;

@end
