//
//  WSCreateGolfRequest.h
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "GolfRequestObj.h"

@interface WSCreateGolfRequest : WSB2CBase
{
    GolfRequestObj* requestObject;
}
-(void)bookingGolf:(GolfRequestObj*)golfRequest;

@end
