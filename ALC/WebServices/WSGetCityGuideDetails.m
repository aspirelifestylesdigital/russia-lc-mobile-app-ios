//
//  WSGetCityGuideDetails.m
//  ALC
//
//  Created by Anh Tran on 9/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetCityGuideDetails.h"
#import "HGWGetBaseRequest.h"
#import "BaseResponseObject.h"

@implementation WSGetCityGuideDetails
@synthesize cityGuideId;
-(id)init{
    self = [super init];
    return self;
}

-(void)getCityGuideDetails:(NSString*) cityId{
    self.cityGuideId = cityId;
    [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_CITY_GUIDE_DETAILS;
    subTask = WS_ST_NONE;
    
    HGWGetBaseRequest *request = [[HGWGetBaseRequest alloc]init];
    request.dictKeyValues = [self buildRequestParams];
    request.requestUrl = [NSString stringWithFormat:@"%@%@", API_URL, GetCityGuideDetails];
    
    [service invokeMethod:kHTTPGET andObject:request forTask:task forSubTask:subTask];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            NSDictionary* data = [jsonResult dictionaryForKey:@"Data"];
            self.cityDetails = [[CityGuideObject alloc] initFromDict:data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
-(void)processDataResultWithError:(NSError *)error
{
    [delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:cityGuideId forKey:@"CityGuideId"];
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [dictKeyValues setObject:((UserObject*)[appdele getLoggedInUser]).userId  forKey:@"UserID"];
    }
    
    return dictKeyValues;
}
@end
