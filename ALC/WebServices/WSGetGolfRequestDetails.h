//
//  WSGetGolfRequestDetails.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "GolfDetailsObj.h"

@interface WSGetGolfRequestDetails : WSB2CBase
{
    NSString* bookingID;
}
@property (strong, nonatomic) GolfDetailsObj* bookingDetails;

-(void) getRequestDetails:(NSString*) bookingId;

@end
