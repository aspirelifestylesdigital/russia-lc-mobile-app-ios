//
//  WSUpdateProfile.h
//  ALC
//
//  Created by Hai NguyenV on 9/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "UserObject.h"

@interface WSUpdateProfile : WSB2CBase
{
    NSMutableDictionary* user;
}
@property (strong, nonatomic) UserObject* userDetails;
-(void)updateUser:(NSMutableDictionary*) userDict;

@end
