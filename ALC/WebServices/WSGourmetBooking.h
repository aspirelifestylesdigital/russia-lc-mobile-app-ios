//
//  WSGourmetBooking.h
//  ALC
//
//  Created by Hai NguyenV on 10/5/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "GourmetRequestObject.h"

@interface WSGourmetBooking : WSB2CBase
{
    GourmetRequestObject* requestObject;
}
-(void)bookingGourmet:(GourmetRequestObject*)gourmetRequest;

@end
