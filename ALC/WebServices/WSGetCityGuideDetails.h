//
//  WSGetCityGuideDetails.h
//  ALC
//
//  Created by Anh Tran on 9/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"
#import "CityGuideObject.h"
@interface WSGetCityGuideDetails : WSBase
@property (strong, nonatomic) NSString* cityGuideId;
@property (strong, nonatomic) CityGuideObject* cityDetails;
-(void)getCityGuideDetails:(NSString*) cityId;
@end
