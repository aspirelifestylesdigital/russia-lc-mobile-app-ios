//
//  WSGetEntertainmentRequestDetails.h
//  ALC
//
//  Created by Anh Tran on 12/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "EntertainmentRequestDetailsObject.h"

@interface WSGetEntertainmentRequestDetails : WSB2CBase

@property (strong, nonatomic) EntertainmentRequestDetailsObject* bookingDetails;

-(void) getRequestDetails:(NSString*) bookingId withTask:(enum WSTASK) tTask;

@end