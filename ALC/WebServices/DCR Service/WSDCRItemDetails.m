//
//  WSDCRItemDetails.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "WSDCRItemDetails.h"
#import "DCRItemDetailsResponse.h"
#import "BaseDCRObject.h"

@interface WSDCRItemDetails(){
    NSString* itemIds;
}

@end
@implementation WSDCRItemDetails

- (void) findItem:(NSString*) item{
    itemIds = item;
     [self checkAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    task = WS_DCR_ITEM_DETAILS;
    subTask = WS_ST_NONE;
    
    NSString* url = [DCR_API_URL stringByAppendingString:DCR_ITEM_DETAILS_API];
    
    [self GET:url withParams:[self buildRequestParams]];
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        DCRItemDetailsResponse *response = [[DCRItemDetailsResponse alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response success]){
            if(response.items && response.items.count > 0){
                _item = [response.items objectAtIndex:0];
            }
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            BaseResponseObject* res = [[BaseResponseObject alloc] initFromDCR:response];
            [self.delegate loadDataFailFrom:res withErrorCode:res.b2cStatus];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    
    [dictKeyValues setObject:itemIds forKey:@"ids"];
    
    return dictKeyValues;
}



@end
